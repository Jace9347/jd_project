﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDX_Library_X86
{
    using System.Data;
    using System.Collections.Concurrent;
    using Common;

    public class clsQueLog
    {
        ConcurrentQueue<string> LogQ;
        private string sPathFile = null;

        public clsQueLog()
        {
            LogQ = new ConcurrentQueue<string>();
        }

        public void Log_Enqueue(string dat)
        {
            LogQ.Enqueue(dat);
        }

        public void Log_Dequeue()
        { 
            string log = null;
            lock (LogQ)
            {
                for (int i = 0; i < LogQ.Count; i++)
                {
                    LogQ.TryDequeue(out log);
                    if (!string.IsNullOrEmpty(log))
                    {

                    }
                }
            }
        }

        public int Que_Count
        {
            get
            {
                return LogQ.Count;
            }
        }

        public string Que_PathFile
        {
            set
            {
                sPathFile = value;
            }
        }
    }
}
