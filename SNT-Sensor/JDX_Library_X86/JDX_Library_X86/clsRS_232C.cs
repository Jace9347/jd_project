﻿namespace JDX_Library_X86.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO.Ports;
    public class clsRS_232C
    {

        /* 2014.12.24 by Jace
         * Mitutoyo 543-490B 추가
         * 
        */

        #region Serial Variable definitions
        SerialPort Port = new SerialPort(); //("COM1",9600,Parity.None,8)
        //public bool bComPortRecvResult = false; //컴포트 리시브 결과 
        //readonly byte[] bPortData = new byte[128];
        //int PortDataHead = 0;
        //private readonly Byte[] B_ReceiveByte = new Byte[1024];
        private static int iBuffCnt = 4096;
        private Byte[] B_QDataBuffer = new Byte[iBuffCnt];
        private int iHead = 0;
        private int iTail = 0;
        private int iRingCnt = iBuffCnt;
        
        public int Head
        {
            get { return iHead; }
            set { iHead = value; }
        }
        public int Tail
        {
            get { return iTail; }
            set { iTail = value; }
        }
        public int RingCount
        {
            get { return iRingCnt; }
        }
        public byte[] SerialBuffer
        {
            get { return B_QDataBuffer; }
            set { B_QDataBuffer = value; }
        }
        public string PortName
        {
            get { return sPortName; }
            set { sPortName = value; }
        }
        public int BaudRate
        {
            get { return iBaudRate; }
            set { iBaudRate = value; }
        }
        
        public int ErrorCount
        {
            get { return iOpenRetryCount; }
            set { iOpenRetryCount = value; }
        }

        public bool Error
        {
            get { return bOpenStatus; }
            set { bOpenStatus = value;}
        }

        //private readonly string[] B_ReceiveString = new string[1024];
        //public string[] B_sQDataBuffer = new string[1024];
        //public int nReceiveDataCount = 0;
        //public int nReceiveDataReadCount = 0;

        //RTS, DTS Enable
        // 요약:
        // 직렬 통신에 RTS(Request to Send) 신호를 사용할 수 있는지 여부를 나타내는 값을 가져오거나 설정합니다.
        // 직렬 통신 동안 DTR(Data Terminal Ready) 신호를 사용할 수 있는지를 나타내는 값을 가져오거나 설정합니다.
        private bool bRtsEnable = true;
        private bool bDtrEnable = true;


        //Rs232C Defualt 변수
        private string sPortName = "COM100"; //COM1,COM2,COM3,COM4.......
        private int iBaudRate = 9600;                //{2400,4800,9600,19200,38400,57600,115200};      // 
        private Parity ParityBit = Parity.None;   //{ 0: Parity.Odd, 1: Parity.Even, 2: Parity.Mark, 3: Parity.None, 4:Parity.Space };
        private int DataBits = 8;                   //7,8
        private StopBits StopBit = StopBits.One;  // StopBits.None, StopBits.One, StopBits.OnePointFive, StopBits.Two}

        //pdi 2014-05-07 추가 변수
        //통신 연결 후 계측기확인용
        private bool bOpenStatus = false;
        //pdi SendCount 5회 이상시 통신연결 NG 변경
        private int iOpenRetryCount = 0;
        //계측기 모델에 따른 데이터 리시브 이벤트핸들러 변경
        //public string strRecevedHandler = strInstrument[0];
        //public static string[] strInstrument = { null,"Mitutoyo", "TOS5050A", "HIOKI", "Aglient34401A" };

        #endregion
        #region Serial Connection
        /// <summary>
        /// Open
        /// </summary>
        /// <returns>bool</returns>
        public bool Open()
        {
            bool rtn = false;
            Port = new SerialPort();// 메모리 할당
            Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            //Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            Port.PortName = PortName;
            Port.BaudRate = BaudRate;
            Port.Parity = ParityBit;
            Port.DataBits = DataBits;
            Port.StopBits = StopBit;
            Port.RtsEnable = bRtsEnable;  // default false
            Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

            try
            {
                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }

            return rtn;
        }
        public bool Open(string ComPortName)
        {
            bool rtn = false;
            Port = new SerialPort();//메모리할당
            Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            Port.PortName = ComPortName;
            PortName = ComPortName;
            Port.BaudRate = BaudRate;
            Port.Parity = ParityBit;
            Port.DataBits = DataBits;
            Port.StopBits = StopBit;
            // 34401A에 반드시 있어야 동작하는 내용(True)
            Port.RtsEnable = bRtsEnable;  // default false
            Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

            try
            {
                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            return rtn;
        }
        /// <summary>
        /// Open
        /// </summary>
        /// <param name="ComPortName"></param>
        /// <param name="BaudRate_Value"></param>
        /// <returns>bool</returns>
        public bool Open(string ComPortName, int BaudRate_Value)
        {
            bool rtn = false;
            try
            {
                Port = new SerialPort();//메모리할당
                Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
                if (string.IsNullOrEmpty(ComPortName))
                    return false;
                Port.PortName = ComPortName;
                PortName = ComPortName;
                Port.BaudRate = BaudRate_Value;
                BaudRate = BaudRate_Value;
                Port.Parity = ParityBit;
                Port.DataBits = DataBits;
                Port.StopBits = StopBit;
                Port.RtsEnable = bRtsEnable;  // default false
                Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return rtn;
        }
        /// <summary>
        /// Open
        /// </summary>
        /// <param name="ComPortName"></param>
        /// <param name="BaudRate_Value"></param>
        /// <param name="Parity_Value"></param>
        /// <returns>bool</returns>
        public bool Open(string ComPortName, int BaudRate_Value, Parity Parity_Value)
        {
            bool rtn = false;
            Port = new SerialPort();//메모리할당
            Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            Port.PortName = ComPortName;
            Port.BaudRate = BaudRate_Value;
            Port.Parity = Parity_Value;
            Port.DataBits = DataBits;
            Port.StopBits = StopBit;
            Port.RtsEnable = bRtsEnable;  // default false
            Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

            PortName = ComPortName;
            BaudRate = BaudRate_Value;
            ParityBit = Parity_Value;
            try
            {
                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            return rtn;
        }
        /// <summary>
        /// Open
        /// </summary>
        /// <param name="ComPortName"></param>
        /// <param name="BaudRate_Value"></param>
        /// <param name="Parity_Value"></param>
        /// <param name="DataBit_Name"></param>
        /// <returns>bool</returns>
        public bool Open(string ComPortName, int BaudRate_Value, Parity Parity_Value, int DataBit_Name)
        {
            bool rtn = false;
            Port = new SerialPort();//메모리할당
            Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            Port.PortName = ComPortName;
            Port.BaudRate = BaudRate_Value;
            Port.Parity = Parity_Value;
            Port.DataBits = DataBit_Name;
            Port.StopBits = StopBit;
            Port.RtsEnable = bRtsEnable;  // default false
            Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

            PortName = ComPortName;
            BaudRate = BaudRate_Value;
            ParityBit = Parity_Value;
            DataBits = DataBit_Name;

            try
            {
                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            return rtn;
        }
        /// <summary>
        /// Open
        /// </summary>
        /// <param name="ComPortName"></param>
        /// <param name="BaudRate_Value"></param>
        /// <param name="Parity_Value"></param>
        /// <param name="DataBit_Name"></param>
        /// <param name="StopBit_Value"></param>
        /// <returns>bool</returns>
        public bool Open(string ComPortName, int BaudRate_Value, Parity Parity_Value, int DataBit_Name, StopBits StopBit_Value)
        {
            bool rtn = false;
            Port = new SerialPort();//메모리할당
            Port.DataReceived += Port_DataRecieved; // Receive DataReading Handler
            try
            {
                Port.PortName = ComPortName;
                Port.BaudRate = BaudRate_Value;
                Port.Parity = Parity_Value;
                Port.DataBits = DataBit_Name;
                Port.StopBits = StopBit_Value;
                Port.RtsEnable = bRtsEnable;  // default false
                Port.DtrEnable = bDtrEnable;   // default false bRtsEnable = false;

                PortName = ComPortName;
                BaudRate = BaudRate_Value;
                ParityBit = Parity_Value;
                DataBits = DataBit_Name;
                StopBit = StopBit_Value;

                Port.Open();
                if (Port.IsOpen)
                {
                    bOpenStatus = true;
                    rtn = true;
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Rs " + ex.ToString());
            }
            return rtn;
        }

        #endregion
        #region Serial DisConnection
        public bool Close()
        {
            bool rtn = false;
            try
            {
                Port.Close();
                if (!Port.IsOpen)
                {
                    bOpenStatus = false;
                    rtn = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }
        #endregion
        #region Serial Status
        public bool Status()
        {
            bool rtn = false;
            try
            {
                if (Port.IsOpen)
                {
                    //bOpenStatus : 통신 연결 확인 용, 
                    if (bOpenStatus)
                    {
                        rtn = true;
                    }
                }
            }
            catch
            {
                rtn = false;
            }
            return rtn;
        }
        #endregion
        #region Serial Data Reception
        //public byte[] byteBuff;
        //public string strBuff = "";
        //public string[] B_sBuff = new string[1024];
        //private int Buff_Cnt = 0;

        //static Action<string> RecievedCallback;
        //public void SetGrabbedCallback(Action<string> callback)
        //{
        //    RecievedCallback = callback;
        //}


        private void Port_DataRecieved(Object sender, SerialDataReceivedEventArgs s)
        {
            try
            {
                System.Threading.Thread.Sleep(80); // 이값에 따라 Data 절단됨(최저 30msec -> 추천 50mSec 이상)
                int nReadLineCount = Port.ReadBufferSize;
                //bComPortRecvResult = false;
                if (Port.IsOpen)
                {
                    iOpenRetryCount = 0;
                    bOpenStatus = true;
                    //strBuff = null;
                    //byte로 읽어 왔을때의 변환(Byte -> string)
                    int Buff_Cnt = Port.BytesToRead;
                    byte[] byteBuff = new byte[Buff_Cnt];
                    Port.Read(byteBuff, 0, Buff_Cnt);
                    //clsGlobal.frmain.Set_tb_high("rs rcv : " + System.Text.Encoding.Default.GetString(byteBuff));
                    for (int i = 0; i < Buff_Cnt; i++)
                    {
                        B_QDataBuffer[iHead++] = byteBuff[i];
                        iHead %= iRingCnt;
                    }



                    //strBuff = System.Text.Encoding.Default.GetString(byteBuff);
                    //RecievedCallback(strBuff);


                    //strBuff = strBuff.Replace("\0", null); // Reading Data의 CR,LF를 Tale에서 없애버림(변경가능)

                    //CR + LF
                    //if (Buff_Cnt == 17 && strBuff.Substring(15, 2) == Environment.NewLine)
                    //{
                    //    strBuff = strBuff.Replace(Environment.NewLine, null);//2014 12 17 jace 변환 꼭 필요
                    //}   
                    //else if (Buff_Cnt == 16 && strBuff.Substring(15, 1) == "\r")   //CR Delete
                    //{
                    //    strBuff = strBuff.Replace("\r", null);//2014 12 17 jace 변환 꼭 필요
                    //}
                    //else
                    //{
                    //    strBuff = strBuff.Replace(Environment.NewLine, null);//2014 12 17 jace 변환 꼭 필요
                    //}
                    //bComPortRecvResult = true;
                    //if (strBuff != null) Console.WriteLine(strBuff);
                }
                /*
                * Starrett SPC data stream consists of 25 ASCII characters.
                * Characters are sent at 1200 baud, 8 bits (8bit must be masked as it's state is undefined), no parity,
                * and 2 stop bits. The 25 character stream has the following format:
                * 1-13 = ASCII Spaces (0x20)
                * 14 = Space (0x20) if reading is positive, Minus Sign (-) (0x2d) if reading is negative.
                * 15-21 = ASCII digits representing current reading including a floating decimal point (.) (0x2e).
                * 22 = Space (0x20).
                * 23-24 = Current unit in ASCII. Inches = "in", Millimeters = "mm"
                * 25 = Carriage Return (0x0d)
               */
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Serial Port; RECV]" + ex.Message);
            }
        }
        #endregion
        #region Serial Transmission
        public bool SendMsg(byte[] DataBuffer, int BufferCount)
        {
            bool rtn = false;
            try
            {
                if (Port.IsOpen)
                {

                    if (iOpenRetryCount > 5)
                    {
                        bOpenStatus = false;
                    }
                    Port.Write(DataBuffer, 0, BufferCount);
                    //iOpenRetryCount++;
                    rtn = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Serial Port; SEND]" + ex.Message);
            }
            return rtn;
        }
        public bool SendMsg(byte[] DataBuffer)
        {
            bool rtn = false;
            try
            {
                if (Port.IsOpen)
                {
                    if (iOpenRetryCount > 5)
                    {
                        bOpenStatus = false;
                    }
                    Port.Write(DataBuffer, 0, DataBuffer.Length);
                    iOpenRetryCount++;
                    rtn = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }
        public bool SendMsg(char[] DataBuffer, int BufferCount)
        {
            bool rtn = false;
            byte[] Sendbuff = new byte[BufferCount];
            try
            {
                if (iOpenRetryCount > 5)
                {
                    bOpenStatus = false;
                }
                for (int i = 0; i < BufferCount; i++)
                {
                    Sendbuff[i] = (byte)DataBuffer[i];
                }
                Port.Write(Sendbuff, 0, BufferCount);
                iOpenRetryCount++;
                rtn = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }
        public bool SendMsg(char[] DataBuffer)
        {
            bool rtn = false;
            byte[] Sendbuff = new byte[DataBuffer.Length];
            try
            {
                if (iOpenRetryCount > 5)
                {
                    bOpenStatus = false;
                }
                for (int i = 0; i < DataBuffer.Length; i++)
                {
                    Sendbuff[i] = (byte)DataBuffer[i];
                }
                Port.Write(Sendbuff, 0, DataBuffer.Length);
                iOpenRetryCount++;
                rtn = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }
        public bool SendMsg(string Commend)
        {
            bool rtn = false;
            try
            {
                if (iOpenRetryCount > 5)
                {
                    bOpenStatus = false;
                }
                Port.WriteLine(Commend);
                iOpenRetryCount++;
                rtn = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }

        public bool SendMsg(string Commend, bool Option)
        {
            bool rtn = false;
            try
            {
                if (iOpenRetryCount > 5)
                {
                    bOpenStatus = false;
                }
                if (!Option)
                {
                    Port.WriteLine(Commend);
                }
                else
                {
                    byte[] Sendbuff = new byte[Commend.Length];
                    char[] cArTemp = Commend.ToCharArray();

                    for (int i = 0; i < Sendbuff.Length; i++)
                    {
                        Sendbuff[i] = (byte)(cArTemp[i]);
                    }
                    Port.Write(Sendbuff, 0, Sendbuff.Length);
                    //iOpenRetryCount++;
                }

                iOpenRetryCount++;
                rtn = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return rtn;
        }
        #endregion
        #region Serial Handshake
        public Handshake SetPortHandshake(Handshake defaultPortHandshake)
        {
            string handshake;

            Console.WriteLine("Available Handshake options:");
            foreach (string s in Enum.GetNames(typeof(Handshake)))
            {
                Console.WriteLine("   {0}", s);
            }

            Console.Write("Handshake({0}):", defaultPortHandshake.ToString());
            handshake = Console.ReadLine();

            if (handshake == "")
            {
                handshake = defaultPortHandshake.ToString();
            }

            return (Handshake)Enum.Parse(typeof(Handshake), handshake);
        }
        #endregion

        #region  System Serial Port Auto-Searching
        // ★★★★★ 시스템에 Rs232C Port 사용가능한 것 읽어오기
        /// <summary>
        /// Get PortNames
        /// </summary>
        /// <returns>object[] PortsNames</returns>
        public static string[] _FindSerial()
        {
            string[] PortsNames = SerialPort.GetPortNames();
            return PortsNames;
        }

        public static object[] _FindSerial(ref int nPort)
        {
            string[] PortsNames = SerialPort.GetPortNames();
            if (PortsNames.Length == 0)
            {
                nPort = -1;
            }
            else
            {
                nPort = PortsNames.Length;
            }
            return PortsNames;
        }
        #endregion
    }
}
