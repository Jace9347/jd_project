﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP_SSJ_Station
{
    public class clsStation
    {
        /// <summary>
        /// 클래스 초기화
        /// </summary>
        public clsStation()
        {
            Serial.LEN = new byte[2];
            Serial.DAT = new byte[] { };
        }

        /// <summary>
        /// 통신 프로토콜 구조체 선언
        /// </summary>
        public struct Protocol
        {
            public const byte STX = 0x02;
            public const byte ETX = 0x03;
            public const byte DEV = 0x81;
            public const byte CRC = 0xff;
            public const byte DUM = 0x00;
        }

        /// <summary>
        /// 시리얼 통신 데이터 수신 구조체 선언
        /// </summary>
        public struct Serial
        {
            public static byte[] LEN;
            public static byte[] DAT;
        }

        /// <summary>
        /// 통신 명령어 구조체 선언
        /// </summary>
        public struct Cmd
        {
            public const byte LogView = 0x48;

            public const byte Interval = 0x50;//2018.10.03 jace

            public const byte SerialNumber = 0x51;
            public const byte Time = 0x52;
            public const byte SensorDataCount = 0x41;//온도 데이터 요청(전체 페이지)
            public const byte SensorData = 0x42;//온도 해당 페이지 요청
            public const byte Get_Config = 0x45;
            //public const byte Set_Config = 0x47;//2018.10.03 김경환 과장님 요청으로 인한 삭제
            public const byte Set_Config = 0x47;//필요한 명령어 집어 넣을 것
            public const byte Reset = 0x54;//Station Rest

            public const byte Batt = 0x46;
            public const byte Batt_Lavel = 0x57;

            public const byte Mode = 0x55;//2018.10.03 jace

            public const byte Limit_Count = 0x56;//2018.10.03 jace

            public const byte Start = 0x59;
            public const byte Stop = 0x60;

            public const byte Acqisition_Date = 0x58;

            //2018.06.12
            public const byte BattStart = 0x62;//시작
            public const byte Batting = 0x63; //진행(1분 단위)
            public const byte BattCharger = 0x64;//충전
            public const byte SensorStart = 0x61;//센서데이터 to station 진행 이벤트; 수집진행
            public const byte NfcOnOff = 0x65;

            //2018.07.27
            public const byte Status = 0x66;
            public const byte SbReset = 0x6A;

            //2018.08.18 jace add
            //에러 추가 커맨드, PC 만 사용하는 변수
            public const byte Error = 0xff;

            public const byte RemainTimeReset = 0x00;
        }
    }
}
