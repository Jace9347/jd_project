﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP_SSJ_Station
{
    using System.Threading;
    using JDX_Library_X86.Common;
    using System.Windows.Forms;

    public class clsWork
    {
        //Event
        public event RxDataEventHandler RxEvent;
        public delegate void RxDataEventHandler(Category category, byte[] dat);  //통신 데이터 이벤트 핸들러

        public int iTotalPage = 0;//온도 데이터 전체 페이지 개수
        public int iGetPage = 0;//온도 데이터 읽은 페이지 개수
        public int iReadCount = 0;//Read Error Count

        public enum Category
        {
            GetConfig, SetConfig, Start, BattAdc, SetTime, TotalPage, TempData, BattStart, BatCharging, Battfull, Nfc, Stop, Sensing, Debug, Status, SbReset,
            SerialNumber, Time, Error, Interval, Mode, CountLimit, RemainTimeReset
        };

        //public Category category;

        public clsWork()
        {
            com = new clsRS_232C();          

            thWorker = new Thread(new ThreadStart(Run));
            thWorker.Name = "PC Rx Station Module";

            thWorker.Start();
        }

        public struct Def
        {
            public static int iComMax = 1;
        }

        /// <summary>
        /// 온도 데이터 구조체 정의
        /// </summary>
        public struct Dat
        {
            public static int iTotalPage = 0;
            public static int[] iData = new int[32768];
        }

        public Thread thWorker;
        public clsRS_232C com;

        public bool isRxState = false;
        public bool isTxState = false;

        void Run()
        {
            while (true)
            {
                try
                {
                    if (com.Status())
                    {   
                        RX_Station();
                    }
                }
                catch { }
            }
        }

        public bool Open(string port)
        {
            bool ret = false;
            try
            {
                com.Open(port, 115200);
                ret = true;
            }
            catch
            { }
            return ret;
        }

        public bool Status()
        {
            bool ret = false;
            try
            {
                ret = com.Status();
            }
            catch
            { }
            return ret;
        }

        public bool Reset()
        {
            iDataCnt = 0;
            iDebugDataCount = 0;
            iDataLenth = 0;
            com.SerialBuffer = new byte[com.RingCount];
            com.Head = 0;
            com.Tail = 0;
            step = Step.STX;
            return true;
        }

        void RX_Station()
        {
            if (com.Head != com.Tail)
            {
                RX_Progress(com.SerialBuffer[com.Tail++]);
                com.Tail %= com.RingCount;
            }
        }

        public enum Step
        {
            STX, DEV, CMD, LEN, DAT, CRC, ETX
        }

        Step step;
        byte command = 0x00;
        int iLenthCnt = 0;
        int iDataCnt = 0;
        int iDataLenth = 0;

        byte[] B_DebugData = new byte[4096];
        int iDebugDataCount = 0;

        byte[] B_MiChingData = new byte[4096];
        int iMichangDataCount = 0;

        void RX_Progress(byte tmp)
        {
            if (iDebugDataCount >= 1000 || iDataLenth >= com.RingCount)
            {
                byte[] error = new byte[5];
                //error[0] = (byte)'E';
                //error[1] = (byte)'R';
                //error[2] = (byte)'R';
                //error[3] = (byte)'O';
                //error[4] = (byte)'R';
                for (int i = 0; i < 5; i++)
                {
                    error[i] = 255;
                }
                RxEvent(Category.Debug, error);
                iDebugDataCount = 0;

                iDataCnt = 0;
                iDataLenth = 0;
                com.SerialBuffer = new byte[com.RingCount];
                com.Head = 0;
                com.Tail = 0;
                step = Step.STX;
            }
            //B_MiChingData[iMichangDataCount++] = tmp;
            //if (tmp == 0x0a)
            //{
            //    byte[] Buff = new byte[iMichangDataCount];
            //    for (int i = 0; i < iMichangDataCount; i++)
            //    {
            //        Buff[i] = B_MiChingData[i];
            //    }
            //    RxEvent(Category.Debug, Buff);
            //    iMichangDataCount = 0;
            //}
            //else if (iMichangDataCount > 20)
            //{
            //    byte[] Buff = new byte[iMichangDataCount];
            //    for (int i = 0; i < iMichangDataCount; i++)
            //    {
            //        Buff[i] = B_MiChingData[i];
            //    }
            //    RxEvent(Category.Debug, Buff);
            //    iMichangDataCount = 0;
            //}
            //Console.WriteLine(tmp.ToString("X2"));

            switch (step)
            {
                case Step.STX:
                    if (tmp == clsStation.Protocol.STX)
                    {
                        step = Step.DEV;
                    }
                    else
                    {
                        if (iDebugDataCount >= 10 || iDataLenth >= com.RingCount)
                        {
                            byte[] error = new byte[iDebugDataCount];
                            for (int i = 0; i < iDebugDataCount; i++)
                            {
                                error[i] = B_DebugData[i];
                            }
                            RxEvent(Category.Debug, error);
                            iDebugDataCount = 0;

                            iDataCnt = 0;
                            iDataLenth = 0;
                            com.SerialBuffer = new byte[com.RingCount];
                            com.Head = 0;
                            com.Tail = 0;
                            step = Step.STX;
                        }
                    }
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.DEV:
                    if (tmp == clsStation.Protocol.DEV)
                    {
                        step = Step.CMD;
                    }
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.CMD:
                    command = tmp;
                    clsStation.Serial.LEN = new byte[2];
                    iLenthCnt = 0;
                    step = Step.LEN;
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.LEN:
                    clsStation.Serial.LEN[iLenthCnt++] = tmp;
                    if (iLenthCnt == 2)
                    {
                        iDataLenth = (int)clsStation.Serial.LEN[0] << 8 | (int)clsStation.Serial.LEN[1];
                        clsStation.Serial.DAT = new byte[iDataLenth];
                        iDataCnt = 0;
                        step = Step.DAT;
                    }
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.DAT:
                    clsStation.Serial.DAT[iDataCnt++] = tmp;
                    if (iDataCnt == iDataLenth)
                    {   
                        step = Step.CRC;
                    }
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.CRC:
                    if (tmp == clsStation.Protocol.CRC)
                    {
                        step = Step.ETX;
                    }
                    B_DebugData[iDebugDataCount++] = tmp;
                    break;
                case Step.ETX:
                    if (tmp == clsStation.Protocol.ETX)
                    {
                        CMD_Progress(command);

                        iDataCnt = 0;
                        iDebugDataCount = 0;
                        iDataLenth = 0;
                        com.SerialBuffer = new byte[com.RingCount];
                        com.Head = 0;
                        com.Tail = 0;
                        step = Step.STX;
                    }
                    else
                    {
                        iDataCnt = 0;
                        iDebugDataCount = 0;
                        iDataLenth = 0;
                        com.SerialBuffer = new byte[com.RingCount];
                        com.Head = 0;
                        com.Tail = 0;
                        step = Step.STX;

                        byte[] error = new byte[5];
                        error[0] = (byte)'E';
                        error[1] = (byte)'R';
                        error[2] = (byte)'R';
                        error[3] = (byte)'O';
                        error[4] = (byte)'R';
                        RxEvent(Category.Debug, error);
                        iDebugDataCount = 0;
                    }
                    //B_DebugData[iDebugDataCount++] = tmp;
                    //byte[] DatBuff = new byte[iDebugDataCount];
                    //for (int i = 0; i < iDebugDataCount; i++)
                    //{
                    //    DatBuff[i] = B_DebugData[i];
                    //}
                    //RxEvent(Category.Debug, DatBuff);
                    //iDebugDataCount = 0;
                    //B_DebugData = new byte[4096];
                    break;
            }
            isRxState = true;
        }

        #region Command Sort
        void CMD_Progress(byte cmd)
        {
            string data = null;
            switch (cmd)
            {
                case clsStation.Cmd.SbReset:
                    RxEvent(Category.SbReset, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Get_Config:
                    //data = System.Text.Encoding.Default.GetString(clsStation.Serial.DAT);
                    RxEvent(Category.GetConfig, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Set_Config:
                    //data = System.Text.Encoding.Default.GetString(clsStation.Serial.DAT);
                    RxEvent(Category.SetConfig, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.SensorDataCount:
                    //int Total = (int)clsStation.Serial.DAT[0] << 8 | (int)clsStation.Serial.DAT[1];
                    RxEvent(Category.TotalPage, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.SensorData:
                    data = System.Text.Encoding.Default.GetString(clsStation.Serial.DAT);
                    RxEvent(Category.TempData, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Acqisition_Date:
                    RxEvent(Category.SetTime, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Start:
                    RxEvent(Category.Start, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Stop:
                    RxEvent(Category.Stop, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.BattStart:
                    RxEvent(Category.BattStart, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Batting:
                    //RxEvent(Category., clsStation.Serial.DAT);
                    RxEvent(Category.BatCharging, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.BattCharger:
                    RxEvent(Category.BatCharging, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.SensorStart:
                    RxEvent(Category.Sensing, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.NfcOnOff:
                    RxEvent(Category.Nfc, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Status:
                    RxEvent(Category.Status, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.SerialNumber:
                    RxEvent(Category.SerialNumber, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Time:
                    RxEvent(Category.Time, clsStation.Serial.DAT);
                    break;

                //2018.10.03 jace
                case clsStation.Cmd.Interval:
                    RxEvent(Category.Interval, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Limit_Count:
                    RxEvent(Category.CountLimit, clsStation.Serial.DAT);
                    break;
                case clsStation.Cmd.Mode:
                    RxEvent(Category.Mode, clsStation.Serial.DAT);
                    break;

                //2018.10.04 Kim
                case clsStation.Cmd.RemainTimeReset:
                    RxEvent(Category.RemainTimeReset, clsStation.Serial.DAT);
                    break;
            }
        } 
        #endregion

        //송신 프로토콜 정의 함수
        #region Protocol

        //2017.08.02 JACE ADD
        public void Set_SbReset()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.SbReset;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }


        //2018.07.27 JACE ADD
        public void Get_Status()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Status;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_Batt()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Batting;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_Sen_Sta_Status()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.SensorStart;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        //2018.03.05 JACE ADD
        public void Get_AcqisitionDate()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Acqisition_Date;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Set_Date(int year, int month, int day)
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.SerialNumber;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x03;//lenth
            buf[cnt++] = (byte)year;
            buf[cnt++] = (byte)month;
            buf[cnt++] = (byte)day;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Set_Time(int hour, int min, int sec)
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Time;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x03;//lenth
            buf[cnt++] = (byte)hour;
            buf[cnt++] = (byte)min;
            buf[cnt++] = (byte)sec;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Sta_Start()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Start;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }
        //2018.03.05 JACE END

        public void Sta_Stop()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Stop;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_TotalPage()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.SensorDataCount;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_TempData(int page)
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.SensorData;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = (byte)(page >> 8);
            buf[cnt++] = (byte)page;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_Config()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Get_Config;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Get_BattAdc()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Batt;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Set_Reset()
        {
            if (!com.Status())
                return;

            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Reset;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void SetConfigData(string dat)
        {
            if (!com.Status())
                return;
            byte[] tmp = new byte[1000];
            int cnt = 0;
            int datlen = dat.Length;
            tmp[cnt++] = clsStation.Protocol.STX;
            tmp[cnt++] = clsStation.Protocol.DEV;
            tmp[cnt++] = clsStation.Cmd.Set_Config;
            tmp[cnt++] = (byte)(datlen >> 8);
            tmp[cnt++] = (byte)datlen;

            char[] cTemp = dat.ToCharArray();
            for (int i = 0; i < cTemp.Length; i++)
            {
                tmp[cnt++] = (byte)cTemp[i];
            }
            tmp[cnt++] = clsStation.Protocol.CRC;
            tmp[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(tmp, cnt);
            //sSetConfigData = dat;
            isTxState = true;
        }

        //2018.10.03 jace
        public void Set_Mode(int mode, int delay, int temperature)
        {
            if (!com.Status())
                return;
            if (mode > 2 && mode < 0)
                return;
            if (delay < 1 && delay > 9999)
                return;
            if (temperature < 1 && temperature > 999)
                return;


            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Mode;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x05;

            buf[cnt++] = (byte)mode;

            buf[cnt++] = (byte)(delay >> 8);
            buf[cnt++] = (byte)delay;

            buf[cnt++] = (byte)(temperature >> 8);
            buf[cnt++] = (byte)temperature;


            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Set_LimitCount(int sec)
        {
            if (!com.Status())
                return;
            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Limit_Count;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x02;

            buf[cnt++] = (byte)(sec >> 8);
            buf[cnt++] = (byte)sec;

            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        public void Set_Interval(int sec)
        {
            if (!com.Status())
                return;
            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.Interval;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x02;

            buf[cnt++] = (byte)(sec >> 8);
            buf[cnt++] = (byte)sec;

            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }

        //2018.10.04 Kim
        public void RemainTimeReset(int sec)
        {
            if (!com.Status())
                return;
            byte[] buf = new byte[255];
            int cnt = 0;

            buf[cnt++] = clsStation.Protocol.STX;
            buf[cnt++] = clsStation.Protocol.DEV;
            buf[cnt++] = clsStation.Cmd.RemainTimeReset;
            buf[cnt++] = clsStation.Protocol.DUM;
            buf[cnt++] = 0x02;

            buf[cnt++] = (byte)(sec >> 8);
            buf[cnt++] = (byte)sec;

            buf[cnt++] = clsStation.Protocol.CRC;
            buf[cnt++] = clsStation.Protocol.ETX;

            com.SendMsg(buf, cnt);
            isTxState = true;
        }
        #endregion
    }
}
