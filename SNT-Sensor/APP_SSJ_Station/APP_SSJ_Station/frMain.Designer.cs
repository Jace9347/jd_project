﻿namespace APP_SSJ_Station
{
    partial class frMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pTop = new System.Windows.Forms.TableLayoutPanel();
            this.btnExit = new System.Windows.Forms.Button();
            this.lbTitle = new System.Windows.Forms.Label();
            this.lbDateTime = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbTempDebug = new System.Windows.Forms.Label();
            this.lbDataCount = new System.Windows.Forms.Label();
            this.lbDebugMsg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNfcState = new System.Windows.Forms.Button();
            this.btnTx = new System.Windows.Forms.Button();
            this.btnRx = new System.Windows.Forms.Button();
            this.comboBoxSerial = new System.Windows.Forms.ComboBox();
            this.btnSerialReflash = new System.Windows.Forms.Button();
            this.btnSerialOpen = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnResetTemperature = new System.Windows.Forms.Button();
            this.btnReadTemperature = new System.Windows.Forms.Button();
            this.btnEndTemperature = new System.Windows.Forms.Button();
            this.btnGetTemperature = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbSendTime = new System.Windows.Forms.Label();
            this.lbSendRate = new System.Windows.Forms.Label();
            this.pBarSend = new System.Windows.Forms.ProgressBar();
            this.label11 = new System.Windows.Forms.Label();
            this.lbRecivedTime = new System.Windows.Forms.Label();
            this.lbRecivedRate = new System.Windows.Forms.Label();
            this.pBarRecevied = new System.Windows.Forms.ProgressBar();
            this.label38 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbTempTime = new System.Windows.Forms.Label();
            this.lbDiscardRate = new System.Windows.Forms.Label();
            this.pBarDiscard = new System.Windows.Forms.ProgressBar();
            this.lbTemppingTime = new System.Windows.Forms.Label();
            this.lbTempEndTime = new System.Windows.Forms.Label();
            this.lbTempStartTime = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbBattLevel = new System.Windows.Forms.Label();
            this.lbVoltageRate = new System.Windows.Forms.Label();
            this.pBarVoltage = new System.Windows.Forms.ProgressBar();
            this.lbBattState = new System.Windows.Forms.Label();
            this.lbTempGetTime = new System.Windows.Forms.Label();
            this.lbBattTime = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbState4 = new System.Windows.Forms.Label();
            this.lbState3 = new System.Windows.Forms.Label();
            this.lbState2 = new System.Windows.Forms.Label();
            this.lbState1 = new System.Windows.Forms.Label();
            this.lbState0 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbConfigDev = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnLoadData = new System.Windows.Forms.Button();
            this.lbRemainTime = new System.Windows.Forms.Label();
            this.lbTotalUseTime = new System.Windows.Forms.Label();
            this.txtLog11 = new System.Windows.Forms.TextBox();
            this.txtLog10 = new System.Windows.Forms.TextBox();
            this.txtLog9 = new System.Windows.Forms.TextBox();
            this.txtLog7 = new System.Windows.Forms.TextBox();
            this.txtLog8 = new System.Windows.Forms.TextBox();
            this.txtLog6 = new System.Windows.Forms.TextBox();
            this.txtLog5 = new System.Windows.Forms.TextBox();
            this.txtLog4 = new System.Windows.Forms.TextBox();
            this.txtLog3 = new System.Windows.Forms.TextBox();
            this.txtLog2 = new System.Windows.Forms.TextBox();
            this.txtLog1 = new System.Windows.Forms.TextBox();
            this.txtLog0 = new System.Windows.Forms.TextBox();
            this.lbLog11 = new System.Windows.Forms.Label();
            this.lbLog10 = new System.Windows.Forms.Label();
            this.lbLog9 = new System.Windows.Forms.Label();
            this.lbLog8 = new System.Windows.Forms.Label();
            this.lbLog7 = new System.Windows.Forms.Label();
            this.lbLog6 = new System.Windows.Forms.Label();
            this.lbLog5 = new System.Windows.Forms.Label();
            this.lbLog4 = new System.Windows.Forms.Label();
            this.lbLog3 = new System.Windows.Forms.Label();
            this.lbLog2 = new System.Windows.Forms.Label();
            this.lbLog1 = new System.Windows.Forms.Label();
            this.lbLog0 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.chkCalc = new System.Windows.Forms.CheckBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbConfigSetTemperature = new System.Windows.Forms.Label();
            this.lbConfigLimitcount = new System.Windows.Forms.Label();
            this.lbConfigDelaytime = new System.Windows.Forms.Label();
            this.lbConfigCycle = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lbConfigPagenumber = new System.Windows.Forms.Label();
            this.lbConfigTotalpage = new System.Windows.Forms.Label();
            this.lbConfigError = new System.Windows.Forms.Label();
            this.lbConfigMode = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnOffset = new System.Windows.Forms.Button();
            this.btnConfigUser = new System.Windows.Forms.Button();
            this.tmrUpdate = new System.Windows.Forms.Timer(this.components);
            this.pTop.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // pTop
            // 
            this.pTop.BackColor = System.Drawing.Color.Transparent;
            this.pTop.ColumnCount = 3;
            this.pTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.pTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.pTop.Controls.Add(this.btnExit, 2, 0);
            this.pTop.Controls.Add(this.lbTitle, 0, 0);
            this.pTop.Controls.Add(this.lbDateTime, 1, 0);
            this.pTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pTop.Location = new System.Drawing.Point(0, 0);
            this.pTop.Margin = new System.Windows.Forms.Padding(0);
            this.pTop.Name = "pTop";
            this.pTop.RowCount = 1;
            this.pTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pTop.Size = new System.Drawing.Size(1264, 40);
            this.pTop.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::APP_SSJ_Station.Properties.Resources.btnclose;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(1114, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(150, 40);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbTitle
            // 
            this.lbTitle.BackColor = System.Drawing.Color.Transparent;
            this.lbTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lbTitle.Location = new System.Drawing.Point(20, 5);
            this.lbTitle.Margin = new System.Windows.Forms.Padding(20, 5, 20, 5);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(924, 30);
            this.lbTitle.TabIndex = 4;
            this.lbTitle.Text = "Sensor Wafer Measure System";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbTitle.DoubleClick += new System.EventHandler(this.lbTitle_DoubleClick);
            this.lbTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbTitle_MouseDown);
            this.lbTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lbTitle_MouseMove);
            this.lbTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lbTitle_MouseUp);
            // 
            // lbDateTime
            // 
            this.lbDateTime.BackColor = System.Drawing.Color.Transparent;
            this.lbDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDateTime.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbDateTime.Image = global::APP_SSJ_Station.Properties.Resources.BLUE_dot_2;
            this.lbDateTime.Location = new System.Drawing.Point(964, 0);
            this.lbDateTime.Margin = new System.Windows.Forms.Padding(0);
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Size = new System.Drawing.Size(150, 40);
            this.lbDateTime.TabIndex = 6;
            this.lbDateTime.Text = "2018-01-05\r\n14:46:20";
            this.lbDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lbTempDebug, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lbDataCount, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lbDebugMsg, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 1014);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1264, 20);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // lbTempDebug
            // 
            this.lbTempDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTempDebug.Location = new System.Drawing.Point(791, 1);
            this.lbTempDebug.Margin = new System.Windows.Forms.Padding(0);
            this.lbTempDebug.Name = "lbTempDebug";
            this.lbTempDebug.Size = new System.Drawing.Size(120, 18);
            this.lbTempDebug.TabIndex = 9;
            this.lbTempDebug.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDataCount
            // 
            this.lbDataCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDataCount.Location = new System.Drawing.Point(912, 1);
            this.lbDataCount.Margin = new System.Windows.Forms.Padding(0);
            this.lbDataCount.Name = "lbDataCount";
            this.lbDataCount.Size = new System.Drawing.Size(250, 18);
            this.lbDataCount.TabIndex = 7;
            this.lbDataCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDebugMsg
            // 
            this.lbDebugMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDebugMsg.Location = new System.Drawing.Point(102, 1);
            this.lbDebugMsg.Margin = new System.Windows.Forms.Padding(0);
            this.lbDebugMsg.Name = "lbDebugMsg";
            this.lbDebugMsg.Size = new System.Drawing.Size(688, 18);
            this.lbDebugMsg.TabIndex = 9;
            this.lbDebugMsg.Text = "Serial Msg";
            this.lbDebugMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(1163, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Version 3.7";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(1, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "ST/Debug :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel4);
            this.panel1.Controls.Add(this.comboBoxSerial);
            this.panel1.Controls.Add(this.btnSerialReflash);
            this.panel1.Controls.Add(this.btnSerialOpen);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(0, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(206, 215);
            this.panel1.TabIndex = 9;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Controls.Add(this.btnNfcState, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnTx, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnRx, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(24, 172);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(154, 33);
            this.tableLayoutPanel4.TabIndex = 94;
            // 
            // btnNfcState
            // 
            this.btnNfcState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNfcState.Font = new System.Drawing.Font("굴림", 10F);
            this.btnNfcState.Location = new System.Drawing.Point(102, 0);
            this.btnNfcState.Margin = new System.Windows.Forms.Padding(0);
            this.btnNfcState.Name = "btnNfcState";
            this.btnNfcState.Size = new System.Drawing.Size(52, 33);
            this.btnNfcState.TabIndex = 28;
            this.btnNfcState.Text = "NFC";
            this.btnNfcState.UseVisualStyleBackColor = true;
            // 
            // btnTx
            // 
            this.btnTx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTx.Font = new System.Drawing.Font("굴림", 10F);
            this.btnTx.Location = new System.Drawing.Point(0, 0);
            this.btnTx.Margin = new System.Windows.Forms.Padding(0);
            this.btnTx.Name = "btnTx";
            this.btnTx.Size = new System.Drawing.Size(51, 33);
            this.btnTx.TabIndex = 26;
            this.btnTx.Text = "송신";
            this.btnTx.UseVisualStyleBackColor = true;
            // 
            // btnRx
            // 
            this.btnRx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRx.Font = new System.Drawing.Font("굴림", 10F);
            this.btnRx.Location = new System.Drawing.Point(51, 0);
            this.btnRx.Margin = new System.Windows.Forms.Padding(0);
            this.btnRx.Name = "btnRx";
            this.btnRx.Size = new System.Drawing.Size(51, 33);
            this.btnRx.TabIndex = 27;
            this.btnRx.Text = "수신";
            this.btnRx.UseVisualStyleBackColor = true;
            // 
            // comboBoxSerial
            // 
            this.comboBoxSerial.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSerial.FormattingEnabled = true;
            this.comboBoxSerial.Location = new System.Drawing.Point(92, 54);
            this.comboBoxSerial.Name = "comboBoxSerial";
            this.comboBoxSerial.Size = new System.Drawing.Size(79, 24);
            this.comboBoxSerial.TabIndex = 23;
            this.comboBoxSerial.Text = "COM3";
            // 
            // btnSerialReflash
            // 
            this.btnSerialReflash.Font = new System.Drawing.Font("굴림", 10F);
            this.btnSerialReflash.Location = new System.Drawing.Point(24, 131);
            this.btnSerialReflash.Margin = new System.Windows.Forms.Padding(0);
            this.btnSerialReflash.Name = "btnSerialReflash";
            this.btnSerialReflash.Size = new System.Drawing.Size(154, 33);
            this.btnSerialReflash.TabIndex = 25;
            this.btnSerialReflash.Text = "새로고침";
            this.btnSerialReflash.UseVisualStyleBackColor = true;
            this.btnSerialReflash.Click += new System.EventHandler(this.btnSerialReflash_Click);
            // 
            // btnSerialOpen
            // 
            this.btnSerialOpen.Font = new System.Drawing.Font("굴림", 10F);
            this.btnSerialOpen.Location = new System.Drawing.Point(24, 90);
            this.btnSerialOpen.Margin = new System.Windows.Forms.Padding(0);
            this.btnSerialOpen.Name = "btnSerialOpen";
            this.btnSerialOpen.Size = new System.Drawing.Size(154, 33);
            this.btnSerialOpen.TabIndex = 24;
            this.btnSerialOpen.Text = "열기";
            this.btnSerialOpen.UseVisualStyleBackColor = true;
            this.btnSerialOpen.Click += new System.EventHandler(this.btnSerialOpen_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("굴림", 12F);
            this.button8.Location = new System.Drawing.Point(24, 49);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(154, 33);
            this.button8.TabIndex = 23;
            this.button8.Text = "포트 :";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 40);
            this.label5.TabIndex = 13;
            this.label5.Text = "Connection";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnResetTemperature);
            this.panel2.Controls.Add(this.btnReadTemperature);
            this.panel2.Controls.Add(this.btnEndTemperature);
            this.panel2.Controls.Add(this.btnGetTemperature);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(212, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 215);
            this.panel2.TabIndex = 10;
            // 
            // btnResetTemperature
            // 
            this.btnResetTemperature.Font = new System.Drawing.Font("굴림", 10F);
            this.btnResetTemperature.Location = new System.Drawing.Point(13, 172);
            this.btnResetTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.btnResetTemperature.Name = "btnResetTemperature";
            this.btnResetTemperature.Size = new System.Drawing.Size(154, 33);
            this.btnResetTemperature.TabIndex = 22;
            this.btnResetTemperature.Text = "온도 데이터 리셋";
            this.btnResetTemperature.UseVisualStyleBackColor = true;
            this.btnResetTemperature.Click += new System.EventHandler(this.btnResetTemperature_Click);
            // 
            // btnReadTemperature
            // 
            this.btnReadTemperature.Font = new System.Drawing.Font("굴림", 10F);
            this.btnReadTemperature.Location = new System.Drawing.Point(13, 131);
            this.btnReadTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.btnReadTemperature.Name = "btnReadTemperature";
            this.btnReadTemperature.Size = new System.Drawing.Size(154, 33);
            this.btnReadTemperature.TabIndex = 21;
            this.btnReadTemperature.Text = "온도 데이터 읽기";
            this.btnReadTemperature.UseVisualStyleBackColor = true;
            this.btnReadTemperature.Click += new System.EventHandler(this.btnReadTemperature_Click);
            // 
            // btnEndTemperature
            // 
            this.btnEndTemperature.Font = new System.Drawing.Font("굴림", 10F);
            this.btnEndTemperature.Location = new System.Drawing.Point(13, 90);
            this.btnEndTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.btnEndTemperature.Name = "btnEndTemperature";
            this.btnEndTemperature.Size = new System.Drawing.Size(154, 33);
            this.btnEndTemperature.TabIndex = 20;
            this.btnEndTemperature.Text = "온도 수집 종료";
            this.btnEndTemperature.UseVisualStyleBackColor = true;
            this.btnEndTemperature.Click += new System.EventHandler(this.btnEndTemperature_Click);
            // 
            // btnGetTemperature
            // 
            this.btnGetTemperature.Font = new System.Drawing.Font("굴림", 10F);
            this.btnGetTemperature.Location = new System.Drawing.Point(13, 49);
            this.btnGetTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.btnGetTemperature.Name = "btnGetTemperature";
            this.btnGetTemperature.Size = new System.Drawing.Size(154, 33);
            this.btnGetTemperature.TabIndex = 19;
            this.btnGetTemperature.Text = "온도 수집 시작";
            this.btnGetTemperature.UseVisualStyleBackColor = true;
            this.btnGetTemperature.Click += new System.EventHandler(this.btnGetTemperature_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 40);
            this.label9.TabIndex = 17;
            this.label9.Text = "Operation";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("굴림", 12F);
            this.btnSave.Location = new System.Drawing.Point(135, 401);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(360, 76);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "데이터 저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lbSendTime);
            this.panel3.Controls.Add(this.lbSendRate);
            this.panel3.Controls.Add(this.pBarSend);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.lbRecivedTime);
            this.panel3.Controls.Add(this.lbRecivedRate);
            this.panel3.Controls.Add(this.pBarRecevied);
            this.panel3.Controls.Add(this.label38);
            this.panel3.Controls.Add(this.tableLayoutPanel3);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(0, 264);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(760, 259);
            this.panel3.TabIndex = 11;
            // 
            // lbSendTime
            // 
            this.lbSendTime.BackColor = System.Drawing.Color.White;
            this.lbSendTime.Location = new System.Drawing.Point(244, 48);
            this.lbSendTime.Margin = new System.Windows.Forms.Padding(0);
            this.lbSendTime.Name = "lbSendTime";
            this.lbSendTime.Size = new System.Drawing.Size(437, 15);
            this.lbSendTime.TabIndex = 93;
            this.lbSendTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSendRate
            // 
            this.lbSendRate.Font = new System.Drawing.Font("굴림", 12F);
            this.lbSendRate.Location = new System.Drawing.Point(705, 44);
            this.lbSendRate.Name = "lbSendRate";
            this.lbSendRate.Size = new System.Drawing.Size(63, 23);
            this.lbSendRate.TabIndex = 92;
            this.lbSendRate.Text = "100%";
            this.lbSendRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBarSend
            // 
            this.pBarSend.Location = new System.Drawing.Point(215, 44);
            this.pBarSend.Name = "pBarSend";
            this.pBarSend.Size = new System.Drawing.Size(484, 23);
            this.pBarSend.TabIndex = 91;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(5, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(216, 23);
            this.label11.TabIndex = 94;
            this.label11.Text = "SOW to Station Data 전송율 :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbRecivedTime
            // 
            this.lbRecivedTime.BackColor = System.Drawing.Color.White;
            this.lbRecivedTime.Location = new System.Drawing.Point(244, 80);
            this.lbRecivedTime.Margin = new System.Windows.Forms.Padding(0);
            this.lbRecivedTime.Name = "lbRecivedTime";
            this.lbRecivedTime.Size = new System.Drawing.Size(437, 15);
            this.lbRecivedTime.TabIndex = 83;
            this.lbRecivedTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbRecivedRate
            // 
            this.lbRecivedRate.Font = new System.Drawing.Font("굴림", 12F);
            this.lbRecivedRate.Location = new System.Drawing.Point(705, 76);
            this.lbRecivedRate.Name = "lbRecivedRate";
            this.lbRecivedRate.Size = new System.Drawing.Size(63, 23);
            this.lbRecivedRate.TabIndex = 36;
            this.lbRecivedRate.Text = "100%";
            this.lbRecivedRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBarRecevied
            // 
            this.pBarRecevied.Location = new System.Drawing.Point(215, 76);
            this.pBarRecevied.Name = "pBarRecevied";
            this.pBarRecevied.Size = new System.Drawing.Size(484, 23);
            this.pBarRecevied.TabIndex = 35;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("굴림", 12F);
            this.label38.Location = new System.Drawing.Point(5, 76);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(216, 23);
            this.label38.TabIndex = 90;
            this.label38.Text = "Station to PC Data 수신율 :";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 115);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(754, 141);
            this.tableLayoutPanel3.TabIndex = 17;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.lbTempTime);
            this.groupBox2.Controls.Add(this.lbDiscardRate);
            this.groupBox2.Controls.Add(this.pBarDiscard);
            this.groupBox2.Controls.Add(this.lbTemppingTime);
            this.groupBox2.Controls.Add(this.lbTempEndTime);
            this.groupBox2.Controls.Add(this.lbTempStartTime);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(380, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 135);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            // 
            // lbTempTime
            // 
            this.lbTempTime.BackColor = System.Drawing.Color.White;
            this.lbTempTime.Location = new System.Drawing.Point(169, 23);
            this.lbTempTime.Margin = new System.Windows.Forms.Padding(0);
            this.lbTempTime.Name = "lbTempTime";
            this.lbTempTime.Size = new System.Drawing.Size(140, 15);
            this.lbTempTime.TabIndex = 95;
            this.lbTempTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDiscardRate
            // 
            this.lbDiscardRate.Font = new System.Drawing.Font("굴림", 12F);
            this.lbDiscardRate.Location = new System.Drawing.Point(320, 19);
            this.lbDiscardRate.Name = "lbDiscardRate";
            this.lbDiscardRate.Size = new System.Drawing.Size(63, 23);
            this.lbDiscardRate.TabIndex = 35;
            this.lbDiscardRate.Text = "100%";
            this.lbDiscardRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBarDiscard
            // 
            this.pBarDiscard.Location = new System.Drawing.Point(160, 19);
            this.pBarDiscard.Name = "pBarDiscard";
            this.pBarDiscard.Size = new System.Drawing.Size(156, 23);
            this.pBarDiscard.TabIndex = 32;
            // 
            // lbTemppingTime
            // 
            this.lbTemppingTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbTemppingTime.Location = new System.Drawing.Point(178, 106);
            this.lbTemppingTime.Name = "lbTemppingTime";
            this.lbTemppingTime.Size = new System.Drawing.Size(138, 23);
            this.lbTemppingTime.TabIndex = 31;
            this.lbTemppingTime.Text = "0시간 00분 00초";
            this.lbTemppingTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTempEndTime
            // 
            this.lbTempEndTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbTempEndTime.Location = new System.Drawing.Point(178, 77);
            this.lbTempEndTime.Name = "lbTempEndTime";
            this.lbTempEndTime.Size = new System.Drawing.Size(138, 23);
            this.lbTempEndTime.TabIndex = 30;
            this.lbTempEndTime.Text = "00시 00분 00초";
            this.lbTempEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTempStartTime
            // 
            this.lbTempStartTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbTempStartTime.Location = new System.Drawing.Point(178, 48);
            this.lbTempStartTime.Name = "lbTempStartTime";
            this.lbTempStartTime.Size = new System.Drawing.Size(138, 23);
            this.lbTempStartTime.TabIndex = 29;
            this.lbTempStartTime.Text = "00시 00분 00초";
            this.lbTempStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("굴림", 12F);
            this.label23.Location = new System.Drawing.Point(16, 106);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(156, 23);
            this.label23.TabIndex = 28;
            this.label23.Text = "온도수집 진행 시간 :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("굴림", 12F);
            this.label26.Location = new System.Drawing.Point(16, 19);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(156, 23);
            this.label26.TabIndex = 25;
            this.label26.Text = "온도 수집 진행율 :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("굴림", 12F);
            this.label24.Location = new System.Drawing.Point(16, 77);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(156, 23);
            this.label24.TabIndex = 27;
            this.label24.Text = "온도수집 종료시간 :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("굴림", 12F);
            this.label25.Location = new System.Drawing.Point(16, 48);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(156, 23);
            this.label25.TabIndex = 26;
            this.label25.Text = "온도수집 시작시간 :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.lbBattLevel);
            this.groupBox1.Controls.Add(this.lbVoltageRate);
            this.groupBox1.Controls.Add(this.pBarVoltage);
            this.groupBox1.Controls.Add(this.lbBattState);
            this.groupBox1.Controls.Add(this.lbTempGetTime);
            this.groupBox1.Controls.Add(this.lbBattTime);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 135);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // lbBattLevel
            // 
            this.lbBattLevel.BackColor = System.Drawing.Color.White;
            this.lbBattLevel.Location = new System.Drawing.Point(148, 23);
            this.lbBattLevel.Margin = new System.Windows.Forms.Padding(0);
            this.lbBattLevel.Name = "lbBattLevel";
            this.lbBattLevel.Size = new System.Drawing.Size(140, 15);
            this.lbBattLevel.TabIndex = 94;
            this.lbBattLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbVoltageRate
            // 
            this.lbVoltageRate.Font = new System.Drawing.Font("굴림", 12F);
            this.lbVoltageRate.Location = new System.Drawing.Point(302, 19);
            this.lbVoltageRate.Name = "lbVoltageRate";
            this.lbVoltageRate.Size = new System.Drawing.Size(63, 23);
            this.lbVoltageRate.TabIndex = 34;
            this.lbVoltageRate.Text = "100%";
            this.lbVoltageRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pBarVoltage
            // 
            this.pBarVoltage.Location = new System.Drawing.Point(140, 19);
            this.pBarVoltage.Name = "pBarVoltage";
            this.pBarVoltage.Size = new System.Drawing.Size(156, 23);
            this.pBarVoltage.TabIndex = 33;
            // 
            // lbBattState
            // 
            this.lbBattState.Font = new System.Drawing.Font("굴림", 12F);
            this.lbBattState.Location = new System.Drawing.Point(204, 108);
            this.lbBattState.Name = "lbBattState";
            this.lbBattState.Size = new System.Drawing.Size(138, 23);
            this.lbBattState.TabIndex = 24;
            this.lbBattState.Text = "매우 양호";
            this.lbBattState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTempGetTime
            // 
            this.lbTempGetTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbTempGetTime.Location = new System.Drawing.Point(204, 79);
            this.lbTempGetTime.Name = "lbTempGetTime";
            this.lbTempGetTime.Size = new System.Drawing.Size(138, 23);
            this.lbTempGetTime.TabIndex = 23;
            this.lbTempGetTime.Text = "0시간 00분 00초";
            this.lbTempGetTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbBattTime
            // 
            this.lbBattTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbBattTime.Location = new System.Drawing.Point(204, 50);
            this.lbBattTime.Name = "lbBattTime";
            this.lbBattTime.Size = new System.Drawing.Size(138, 23);
            this.lbBattTime.TabIndex = 22;
            this.lbBattTime.Text = "0시간 00분 00초";
            this.lbBattTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("굴림", 12F);
            this.label19.Location = new System.Drawing.Point(16, 106);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(156, 23);
            this.label19.TabIndex = 20;
            this.label19.Text = "배터리 상태 :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("굴림", 12F);
            this.label18.Location = new System.Drawing.Point(16, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(156, 23);
            this.label18.TabIndex = 19;
            this.label18.Text = "온도 수집 가능시간 :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("굴림", 12F);
            this.label17.Location = new System.Drawing.Point(16, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(156, 23);
            this.label17.TabIndex = 18;
            this.label17.Text = "충전 예상 소요시간 :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("굴림", 12F);
            this.label16.Location = new System.Drawing.Point(16, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 23);
            this.label16.TabIndex = 17;
            this.label16.Text = "배터리 레벨 :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lbState4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbState3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbState2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbState1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbState0, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(153, 8);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(604, 32);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // lbState4
            // 
            this.lbState4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbState4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbState4.Font = new System.Drawing.Font("굴림", 12F);
            this.lbState4.Location = new System.Drawing.Point(481, 1);
            this.lbState4.Margin = new System.Windows.Forms.Padding(0);
            this.lbState4.Name = "lbState4";
            this.lbState4.Size = new System.Drawing.Size(122, 30);
            this.lbState4.TabIndex = 20;
            this.lbState4.Text = "대기중";
            this.lbState4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbState3
            // 
            this.lbState3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbState3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbState3.Font = new System.Drawing.Font("굴림", 12F);
            this.lbState3.Location = new System.Drawing.Point(361, 1);
            this.lbState3.Margin = new System.Windows.Forms.Padding(0);
            this.lbState3.Name = "lbState3";
            this.lbState3.Size = new System.Drawing.Size(119, 30);
            this.lbState3.TabIndex = 19;
            this.lbState3.Text = "충전중";
            this.lbState3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbState2
            // 
            this.lbState2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbState2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbState2.Font = new System.Drawing.Font("굴림", 12F);
            this.lbState2.Location = new System.Drawing.Point(241, 1);
            this.lbState2.Margin = new System.Windows.Forms.Padding(0);
            this.lbState2.Name = "lbState2";
            this.lbState2.Size = new System.Drawing.Size(119, 30);
            this.lbState2.TabIndex = 18;
            this.lbState2.Text = "데이터 변환중";
            this.lbState2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbState1
            // 
            this.lbState1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbState1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbState1.Font = new System.Drawing.Font("굴림", 12F);
            this.lbState1.Location = new System.Drawing.Point(121, 1);
            this.lbState1.Margin = new System.Windows.Forms.Padding(0);
            this.lbState1.Name = "lbState1";
            this.lbState1.Size = new System.Drawing.Size(119, 30);
            this.lbState1.TabIndex = 17;
            this.lbState1.Text = "데이터 전송중";
            this.lbState1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbState0
            // 
            this.lbState0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbState0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbState0.Font = new System.Drawing.Font("굴림", 12F);
            this.lbState0.Location = new System.Drawing.Point(1, 1);
            this.lbState0.Margin = new System.Windows.Forms.Padding(0);
            this.lbState0.Name = "lbState0";
            this.lbState0.Size = new System.Drawing.Size(119, 30);
            this.lbState0.TabIndex = 16;
            this.lbState0.Text = "온도 수집중";
            this.lbState0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 40);
            this.label6.TabIndex = 14;
            this.label6.Text = "Status";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.dgvData);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Location = new System.Drawing.Point(0, 529);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1264, 483);
            this.panel4.TabIndex = 12;
            // 
            // dgvData
            // 
            this.dgvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(0, 40);
            this.dgvData.Margin = new System.Windows.Forms.Padding(0);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowTemplate.Height = 23;
            this.dgvData.Size = new System.Drawing.Size(1264, 443);
            this.dgvData.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 40);
            this.label8.TabIndex = 16;
            this.label8.Text = "Temperature";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 40);
            this.label7.TabIndex = 15;
            this.label7.Text = "Information";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label7_MouseDown);
            this.label7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label7_MouseUp);
            // 
            // lbConfigDev
            // 
            this.lbConfigDev.BackColor = System.Drawing.Color.White;
            this.lbConfigDev.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbConfigDev.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lbConfigDev.Location = new System.Drawing.Point(0, 0);
            this.lbConfigDev.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigDev.Name = "lbConfigDev";
            this.lbConfigDev.Size = new System.Drawing.Size(150, 40);
            this.lbConfigDev.TabIndex = 18;
            this.lbConfigDev.Text = "Config";
            this.lbConfigDev.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbConfigDev.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbConfigDev_MouseDown);
            this.lbConfigDev.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lbConfigDev_MouseUp);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnLoadData);
            this.panel5.Controls.Add(this.lbRemainTime);
            this.panel5.Controls.Add(this.lbTotalUseTime);
            this.panel5.Controls.Add(this.txtLog11);
            this.panel5.Controls.Add(this.txtLog10);
            this.panel5.Controls.Add(this.txtLog9);
            this.panel5.Controls.Add(this.txtLog7);
            this.panel5.Controls.Add(this.txtLog8);
            this.panel5.Controls.Add(this.txtLog6);
            this.panel5.Controls.Add(this.txtLog5);
            this.panel5.Controls.Add(this.txtLog4);
            this.panel5.Controls.Add(this.txtLog3);
            this.panel5.Controls.Add(this.txtLog2);
            this.panel5.Controls.Add(this.txtLog1);
            this.panel5.Controls.Add(this.txtLog0);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Controls.Add(this.lbLog11);
            this.panel5.Controls.Add(this.lbLog10);
            this.panel5.Controls.Add(this.lbLog9);
            this.panel5.Controls.Add(this.lbLog8);
            this.panel5.Controls.Add(this.lbLog7);
            this.panel5.Controls.Add(this.lbLog6);
            this.panel5.Controls.Add(this.lbLog5);
            this.panel5.Controls.Add(this.lbLog4);
            this.panel5.Controls.Add(this.lbLog3);
            this.panel5.Controls.Add(this.lbLog2);
            this.panel5.Controls.Add(this.lbLog1);
            this.panel5.Controls.Add(this.lbLog0);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Location = new System.Drawing.Point(763, 43);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(500, 481);
            this.panel5.TabIndex = 12;
            // 
            // btnLoadData
            // 
            this.btnLoadData.Font = new System.Drawing.Font("굴림", 12F);
            this.btnLoadData.Location = new System.Drawing.Point(0, 401);
            this.btnLoadData.Margin = new System.Windows.Forms.Padding(0);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(135, 76);
            this.btnLoadData.TabIndex = 94;
            this.btnLoadData.Text = "불러오기";
            this.btnLoadData.UseVisualStyleBackColor = true;
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // lbRemainTime
            // 
            this.lbRemainTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbRemainTime.Location = new System.Drawing.Point(12, 359);
            this.lbRemainTime.Name = "lbRemainTime";
            this.lbRemainTime.Size = new System.Drawing.Size(477, 23);
            this.lbRemainTime.TabIndex = 90;
            this.lbRemainTime.Text = "남은 사용 시간 :";
            this.lbRemainTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTotalUseTime
            // 
            this.lbTotalUseTime.Font = new System.Drawing.Font("굴림", 12F);
            this.lbTotalUseTime.Location = new System.Drawing.Point(12, 325);
            this.lbTotalUseTime.Name = "lbTotalUseTime";
            this.lbTotalUseTime.Size = new System.Drawing.Size(477, 23);
            this.lbTotalUseTime.TabIndex = 89;
            this.lbTotalUseTime.Text = "누적 사용 시간 :";
            this.lbTotalUseTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLog11
            // 
            this.txtLog11.Location = new System.Drawing.Point(135, 277);
            this.txtLog11.Name = "txtLog11";
            this.txtLog11.Size = new System.Drawing.Size(360, 21);
            this.txtLog11.TabIndex = 85;
            this.txtLog11.Text = "Celsius";
            // 
            // txtLog10
            // 
            this.txtLog10.Location = new System.Drawing.Point(135, 256);
            this.txtLog10.Name = "txtLog10";
            this.txtLog10.Size = new System.Drawing.Size(360, 21);
            this.txtLog10.TabIndex = 84;
            this.txtLog10.Text = "Temperature";
            // 
            // txtLog9
            // 
            this.txtLog9.Location = new System.Drawing.Point(135, 235);
            this.txtLog9.Name = "txtLog9";
            this.txtLog9.Size = new System.Drawing.Size(360, 21);
            this.txtLog9.TabIndex = 83;
            // 
            // txtLog7
            // 
            this.txtLog7.Location = new System.Drawing.Point(135, 193);
            this.txtLog7.Name = "txtLog7";
            this.txtLog7.Size = new System.Drawing.Size(360, 21);
            this.txtLog7.TabIndex = 86;
            // 
            // txtLog8
            // 
            this.txtLog8.Location = new System.Drawing.Point(135, 214);
            this.txtLog8.Name = "txtLog8";
            this.txtLog8.Size = new System.Drawing.Size(360, 21);
            this.txtLog8.TabIndex = 82;
            this.txtLog8.Text = "CH1";
            // 
            // txtLog6
            // 
            this.txtLog6.Location = new System.Drawing.Point(135, 172);
            this.txtLog6.Name = "txtLog6";
            this.txtLog6.Size = new System.Drawing.Size(360, 21);
            this.txtLog6.TabIndex = 81;
            // 
            // txtLog5
            // 
            this.txtLog5.Location = new System.Drawing.Point(135, 151);
            this.txtLog5.Name = "txtLog5";
            this.txtLog5.ReadOnly = true;
            this.txtLog5.Size = new System.Drawing.Size(360, 21);
            this.txtLog5.TabIndex = 80;
            // 
            // txtLog4
            // 
            this.txtLog4.Location = new System.Drawing.Point(135, 130);
            this.txtLog4.Name = "txtLog4";
            this.txtLog4.Size = new System.Drawing.Size(360, 21);
            this.txtLog4.TabIndex = 79;
            // 
            // txtLog3
            // 
            this.txtLog3.Location = new System.Drawing.Point(135, 109);
            this.txtLog3.Name = "txtLog3";
            this.txtLog3.Size = new System.Drawing.Size(360, 21);
            this.txtLog3.TabIndex = 78;
            this.txtLog3.Text = "270";
            // 
            // txtLog2
            // 
            this.txtLog2.Location = new System.Drawing.Point(135, 88);
            this.txtLog2.Name = "txtLog2";
            this.txtLog2.Size = new System.Drawing.Size(360, 21);
            this.txtLog2.TabIndex = 77;
            this.txtLog2.Text = "300";
            // 
            // txtLog1
            // 
            this.txtLog1.Location = new System.Drawing.Point(135, 67);
            this.txtLog1.Name = "txtLog1";
            this.txtLog1.Size = new System.Drawing.Size(360, 21);
            this.txtLog1.TabIndex = 76;
            this.txtLog1.Text = "SensArray INtegral Wafer";
            // 
            // txtLog0
            // 
            this.txtLog0.Location = new System.Drawing.Point(135, 46);
            this.txtLog0.Name = "txtLog0";
            this.txtLog0.Size = new System.Drawing.Size(360, 21);
            this.txtLog0.TabIndex = 75;
            this.txtLog0.Text = "102668SE";
            // 
            // lbLog11
            // 
            this.lbLog11.Location = new System.Drawing.Point(6, 277);
            this.lbLog11.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog11.Name = "lbLog11";
            this.lbLog11.Size = new System.Drawing.Size(141, 21);
            this.lbLog11.TabIndex = 74;
            this.lbLog11.Text = "Units";
            this.lbLog11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog10
            // 
            this.lbLog10.Location = new System.Drawing.Point(6, 256);
            this.lbLog10.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog10.Name = "lbLog10";
            this.lbLog10.Size = new System.Drawing.Size(141, 21);
            this.lbLog10.TabIndex = 73;
            this.lbLog10.Text = "Data Type";
            this.lbLog10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog9
            // 
            this.lbLog9.Location = new System.Drawing.Point(6, 235);
            this.lbLog9.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog9.Name = "lbLog9";
            this.lbLog9.Size = new System.Drawing.Size(141, 21);
            this.lbLog9.TabIndex = 72;
            this.lbLog9.Text = "Comments";
            this.lbLog9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog8
            // 
            this.lbLog8.Location = new System.Drawing.Point(6, 214);
            this.lbLog8.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog8.Name = "lbLog8";
            this.lbLog8.Size = new System.Drawing.Size(141, 21);
            this.lbLog8.TabIndex = 71;
            this.lbLog8.Text = "Chamber";
            this.lbLog8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog7
            // 
            this.lbLog7.Location = new System.Drawing.Point(6, 193);
            this.lbLog7.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog7.Name = "lbLog7";
            this.lbLog7.Size = new System.Drawing.Size(141, 21);
            this.lbLog7.TabIndex = 70;
            this.lbLog7.Text = "Tool";
            this.lbLog7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog6
            // 
            this.lbLog6.Location = new System.Drawing.Point(6, 172);
            this.lbLog6.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog6.Name = "lbLog6";
            this.lbLog6.Size = new System.Drawing.Size(141, 21);
            this.lbLog6.TabIndex = 69;
            this.lbLog6.Text = "Run By";
            this.lbLog6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog5
            // 
            this.lbLog5.Location = new System.Drawing.Point(6, 151);
            this.lbLog5.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog5.Name = "lbLog5";
            this.lbLog5.Size = new System.Drawing.Size(141, 21);
            this.lbLog5.TabIndex = 68;
            this.lbLog5.Text = "File Name";
            this.lbLog5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog4
            // 
            this.lbLog4.Location = new System.Drawing.Point(6, 130);
            this.lbLog4.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog4.Name = "lbLog4";
            this.lbLog4.Size = new System.Drawing.Size(141, 21);
            this.lbLog4.TabIndex = 67;
            this.lbLog4.Text = "Acquisition Date";
            this.lbLog4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog3
            // 
            this.lbLog3.Location = new System.Drawing.Point(6, 106);
            this.lbLog3.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog3.Name = "lbLog3";
            this.lbLog3.Size = new System.Drawing.Size(141, 27);
            this.lbLog3.TabIndex = 66;
            this.lbLog3.Text = "Wafer Notch Location\r\n  (CCW form + X axis)";
            this.lbLog3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog2
            // 
            this.lbLog2.Location = new System.Drawing.Point(6, 87);
            this.lbLog2.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog2.Name = "lbLog2";
            this.lbLog2.Size = new System.Drawing.Size(141, 21);
            this.lbLog2.TabIndex = 65;
            this.lbLog2.Text = "Wafer Diameter";
            this.lbLog2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog1
            // 
            this.lbLog1.Location = new System.Drawing.Point(6, 67);
            this.lbLog1.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog1.Name = "lbLog1";
            this.lbLog1.Size = new System.Drawing.Size(141, 21);
            this.lbLog1.TabIndex = 64;
            this.lbLog1.Text = "Product Type";
            this.lbLog1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbLog0
            // 
            this.lbLog0.Location = new System.Drawing.Point(6, 46);
            this.lbLog0.Margin = new System.Windows.Forms.Padding(0);
            this.lbLog0.Name = "lbLog0";
            this.lbLog0.Size = new System.Drawing.Size(141, 21);
            this.lbLog0.TabIndex = 63;
            this.lbLog0.Text = "Wafer ID";
            this.lbLog0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.chkCalc);
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.btnOffset);
            this.panel6.Controls.Add(this.btnConfigUser);
            this.panel6.Controls.Add(this.lbConfigDev);
            this.panel6.Location = new System.Drawing.Point(399, 43);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(361, 215);
            this.panel6.TabIndex = 13;
            // 
            // chkCalc
            // 
            this.chkCalc.AutoSize = true;
            this.chkCalc.Font = new System.Drawing.Font("굴림", 20F);
            this.chkCalc.Location = new System.Drawing.Point(155, 9);
            this.chkCalc.Name = "chkCalc";
            this.chkCalc.Size = new System.Drawing.Size(193, 31);
            this.chkCalc.TabIndex = 27;
            this.chkCalc.Text = "켈리브레이션";
            this.chkCalc.UseVisualStyleBackColor = true;
            this.chkCalc.CheckedChanged += new System.EventHandler(this.chkCalc_CheckedChanged);
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.label21);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.lbConfigSetTemperature);
            this.panel8.Controls.Add(this.lbConfigLimitcount);
            this.panel8.Controls.Add(this.lbConfigDelaytime);
            this.panel8.Controls.Add(this.lbConfigCycle);
            this.panel8.Location = new System.Drawing.Point(181, 86);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(178, 126);
            this.panel8.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(4, 95);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 21);
            this.label20.TabIndex = 86;
            this.label20.Text = "설정 온도 :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(4, 66);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 21);
            this.label21.TabIndex = 85;
            this.label21.Text = "제한 횟수 :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(4, 37);
            this.label22.Margin = new System.Windows.Forms.Padding(0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 21);
            this.label22.TabIndex = 84;
            this.label22.Text = "측정 지연 시간 :";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(4, 8);
            this.label27.Margin = new System.Windows.Forms.Padding(0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(96, 21);
            this.label27.TabIndex = 83;
            this.label27.Text = "온도 수집 주기 :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigSetTemperature
            // 
            this.lbConfigSetTemperature.Location = new System.Drawing.Point(100, 95);
            this.lbConfigSetTemperature.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigSetTemperature.Name = "lbConfigSetTemperature";
            this.lbConfigSetTemperature.Size = new System.Drawing.Size(73, 21);
            this.lbConfigSetTemperature.TabIndex = 82;
            this.lbConfigSetTemperature.Text = "0";
            this.lbConfigSetTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigLimitcount
            // 
            this.lbConfigLimitcount.Location = new System.Drawing.Point(100, 66);
            this.lbConfigLimitcount.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigLimitcount.Name = "lbConfigLimitcount";
            this.lbConfigLimitcount.Size = new System.Drawing.Size(73, 21);
            this.lbConfigLimitcount.TabIndex = 81;
            this.lbConfigLimitcount.Text = "0";
            this.lbConfigLimitcount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigDelaytime
            // 
            this.lbConfigDelaytime.Location = new System.Drawing.Point(100, 37);
            this.lbConfigDelaytime.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigDelaytime.Name = "lbConfigDelaytime";
            this.lbConfigDelaytime.Size = new System.Drawing.Size(73, 21);
            this.lbConfigDelaytime.TabIndex = 80;
            this.lbConfigDelaytime.Text = "0";
            this.lbConfigDelaytime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigCycle
            // 
            this.lbConfigCycle.Location = new System.Drawing.Point(100, 8);
            this.lbConfigCycle.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigCycle.Name = "lbConfigCycle";
            this.lbConfigCycle.Size = new System.Drawing.Size(73, 21);
            this.lbConfigCycle.TabIndex = 79;
            this.lbConfigCycle.Text = "0";
            this.lbConfigCycle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lbConfigPagenumber);
            this.panel7.Controls.Add(this.lbConfigTotalpage);
            this.panel7.Controls.Add(this.lbConfigError);
            this.panel7.Controls.Add(this.lbConfigMode);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Location = new System.Drawing.Point(3, 86);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(178, 126);
            this.panel7.TabIndex = 25;
            // 
            // lbConfigPagenumber
            // 
            this.lbConfigPagenumber.Location = new System.Drawing.Point(90, 97);
            this.lbConfigPagenumber.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigPagenumber.Name = "lbConfigPagenumber";
            this.lbConfigPagenumber.Size = new System.Drawing.Size(79, 21);
            this.lbConfigPagenumber.TabIndex = 78;
            this.lbConfigPagenumber.Text = "0";
            this.lbConfigPagenumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigTotalpage
            // 
            this.lbConfigTotalpage.Location = new System.Drawing.Point(111, 66);
            this.lbConfigTotalpage.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigTotalpage.Name = "lbConfigTotalpage";
            this.lbConfigTotalpage.Size = new System.Drawing.Size(58, 21);
            this.lbConfigTotalpage.TabIndex = 77;
            this.lbConfigTotalpage.Text = "0";
            this.lbConfigTotalpage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigError
            // 
            this.lbConfigError.Location = new System.Drawing.Point(111, 39);
            this.lbConfigError.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigError.Name = "lbConfigError";
            this.lbConfigError.Size = new System.Drawing.Size(58, 21);
            this.lbConfigError.TabIndex = 76;
            this.lbConfigError.Text = "0";
            this.lbConfigError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbConfigMode
            // 
            this.lbConfigMode.Location = new System.Drawing.Point(81, 3);
            this.lbConfigMode.Margin = new System.Windows.Forms.Padding(0);
            this.lbConfigMode.Name = "lbConfigMode";
            this.lbConfigMode.Size = new System.Drawing.Size(96, 34);
            this.lbConfigMode.TabIndex = 75;
            this.lbConfigMode.Text = "0";
            this.lbConfigMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(6, 95);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 21);
            this.label12.TabIndex = 82;
            this.label12.Text = "데이터 번호 :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(6, 66);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 21);
            this.label13.TabIndex = 81;
            this.label13.Text = "전체 데이터 개수 :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(6, 37);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 21);
            this.label14.TabIndex = 80;
            this.label14.Text = "에러 횟수 :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(6, 8);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 21);
            this.label15.TabIndex = 79;
            this.label15.Text = "측정 모드 :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnOffset
            // 
            this.btnOffset.Font = new System.Drawing.Font("굴림", 10F);
            this.btnOffset.Location = new System.Drawing.Point(181, 50);
            this.btnOffset.Margin = new System.Windows.Forms.Padding(0);
            this.btnOffset.Name = "btnOffset";
            this.btnOffset.Size = new System.Drawing.Size(154, 33);
            this.btnOffset.TabIndex = 24;
            this.btnOffset.Text = "보정 설정";
            this.btnOffset.UseVisualStyleBackColor = true;
            this.btnOffset.Click += new System.EventHandler(this.btnOffset_Click);
            // 
            // btnConfigUser
            // 
            this.btnConfigUser.Font = new System.Drawing.Font("굴림", 10F);
            this.btnConfigUser.Location = new System.Drawing.Point(27, 50);
            this.btnConfigUser.Margin = new System.Windows.Forms.Padding(0);
            this.btnConfigUser.Name = "btnConfigUser";
            this.btnConfigUser.Size = new System.Drawing.Size(154, 33);
            this.btnConfigUser.TabIndex = 23;
            this.btnConfigUser.Text = "설정 변경";
            this.btnConfigUser.UseVisualStyleBackColor = true;
            this.btnConfigUser.Click += new System.EventHandler(this.btnConfigUser_Click_1);
            // 
            // tmrUpdate
            // 
            this.tmrUpdate.Enabled = true;
            this.tmrUpdate.Tick += new System.EventHandler(this.tmrUpdate_Tick);
            // 
            // frMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = global::APP_SSJ_Station.Properties.Resources.BLUE_line_11;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1264, 1034);
            this.ControlBox = false;
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.pTop);
            this.Name = "frMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frMain_FormClosing);
            this.Load += new System.EventHandler(this.frMain_Load);
            this.pTop.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pTop;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.Label lbDateTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lbTempDebug;
        private System.Windows.Forms.Label lbDataCount;
        public System.Windows.Forms.Label lbDebugMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBoxSerial;
        private System.Windows.Forms.Button btnRx;
        private System.Windows.Forms.Button btnTx;
        private System.Windows.Forms.Button btnSerialReflash;
        private System.Windows.Forms.Button btnSerialOpen;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnResetTemperature;
        private System.Windows.Forms.Button btnReadTemperature;
        private System.Windows.Forms.Button btnEndTemperature;
        private System.Windows.Forms.Button btnGetTemperature;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbState4;
        private System.Windows.Forms.Label lbState3;
        private System.Windows.Forms.Label lbState2;
        private System.Windows.Forms.Label lbState1;
        private System.Windows.Forms.Label lbState0;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbConfigDev;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txtLog7;
        private System.Windows.Forms.Label lbLog11;
        private System.Windows.Forms.TextBox txtLog11;
        private System.Windows.Forms.Label lbLog10;
        private System.Windows.Forms.TextBox txtLog10;
        private System.Windows.Forms.Label lbLog9;
        private System.Windows.Forms.TextBox txtLog9;
        private System.Windows.Forms.Label lbLog8;
        private System.Windows.Forms.TextBox txtLog8;
        private System.Windows.Forms.Label lbLog7;
        private System.Windows.Forms.TextBox txtLog6;
        private System.Windows.Forms.Label lbLog6;
        private System.Windows.Forms.TextBox txtLog5;
        private System.Windows.Forms.Label lbLog5;
        private System.Windows.Forms.TextBox txtLog4;
        private System.Windows.Forms.Label lbLog4;
        private System.Windows.Forms.TextBox txtLog3;
        private System.Windows.Forms.Label lbLog3;
        private System.Windows.Forms.TextBox txtLog2;
        private System.Windows.Forms.Label lbLog2;
        private System.Windows.Forms.TextBox txtLog1;
        private System.Windows.Forms.Label lbLog1;
        private System.Windows.Forms.TextBox txtLog0;
        private System.Windows.Forms.Label lbLog0;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbBattState;
        private System.Windows.Forms.Label lbTempGetTime;
        private System.Windows.Forms.Label lbBattTime;
        private System.Windows.Forms.Label lbTemppingTime;
        private System.Windows.Forms.Label lbTempEndTime;
        private System.Windows.Forms.Label lbTempStartTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Timer tmrUpdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lbDiscardRate;
        private System.Windows.Forms.ProgressBar pBarDiscard;
        private System.Windows.Forms.Label lbVoltageRate;
        private System.Windows.Forms.ProgressBar pBarVoltage;
        private System.Windows.Forms.Button btnOffset;
        private System.Windows.Forms.Button btnConfigUser;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lbConfigSetTemperature;
        private System.Windows.Forms.Label lbConfigLimitcount;
        private System.Windows.Forms.Label lbConfigDelaytime;
        private System.Windows.Forms.Label lbConfigCycle;
        public System.Windows.Forms.Label lbConfigPagenumber;
        public System.Windows.Forms.Label lbConfigTotalpage;
        private System.Windows.Forms.Label lbConfigError;
        private System.Windows.Forms.Label lbConfigMode;
        private System.Windows.Forms.Label lbRecivedRate;
        private System.Windows.Forms.ProgressBar pBarRecevied;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lbRemainTime;
        private System.Windows.Forms.Label lbTotalUseTime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbRecivedTime;
        private System.Windows.Forms.Label lbTempTime;
        private System.Windows.Forms.Label lbBattLevel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnNfcState;
        private System.Windows.Forms.Label lbSendTime;
        private System.Windows.Forms.Label lbSendRate;
        private System.Windows.Forms.ProgressBar pBarSend;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkCalc;
        private System.Windows.Forms.Button btnLoadData;
    }
}

