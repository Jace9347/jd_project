﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP_SSJ_Station
{
    class clsHextoBcd
    {
        public static int Hex2Bcd(int h)
        {
            int y;
            y = (h / 10) << 4;
            y = y | (h % 10);
            return (y);
            //int r = 0;
            //string dat = x.ToString("X2");
            //r = int.Parse(dat);
            //return r;
        }

        public static int Bcd2Hex(int b)
        {
            int r = 0;
            string dat = b.ToString("X2");
            r = int.Parse(dat);
            return r;
        }
    }
}
