﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP_SSJ_Station
{
    public partial class frConfigDev : Form
    {
        public frConfigDev()
        {
            InitializeComponent();
        }

        public frConfigDev(frMain _this)
        {
            InitializeComponent();

            main = _this;

            _Array();

            if (main.work.Status())
            {
                main.bUserConfig = false;
                main.work.Get_Config();
            }
        }

        frMain main;
        static int iMax = 24;
        TextBox[] txtSetStatus = new TextBox[iMax];
        Label[] lbGetStatus = new Label[iMax];

        void _Array()
        {
            lbGetStatus[0] = lbGetStatus0; txtSetStatus[0] = txtSetStatus0;
            lbGetStatus[1] = lbGetStatus1; txtSetStatus[1] = txtSetStatus1;
            lbGetStatus[2] = lbGetStatus2; txtSetStatus[2] = txtSetStatus2;
            lbGetStatus[3] = lbGetStatus3; txtSetStatus[3] = txtSetStatus3;
            lbGetStatus[4] = lbGetStatus4; txtSetStatus[4] = txtSetStatus4;
            lbGetStatus[5] = lbGetStatus5; txtSetStatus[5] = txtSetStatus5;
            lbGetStatus[6] = lbGetStatus6; txtSetStatus[6] = txtSetStatus6;
            lbGetStatus[7] = lbGetStatus7; txtSetStatus[7] = txtSetStatus7;
            lbGetStatus[8] = lbGetStatus8; txtSetStatus[8] = txtSetStatus8;
            lbGetStatus[9] = lbGetStatus9; txtSetStatus[9] = txtSetStatus9;
            lbGetStatus[10] = lbGetStatus10; txtSetStatus[10] = txtSetStatus10;
            lbGetStatus[11] = lbGetStatus11; txtSetStatus[11] = txtSetStatus11;
            lbGetStatus[12] = lbGetStatus12; txtSetStatus[12] = txtSetStatus12;
            lbGetStatus[13] = lbGetStatus13; txtSetStatus[13] = txtSetStatus13;
            lbGetStatus[14] = lbGetStatus14; txtSetStatus[14] = txtSetStatus14;
            lbGetStatus[15] = lbGetStatus15; txtSetStatus[15] = txtSetStatus15;
            lbGetStatus[16] = lbGetStatus16; txtSetStatus[16] = txtSetStatus16;
            lbGetStatus[17] = lbGetStatus17; txtSetStatus[17] = txtSetStatus17;
            lbGetStatus[18] = lbGetStatus18; txtSetStatus[18] = txtSetStatus18;
            lbGetStatus[19] = lbGetStatus19; txtSetStatus[19] = txtSetStatus19;
            lbGetStatus[20] = lbGetStatus20; txtSetStatus[20] = txtSetStatus20;
            lbGetStatus[21] = lbGetStatus21; txtSetStatus[21] = txtSetStatus21;
            lbGetStatus[22] = lbGetStatus22; txtSetStatus[22] = txtSetStatus22;
            lbGetStatus[23] = lbGetStatus23; txtSetStatus[23] = txtSetStatus23;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            string[] config = new string[] { "S:", "M:", "B:", "RC:", "EC:", "SN:", "PN:", "TP:", "T:", "IV:", "V:", "R:", "DR:", "CL:", "DL:", "CN:", "ST:", "SS:", "UC:", "UT:", "MT:", "WT:", "CC:", "ID:" };

        //S:xx/M:xx/B:xx/RC:xxx/EC:xxx/SN:xx,xx,xx/PN:xxxxx,xxxxx/TP:xxxxx/T:xx,xx,xx/IV:xxxx/V:x.x.x/R:x/DR:x/CL:xx/DL:xxxx/CN:xxxx/ST:xxx/SS:x/UC:xxxxx/UT:xxxxx/MT:xxxxx/WT:xxxxxxxx/CC:xxxxx/ID:xxxxx/

                                                       string configdata = null;

            for (int i = 0; i < config.Length; i++)
            {
                configdata += config[i] + txtSetStatus[i].Text + "/";
            }
            main.work.SetConfigData(configdata);
        }

        public void Status(string dat)
        {
            dat = dat.Replace("\0", null);

            string[] buff = dat.Split('/');
            int[] iLenth = new int[buff.Length];

            for (int i = 0; i < buff.Length; i++)
            {
                int last = buff[i].LastIndexOf(':');
                if (!string.IsNullOrEmpty(buff[i]))
                {
                    try
                    {
                        lbGetStatus[i].Text = buff[i].Substring(last + 1);
                        txtSetStatus[i].Text = lbGetStatus[i].Text;
                        iLenth[i] = txtSetStatus[i].Text.Length;
                    }
                    catch
                    { }
                }
            }
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            main.work.Get_Config();
        }

        private void frConfig_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (!txtSetStatus2.ReadOnly)
                {
                    //txtSetStatus0.ReadOnly = false;
                    //txtSetStatus1.ReadOnly = false;

                    txtSetStatus2.ReadOnly = true;
                    txtSetStatus3.ReadOnly = true;
                    txtSetStatus4.ReadOnly = true;
                    txtSetStatus5.ReadOnly = true;
                    txtSetStatus6.ReadOnly = true;
                    txtSetStatus7.ReadOnly = true;
                    txtSetStatus8.ReadOnly = true;

                    //txtSetStatus9.ReadOnly = false;

                    txtSetStatus10.ReadOnly = true;
                    txtSetStatus11.ReadOnly = true;
                    txtSetStatus12.ReadOnly = true;
                    txtSetStatus13.ReadOnly = true;

                    //txtSetStatus14.ReadOnly = false;
                    //txtSetStatus15.ReadOnly = false;
                    //txtSetStatus16.ReadOnly = false;
                }
                else
                {
                    //txtSetStatus0.ReadOnly = true;
                    //txtSetStatus1.ReadOnly = true;

                    txtSetStatus2.ReadOnly = false;
                    txtSetStatus3.ReadOnly = false;
                    txtSetStatus4.ReadOnly = false;
                    txtSetStatus5.ReadOnly = false;
                    txtSetStatus6.ReadOnly = false;
                    txtSetStatus7.ReadOnly = false;
                    txtSetStatus8.ReadOnly = false;

                    //txtSetStatus9.ReadOnly = true;

                    txtSetStatus10.ReadOnly = false;
                    txtSetStatus11.ReadOnly = false;
                    txtSetStatus12.ReadOnly = false;
                    txtSetStatus13.ReadOnly = false;

                    //txtSetStatus14.ReadOnly = true;
                    //txtSetStatus15.ReadOnly = true;
                    //txtSetStatus16.ReadOnly = true;
                }
            }
        }
    }
}
