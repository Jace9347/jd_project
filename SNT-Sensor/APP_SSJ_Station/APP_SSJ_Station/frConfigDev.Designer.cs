﻿namespace APP_SSJ_Station
{
    partial class frConfigDev
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGet = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.txtSetStatus16 = new System.Windows.Forms.TextBox();
            this.txtSetStatus15 = new System.Windows.Forms.TextBox();
            this.txtSetStatus14 = new System.Windows.Forms.TextBox();
            this.txtSetStatus13 = new System.Windows.Forms.TextBox();
            this.txtSetStatus12 = new System.Windows.Forms.TextBox();
            this.txtSetStatus11 = new System.Windows.Forms.TextBox();
            this.txtSetStatus10 = new System.Windows.Forms.TextBox();
            this.txtSetStatus9 = new System.Windows.Forms.TextBox();
            this.txtSetStatus8 = new System.Windows.Forms.TextBox();
            this.txtSetStatus7 = new System.Windows.Forms.TextBox();
            this.txtSetStatus6 = new System.Windows.Forms.TextBox();
            this.txtSetStatus5 = new System.Windows.Forms.TextBox();
            this.txtSetStatus4 = new System.Windows.Forms.TextBox();
            this.txtSetStatus3 = new System.Windows.Forms.TextBox();
            this.txtSetStatus2 = new System.Windows.Forms.TextBox();
            this.txtSetStatus1 = new System.Windows.Forms.TextBox();
            this.txtSetStatus0 = new System.Windows.Forms.TextBox();
            this.lbGetStatus16 = new System.Windows.Forms.Label();
            this.lbGetStatus15 = new System.Windows.Forms.Label();
            this.lbGetStatus14 = new System.Windows.Forms.Label();
            this.lbGetStatus13 = new System.Windows.Forms.Label();
            this.lbGetStatus12 = new System.Windows.Forms.Label();
            this.lbGetStatus11 = new System.Windows.Forms.Label();
            this.lbGetStatus10 = new System.Windows.Forms.Label();
            this.lbGetStatus8 = new System.Windows.Forms.Label();
            this.lbGetStatus7 = new System.Windows.Forms.Label();
            this.lbGetStatus6 = new System.Windows.Forms.Label();
            this.lbGetStatus5 = new System.Windows.Forms.Label();
            this.lbGetStatus4 = new System.Windows.Forms.Label();
            this.lbGetStatus3 = new System.Windows.Forms.Label();
            this.lbGetStatus2 = new System.Windows.Forms.Label();
            this.lbGetStatus1 = new System.Windows.Forms.Label();
            this.lbGetStatus0 = new System.Windows.Forms.Label();
            this.lbGetStatus9 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnConfig = new System.Windows.Forms.Panel();
            this.txtSetStatus23 = new System.Windows.Forms.TextBox();
            this.lbGetStatus23 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSetStatus22 = new System.Windows.Forms.TextBox();
            this.lbGetStatus22 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtSetStatus21 = new System.Windows.Forms.TextBox();
            this.lbGetStatus21 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSetStatus20 = new System.Windows.Forms.TextBox();
            this.lbGetStatus20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSetStatus19 = new System.Windows.Forms.TextBox();
            this.lbGetStatus19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSetStatus18 = new System.Windows.Forms.TextBox();
            this.lbGetStatus18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSetStatus17 = new System.Windows.Forms.TextBox();
            this.lbGetStatus17 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGet
            // 
            this.btnGet.Font = new System.Drawing.Font("굴림", 25F);
            this.btnGet.Location = new System.Drawing.Point(564, 9);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(146, 222);
            this.btnGet.TabIndex = 84;
            this.btnGet.Text = "Read";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("굴림", 25F);
            this.btnClose.Location = new System.Drawing.Point(564, 807);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(146, 88);
            this.btnClose.TabIndex = 83;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSet
            // 
            this.btnSet.Enabled = false;
            this.btnSet.Font = new System.Drawing.Font("굴림", 25F);
            this.btnSet.Location = new System.Drawing.Point(564, 268);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(146, 222);
            this.btnSet.TabIndex = 82;
            this.btnSet.Text = "Send";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // txtSetStatus16
            // 
            this.txtSetStatus16.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus16.Location = new System.Drawing.Point(313, 599);
            this.txtSetStatus16.Name = "txtSetStatus16";
            this.txtSetStatus16.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus16.TabIndex = 81;
            // 
            // txtSetStatus15
            // 
            this.txtSetStatus15.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus15.Location = new System.Drawing.Point(313, 562);
            this.txtSetStatus15.Name = "txtSetStatus15";
            this.txtSetStatus15.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus15.TabIndex = 80;
            // 
            // txtSetStatus14
            // 
            this.txtSetStatus14.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus14.Location = new System.Drawing.Point(313, 525);
            this.txtSetStatus14.Name = "txtSetStatus14";
            this.txtSetStatus14.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus14.TabIndex = 79;
            // 
            // txtSetStatus13
            // 
            this.txtSetStatus13.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus13.Location = new System.Drawing.Point(313, 488);
            this.txtSetStatus13.Name = "txtSetStatus13";
            this.txtSetStatus13.ReadOnly = true;
            this.txtSetStatus13.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus13.TabIndex = 78;
            // 
            // txtSetStatus12
            // 
            this.txtSetStatus12.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus12.Location = new System.Drawing.Point(313, 451);
            this.txtSetStatus12.Name = "txtSetStatus12";
            this.txtSetStatus12.ReadOnly = true;
            this.txtSetStatus12.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus12.TabIndex = 77;
            // 
            // txtSetStatus11
            // 
            this.txtSetStatus11.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus11.Location = new System.Drawing.Point(313, 414);
            this.txtSetStatus11.Name = "txtSetStatus11";
            this.txtSetStatus11.ReadOnly = true;
            this.txtSetStatus11.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus11.TabIndex = 76;
            // 
            // txtSetStatus10
            // 
            this.txtSetStatus10.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus10.Location = new System.Drawing.Point(313, 377);
            this.txtSetStatus10.Name = "txtSetStatus10";
            this.txtSetStatus10.ReadOnly = true;
            this.txtSetStatus10.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus10.TabIndex = 75;
            // 
            // txtSetStatus9
            // 
            this.txtSetStatus9.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus9.Location = new System.Drawing.Point(313, 340);
            this.txtSetStatus9.Name = "txtSetStatus9";
            this.txtSetStatus9.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus9.TabIndex = 74;
            // 
            // txtSetStatus8
            // 
            this.txtSetStatus8.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus8.Location = new System.Drawing.Point(313, 303);
            this.txtSetStatus8.Name = "txtSetStatus8";
            this.txtSetStatus8.ReadOnly = true;
            this.txtSetStatus8.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus8.TabIndex = 73;
            // 
            // txtSetStatus7
            // 
            this.txtSetStatus7.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus7.Location = new System.Drawing.Point(313, 266);
            this.txtSetStatus7.Name = "txtSetStatus7";
            this.txtSetStatus7.ReadOnly = true;
            this.txtSetStatus7.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus7.TabIndex = 72;
            // 
            // txtSetStatus6
            // 
            this.txtSetStatus6.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus6.Location = new System.Drawing.Point(313, 229);
            this.txtSetStatus6.Name = "txtSetStatus6";
            this.txtSetStatus6.ReadOnly = true;
            this.txtSetStatus6.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus6.TabIndex = 71;
            // 
            // txtSetStatus5
            // 
            this.txtSetStatus5.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus5.Location = new System.Drawing.Point(313, 192);
            this.txtSetStatus5.Name = "txtSetStatus5";
            this.txtSetStatus5.ReadOnly = true;
            this.txtSetStatus5.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus5.TabIndex = 70;
            // 
            // txtSetStatus4
            // 
            this.txtSetStatus4.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus4.Location = new System.Drawing.Point(313, 155);
            this.txtSetStatus4.Name = "txtSetStatus4";
            this.txtSetStatus4.ReadOnly = true;
            this.txtSetStatus4.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus4.TabIndex = 69;
            // 
            // txtSetStatus3
            // 
            this.txtSetStatus3.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus3.Location = new System.Drawing.Point(313, 118);
            this.txtSetStatus3.Name = "txtSetStatus3";
            this.txtSetStatus3.ReadOnly = true;
            this.txtSetStatus3.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus3.TabIndex = 68;
            // 
            // txtSetStatus2
            // 
            this.txtSetStatus2.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus2.Location = new System.Drawing.Point(313, 81);
            this.txtSetStatus2.Name = "txtSetStatus2";
            this.txtSetStatus2.ReadOnly = true;
            this.txtSetStatus2.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus2.TabIndex = 67;
            // 
            // txtSetStatus1
            // 
            this.txtSetStatus1.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus1.Location = new System.Drawing.Point(313, 44);
            this.txtSetStatus1.Name = "txtSetStatus1";
            this.txtSetStatus1.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus1.TabIndex = 66;
            // 
            // txtSetStatus0
            // 
            this.txtSetStatus0.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus0.Location = new System.Drawing.Point(313, 7);
            this.txtSetStatus0.Name = "txtSetStatus0";
            this.txtSetStatus0.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus0.TabIndex = 65;
            // 
            // lbGetStatus16
            // 
            this.lbGetStatus16.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus16.Location = new System.Drawing.Point(155, 601);
            this.lbGetStatus16.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus16.Name = "lbGetStatus16";
            this.lbGetStatus16.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus16.TabIndex = 64;
            this.lbGetStatus16.Text = "GET DATA";
            this.lbGetStatus16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus15
            // 
            this.lbGetStatus15.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus15.Location = new System.Drawing.Point(155, 564);
            this.lbGetStatus15.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus15.Name = "lbGetStatus15";
            this.lbGetStatus15.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus15.TabIndex = 63;
            this.lbGetStatus15.Text = "GET DATA";
            this.lbGetStatus15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus14
            // 
            this.lbGetStatus14.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus14.Location = new System.Drawing.Point(155, 527);
            this.lbGetStatus14.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus14.Name = "lbGetStatus14";
            this.lbGetStatus14.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus14.TabIndex = 62;
            this.lbGetStatus14.Text = "GET DATA";
            this.lbGetStatus14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus13
            // 
            this.lbGetStatus13.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus13.Location = new System.Drawing.Point(155, 490);
            this.lbGetStatus13.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus13.Name = "lbGetStatus13";
            this.lbGetStatus13.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus13.TabIndex = 61;
            this.lbGetStatus13.Text = "GET DATA";
            this.lbGetStatus13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus12
            // 
            this.lbGetStatus12.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus12.Location = new System.Drawing.Point(155, 453);
            this.lbGetStatus12.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus12.Name = "lbGetStatus12";
            this.lbGetStatus12.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus12.TabIndex = 60;
            this.lbGetStatus12.Text = "GET DATA";
            this.lbGetStatus12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus11
            // 
            this.lbGetStatus11.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus11.Location = new System.Drawing.Point(155, 416);
            this.lbGetStatus11.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus11.Name = "lbGetStatus11";
            this.lbGetStatus11.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus11.TabIndex = 59;
            this.lbGetStatus11.Text = "GET DATA";
            this.lbGetStatus11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus10
            // 
            this.lbGetStatus10.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus10.Location = new System.Drawing.Point(155, 379);
            this.lbGetStatus10.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus10.Name = "lbGetStatus10";
            this.lbGetStatus10.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus10.TabIndex = 58;
            this.lbGetStatus10.Text = "GET DATA";
            this.lbGetStatus10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus8
            // 
            this.lbGetStatus8.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus8.Location = new System.Drawing.Point(155, 305);
            this.lbGetStatus8.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus8.Name = "lbGetStatus8";
            this.lbGetStatus8.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus8.TabIndex = 57;
            this.lbGetStatus8.Text = "GET DATA";
            this.lbGetStatus8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus7
            // 
            this.lbGetStatus7.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus7.Location = new System.Drawing.Point(155, 268);
            this.lbGetStatus7.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus7.Name = "lbGetStatus7";
            this.lbGetStatus7.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus7.TabIndex = 56;
            this.lbGetStatus7.Text = "GET DATA";
            this.lbGetStatus7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus6
            // 
            this.lbGetStatus6.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus6.Location = new System.Drawing.Point(155, 231);
            this.lbGetStatus6.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus6.Name = "lbGetStatus6";
            this.lbGetStatus6.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus6.TabIndex = 55;
            this.lbGetStatus6.Text = "GET DATA";
            this.lbGetStatus6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus5
            // 
            this.lbGetStatus5.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus5.Location = new System.Drawing.Point(155, 194);
            this.lbGetStatus5.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus5.Name = "lbGetStatus5";
            this.lbGetStatus5.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus5.TabIndex = 54;
            this.lbGetStatus5.Text = "GET DATA";
            this.lbGetStatus5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus4
            // 
            this.lbGetStatus4.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus4.Location = new System.Drawing.Point(155, 157);
            this.lbGetStatus4.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus4.Name = "lbGetStatus4";
            this.lbGetStatus4.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus4.TabIndex = 53;
            this.lbGetStatus4.Text = "GET DATA";
            this.lbGetStatus4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus3
            // 
            this.lbGetStatus3.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus3.Location = new System.Drawing.Point(155, 120);
            this.lbGetStatus3.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus3.Name = "lbGetStatus3";
            this.lbGetStatus3.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus3.TabIndex = 52;
            this.lbGetStatus3.Text = "GET DATA";
            this.lbGetStatus3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus2
            // 
            this.lbGetStatus2.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus2.Location = new System.Drawing.Point(155, 83);
            this.lbGetStatus2.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus2.Name = "lbGetStatus2";
            this.lbGetStatus2.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus2.TabIndex = 51;
            this.lbGetStatus2.Text = "GET DATA";
            this.lbGetStatus2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus1
            // 
            this.lbGetStatus1.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus1.Location = new System.Drawing.Point(155, 46);
            this.lbGetStatus1.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus1.Name = "lbGetStatus1";
            this.lbGetStatus1.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus1.TabIndex = 50;
            this.lbGetStatus1.Text = "GET DATA";
            this.lbGetStatus1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus0
            // 
            this.lbGetStatus0.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus0.Location = new System.Drawing.Point(155, 9);
            this.lbGetStatus0.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus0.Name = "lbGetStatus0";
            this.lbGetStatus0.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus0.TabIndex = 49;
            this.lbGetStatus0.Text = "GET DATA";
            this.lbGetStatus0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGetStatus9
            // 
            this.lbGetStatus9.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus9.Location = new System.Drawing.Point(155, 342);
            this.lbGetStatus9.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus9.Name = "lbGetStatus9";
            this.lbGetStatus9.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus9.TabIndex = 48;
            this.lbGetStatus9.Text = "GET DATA";
            this.lbGetStatus9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("굴림", 12F);
            this.label21.Location = new System.Drawing.Point(0, 601);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(155, 37);
            this.label21.TabIndex = 47;
            this.label21.Text = "Temp Limit : ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("굴림", 12F);
            this.label20.Location = new System.Drawing.Point(0, 564);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(155, 37);
            this.label20.TabIndex = 46;
            this.label20.Text = "Count Limit : ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("굴림", 12F);
            this.label19.Location = new System.Drawing.Point(0, 527);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(155, 37);
            this.label19.TabIndex = 45;
            this.label19.Text = "Delay : ";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("굴림", 12F);
            this.label18.Location = new System.Drawing.Point(0, 490);
            this.label18.Margin = new System.Windows.Forms.Padding(0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(155, 37);
            this.label18.TabIndex = 44;
            this.label18.Text = "Battery Cut Off Level : ";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("굴림", 12F);
            this.label17.Location = new System.Drawing.Point(0, 453);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(155, 37);
            this.label17.TabIndex = 43;
            this.label17.Text = "Data Ready : ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("굴림", 12F);
            this.label16.Location = new System.Drawing.Point(0, 416);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(155, 37);
            this.label16.TabIndex = 42;
            this.label16.Text = "Reset(Sensor BD) : ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("굴림", 12F);
            this.label14.Location = new System.Drawing.Point(0, 379);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(155, 37);
            this.label14.TabIndex = 40;
            this.label14.Text = "Version : ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("굴림", 12F);
            this.label13.Location = new System.Drawing.Point(0, 305);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(155, 37);
            this.label13.TabIndex = 39;
            this.label13.Text = "Time : ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("굴림", 12F);
            this.label12.Location = new System.Drawing.Point(0, 268);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 37);
            this.label12.TabIndex = 38;
            this.label12.Text = "Total Page : ";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("굴림", 12F);
            this.label11.Location = new System.Drawing.Point(0, 231);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(155, 37);
            this.label11.TabIndex = 37;
            this.label11.Text = "Page Number : ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("굴림", 12F);
            this.label10.Location = new System.Drawing.Point(0, 194);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 37);
            this.label10.TabIndex = 36;
            this.label10.Text = "Serial Number : ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("굴림", 12F);
            this.label9.Location = new System.Drawing.Point(0, 157);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 37);
            this.label9.TabIndex = 35;
            this.label9.Text = "Error Count : ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("굴림", 12F);
            this.label8.Location = new System.Drawing.Point(0, 120);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(155, 37);
            this.label8.TabIndex = 34;
            this.label8.Text = "Read Count : ";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("굴림", 12F);
            this.label7.Location = new System.Drawing.Point(0, 83);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 37);
            this.label7.TabIndex = 33;
            this.label7.Text = "Battery Level : ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("굴림", 12F);
            this.label6.Location = new System.Drawing.Point(0, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 37);
            this.label6.TabIndex = 8;
            this.label6.Text = "Mode : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("굴림", 12F);
            this.label5.Location = new System.Drawing.Point(0, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 37);
            this.label5.TabIndex = 7;
            this.label5.Text = "Status : ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnConfig
            // 
            this.pnConfig.Controls.Add(this.txtSetStatus23);
            this.pnConfig.Controls.Add(this.lbGetStatus23);
            this.pnConfig.Controls.Add(this.label31);
            this.pnConfig.Controls.Add(this.txtSetStatus22);
            this.pnConfig.Controls.Add(this.lbGetStatus22);
            this.pnConfig.Controls.Add(this.label29);
            this.pnConfig.Controls.Add(this.txtSetStatus21);
            this.pnConfig.Controls.Add(this.lbGetStatus21);
            this.pnConfig.Controls.Add(this.label27);
            this.pnConfig.Controls.Add(this.txtSetStatus20);
            this.pnConfig.Controls.Add(this.lbGetStatus20);
            this.pnConfig.Controls.Add(this.label25);
            this.pnConfig.Controls.Add(this.txtSetStatus19);
            this.pnConfig.Controls.Add(this.lbGetStatus19);
            this.pnConfig.Controls.Add(this.label23);
            this.pnConfig.Controls.Add(this.txtSetStatus18);
            this.pnConfig.Controls.Add(this.lbGetStatus18);
            this.pnConfig.Controls.Add(this.label15);
            this.pnConfig.Controls.Add(this.txtSetStatus17);
            this.pnConfig.Controls.Add(this.lbGetStatus17);
            this.pnConfig.Controls.Add(this.label2);
            this.pnConfig.Controls.Add(this.btnGet);
            this.pnConfig.Controls.Add(this.btnClose);
            this.pnConfig.Controls.Add(this.btnSet);
            this.pnConfig.Controls.Add(this.txtSetStatus16);
            this.pnConfig.Controls.Add(this.txtSetStatus15);
            this.pnConfig.Controls.Add(this.txtSetStatus14);
            this.pnConfig.Controls.Add(this.txtSetStatus13);
            this.pnConfig.Controls.Add(this.txtSetStatus12);
            this.pnConfig.Controls.Add(this.txtSetStatus11);
            this.pnConfig.Controls.Add(this.txtSetStatus10);
            this.pnConfig.Controls.Add(this.txtSetStatus9);
            this.pnConfig.Controls.Add(this.txtSetStatus8);
            this.pnConfig.Controls.Add(this.txtSetStatus7);
            this.pnConfig.Controls.Add(this.txtSetStatus6);
            this.pnConfig.Controls.Add(this.txtSetStatus5);
            this.pnConfig.Controls.Add(this.txtSetStatus4);
            this.pnConfig.Controls.Add(this.txtSetStatus3);
            this.pnConfig.Controls.Add(this.txtSetStatus2);
            this.pnConfig.Controls.Add(this.txtSetStatus1);
            this.pnConfig.Controls.Add(this.txtSetStatus0);
            this.pnConfig.Controls.Add(this.lbGetStatus16);
            this.pnConfig.Controls.Add(this.lbGetStatus15);
            this.pnConfig.Controls.Add(this.lbGetStatus14);
            this.pnConfig.Controls.Add(this.lbGetStatus13);
            this.pnConfig.Controls.Add(this.lbGetStatus12);
            this.pnConfig.Controls.Add(this.lbGetStatus11);
            this.pnConfig.Controls.Add(this.lbGetStatus10);
            this.pnConfig.Controls.Add(this.lbGetStatus8);
            this.pnConfig.Controls.Add(this.lbGetStatus7);
            this.pnConfig.Controls.Add(this.lbGetStatus6);
            this.pnConfig.Controls.Add(this.lbGetStatus5);
            this.pnConfig.Controls.Add(this.lbGetStatus4);
            this.pnConfig.Controls.Add(this.lbGetStatus3);
            this.pnConfig.Controls.Add(this.lbGetStatus2);
            this.pnConfig.Controls.Add(this.lbGetStatus1);
            this.pnConfig.Controls.Add(this.lbGetStatus0);
            this.pnConfig.Controls.Add(this.lbGetStatus9);
            this.pnConfig.Controls.Add(this.label21);
            this.pnConfig.Controls.Add(this.label20);
            this.pnConfig.Controls.Add(this.label19);
            this.pnConfig.Controls.Add(this.label18);
            this.pnConfig.Controls.Add(this.label17);
            this.pnConfig.Controls.Add(this.label16);
            this.pnConfig.Controls.Add(this.label14);
            this.pnConfig.Controls.Add(this.label13);
            this.pnConfig.Controls.Add(this.label12);
            this.pnConfig.Controls.Add(this.label11);
            this.pnConfig.Controls.Add(this.label10);
            this.pnConfig.Controls.Add(this.label9);
            this.pnConfig.Controls.Add(this.label8);
            this.pnConfig.Controls.Add(this.label7);
            this.pnConfig.Controls.Add(this.label6);
            this.pnConfig.Controls.Add(this.label5);
            this.pnConfig.Controls.Add(this.label4);
            this.pnConfig.Location = new System.Drawing.Point(9, 9);
            this.pnConfig.Margin = new System.Windows.Forms.Padding(0);
            this.pnConfig.Name = "pnConfig";
            this.pnConfig.Size = new System.Drawing.Size(731, 908);
            this.pnConfig.TabIndex = 7;
            // 
            // txtSetStatus23
            // 
            this.txtSetStatus23.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus23.Location = new System.Drawing.Point(313, 858);
            this.txtSetStatus23.Name = "txtSetStatus23";
            this.txtSetStatus23.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus23.TabIndex = 105;
            // 
            // lbGetStatus23
            // 
            this.lbGetStatus23.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus23.Location = new System.Drawing.Point(155, 860);
            this.lbGetStatus23.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus23.Name = "lbGetStatus23";
            this.lbGetStatus23.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus23.TabIndex = 104;
            this.lbGetStatus23.Text = "GET DATA";
            this.lbGetStatus23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("굴림", 12F);
            this.label31.Location = new System.Drawing.Point(0, 860);
            this.label31.Margin = new System.Windows.Forms.Padding(0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(155, 37);
            this.label31.TabIndex = 103;
            this.label31.Text = "Wafer ID : ";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus22
            // 
            this.txtSetStatus22.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus22.Location = new System.Drawing.Point(313, 821);
            this.txtSetStatus22.Name = "txtSetStatus22";
            this.txtSetStatus22.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus22.TabIndex = 102;
            // 
            // lbGetStatus22
            // 
            this.lbGetStatus22.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus22.Location = new System.Drawing.Point(155, 823);
            this.lbGetStatus22.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus22.Name = "lbGetStatus22";
            this.lbGetStatus22.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus22.TabIndex = 101;
            this.lbGetStatus22.Text = "GET DATA";
            this.lbGetStatus22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("굴림", 12F);
            this.label29.Location = new System.Drawing.Point(0, 823);
            this.label29.Margin = new System.Windows.Forms.Padding(0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(155, 37);
            this.label29.TabIndex = 100;
            this.label29.Text = "Cut Count : ";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus21
            // 
            this.txtSetStatus21.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus21.Location = new System.Drawing.Point(313, 784);
            this.txtSetStatus21.Name = "txtSetStatus21";
            this.txtSetStatus21.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus21.TabIndex = 99;
            // 
            // lbGetStatus21
            // 
            this.lbGetStatus21.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus21.Location = new System.Drawing.Point(155, 786);
            this.lbGetStatus21.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus21.Name = "lbGetStatus21";
            this.lbGetStatus21.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus21.TabIndex = 98;
            this.lbGetStatus21.Text = "GET DATA";
            this.lbGetStatus21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("굴림", 12F);
            this.label27.Location = new System.Drawing.Point(0, 786);
            this.label27.Margin = new System.Windows.Forms.Padding(0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(155, 37);
            this.label27.TabIndex = 97;
            this.label27.Text = "Wake Time : ";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus20
            // 
            this.txtSetStatus20.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus20.Location = new System.Drawing.Point(313, 747);
            this.txtSetStatus20.Name = "txtSetStatus20";
            this.txtSetStatus20.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus20.TabIndex = 96;
            // 
            // lbGetStatus20
            // 
            this.lbGetStatus20.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus20.Location = new System.Drawing.Point(155, 749);
            this.lbGetStatus20.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus20.Name = "lbGetStatus20";
            this.lbGetStatus20.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus20.TabIndex = 95;
            this.lbGetStatus20.Text = "GET DATA";
            this.lbGetStatus20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("굴림", 12F);
            this.label25.Location = new System.Drawing.Point(0, 749);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(155, 37);
            this.label25.TabIndex = 94;
            this.label25.Text = "Max Time(Sec) : ";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus19
            // 
            this.txtSetStatus19.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus19.Location = new System.Drawing.Point(313, 710);
            this.txtSetStatus19.Name = "txtSetStatus19";
            this.txtSetStatus19.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus19.TabIndex = 93;
            // 
            // lbGetStatus19
            // 
            this.lbGetStatus19.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus19.Location = new System.Drawing.Point(155, 712);
            this.lbGetStatus19.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus19.Name = "lbGetStatus19";
            this.lbGetStatus19.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus19.TabIndex = 92;
            this.lbGetStatus19.Text = "GET DATA";
            this.lbGetStatus19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("굴림", 12F);
            this.label23.Location = new System.Drawing.Point(0, 712);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(155, 37);
            this.label23.TabIndex = 91;
            this.label23.Text = "Used Time(sec) : ";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus18
            // 
            this.txtSetStatus18.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus18.Location = new System.Drawing.Point(313, 673);
            this.txtSetStatus18.Name = "txtSetStatus18";
            this.txtSetStatus18.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus18.TabIndex = 90;
            // 
            // lbGetStatus18
            // 
            this.lbGetStatus18.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus18.Location = new System.Drawing.Point(155, 675);
            this.lbGetStatus18.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus18.Name = "lbGetStatus18";
            this.lbGetStatus18.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus18.TabIndex = 89;
            this.lbGetStatus18.Text = "GET DATA";
            this.lbGetStatus18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("굴림", 12F);
            this.label15.Location = new System.Drawing.Point(0, 675);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(155, 37);
            this.label15.TabIndex = 88;
            this.label15.Text = "Used Count : ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSetStatus17
            // 
            this.txtSetStatus17.Font = new System.Drawing.Font("굴림", 19F);
            this.txtSetStatus17.Location = new System.Drawing.Point(313, 636);
            this.txtSetStatus17.Name = "txtSetStatus17";
            this.txtSetStatus17.Size = new System.Drawing.Size(228, 37);
            this.txtSetStatus17.TabIndex = 87;
            // 
            // lbGetStatus17
            // 
            this.lbGetStatus17.Font = new System.Drawing.Font("굴림", 12F);
            this.lbGetStatus17.Location = new System.Drawing.Point(155, 638);
            this.lbGetStatus17.Margin = new System.Windows.Forms.Padding(0);
            this.lbGetStatus17.Name = "lbGetStatus17";
            this.lbGetStatus17.Size = new System.Drawing.Size(155, 37);
            this.lbGetStatus17.TabIndex = 86;
            this.lbGetStatus17.Text = "GET DATA";
            this.lbGetStatus17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("굴림", 12F);
            this.label2.Location = new System.Drawing.Point(0, 638);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 37);
            this.label2.TabIndex = 85;
            this.label2.Text = "Start Shaking : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("굴림", 12F);
            this.label4.Location = new System.Drawing.Point(0, 342);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 37);
            this.label4.TabIndex = 6;
            this.label4.Text = "Interval : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frConfigDev
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(746, 921);
            this.ControlBox = false;
            this.Controls.Add(this.pnConfig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "frConfigDev";
            this.Text = "Config";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frConfig_KeyDown);
            this.pnConfig.ResumeLayout(false);
            this.pnConfig.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.TextBox txtSetStatus16;
        private System.Windows.Forms.TextBox txtSetStatus15;
        private System.Windows.Forms.TextBox txtSetStatus14;
        private System.Windows.Forms.TextBox txtSetStatus13;
        private System.Windows.Forms.TextBox txtSetStatus12;
        private System.Windows.Forms.TextBox txtSetStatus11;
        private System.Windows.Forms.TextBox txtSetStatus10;
        private System.Windows.Forms.TextBox txtSetStatus9;
        private System.Windows.Forms.TextBox txtSetStatus8;
        private System.Windows.Forms.TextBox txtSetStatus7;
        private System.Windows.Forms.TextBox txtSetStatus6;
        private System.Windows.Forms.TextBox txtSetStatus5;
        private System.Windows.Forms.TextBox txtSetStatus4;
        private System.Windows.Forms.TextBox txtSetStatus3;
        private System.Windows.Forms.TextBox txtSetStatus2;
        private System.Windows.Forms.TextBox txtSetStatus1;
        private System.Windows.Forms.TextBox txtSetStatus0;
        private System.Windows.Forms.Label lbGetStatus16;
        private System.Windows.Forms.Label lbGetStatus15;
        private System.Windows.Forms.Label lbGetStatus14;
        private System.Windows.Forms.Label lbGetStatus13;
        private System.Windows.Forms.Label lbGetStatus12;
        private System.Windows.Forms.Label lbGetStatus11;
        private System.Windows.Forms.Label lbGetStatus10;
        private System.Windows.Forms.Label lbGetStatus8;
        private System.Windows.Forms.Label lbGetStatus7;
        private System.Windows.Forms.Label lbGetStatus6;
        private System.Windows.Forms.Label lbGetStatus5;
        private System.Windows.Forms.Label lbGetStatus4;
        private System.Windows.Forms.Label lbGetStatus3;
        private System.Windows.Forms.Label lbGetStatus2;
        private System.Windows.Forms.Label lbGetStatus1;
        private System.Windows.Forms.Label lbGetStatus0;
        private System.Windows.Forms.Label lbGetStatus9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnConfig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSetStatus23;
        private System.Windows.Forms.Label lbGetStatus23;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtSetStatus22;
        private System.Windows.Forms.Label lbGetStatus22;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtSetStatus21;
        private System.Windows.Forms.Label lbGetStatus21;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtSetStatus20;
        private System.Windows.Forms.Label lbGetStatus20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtSetStatus19;
        private System.Windows.Forms.Label lbGetStatus19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSetStatus18;
        private System.Windows.Forms.Label lbGetStatus18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSetStatus17;
        private System.Windows.Forms.Label lbGetStatus17;
        private System.Windows.Forms.Label label2;
    }
}