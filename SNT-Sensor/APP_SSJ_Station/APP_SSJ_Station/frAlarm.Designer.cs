﻿namespace APP_SSJ_Station
{
    partial class frAlarm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnTitle = new System.Windows.Forms.Panel();
            this.lbTimeOut = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lbMsg = new System.Windows.Forms.Label();
            this.lbHeader = new System.Windows.Forms.Label();
            this.tmrToggle = new System.Windows.Forms.Timer(this.components);
            this.pnTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTitle
            // 
            this.pnTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnTitle.BackColor = System.Drawing.Color.Salmon;
            this.pnTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnTitle.Controls.Add(this.lbTimeOut);
            this.pnTitle.Controls.Add(this.btnClose);
            this.pnTitle.Controls.Add(this.lbMsg);
            this.pnTitle.Controls.Add(this.lbHeader);
            this.pnTitle.Location = new System.Drawing.Point(12, 12);
            this.pnTitle.Name = "pnTitle";
            this.pnTitle.Size = new System.Drawing.Size(1222, 321);
            this.pnTitle.TabIndex = 0;
            // 
            // lbTimeOut
            // 
            this.lbTimeOut.BackColor = System.Drawing.Color.Pink;
            this.lbTimeOut.Font = new System.Drawing.Font("Cambria", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTimeOut.ForeColor = System.Drawing.Color.Black;
            this.lbTimeOut.Location = new System.Drawing.Point(3, 220);
            this.lbTimeOut.Name = "lbTimeOut";
            this.lbTimeOut.Size = new System.Drawing.Size(1043, 90);
            this.lbTimeOut.TabIndex = 89;
            this.lbTimeOut.Text = "Auto Close (5) sec->";
            this.lbTimeOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("굴림", 25F);
            this.btnClose.Location = new System.Drawing.Point(1049, 220);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(130, 90);
            this.btnClose.TabIndex = 88;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lbMsg
            // 
            this.lbMsg.BackColor = System.Drawing.Color.Transparent;
            this.lbMsg.Font = new System.Drawing.Font("Cambria", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMsg.ForeColor = System.Drawing.Color.Black;
            this.lbMsg.Location = new System.Drawing.Point(34, 60);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(1108, 134);
            this.lbMsg.TabIndex = 1;
            this.lbMsg.Text = "aaaaaa";
            this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeader
            // 
            this.lbHeader.BackColor = System.Drawing.Color.Transparent;
            this.lbHeader.Font = new System.Drawing.Font("Cambria", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeader.ForeColor = System.Drawing.Color.White;
            this.lbHeader.Location = new System.Drawing.Point(17, -8);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(292, 90);
            this.lbHeader.TabIndex = 0;
            this.lbHeader.Text = "Alarm";
            this.lbHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrToggle
            // 
            this.tmrToggle.Interval = 500;
            this.tmrToggle.Tick += new System.EventHandler(this.tmrToggle_Tick);
            // 
            // frAlarm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1246, 345);
            this.ControlBox = false;
            this.Controls.Add(this.pnTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frAlarm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.pnTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTitle;
        private System.Windows.Forms.Label lbMsg;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Timer tmrToggle;
        private System.Windows.Forms.Label lbTimeOut;
    }
}