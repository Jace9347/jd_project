﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP_SSJ_Station
{
    using JDX_Library_X86.Common;
    using System.Xml;
    using System.IO;
    public partial class frModel : Form
    {
        public frModel()
        {
            InitializeComponent();
        }
        public frModel(frMain _this)
        {
            InitializeComponent();

            main = _this;
        }

        frMain main;

        frMain.ST_SeletedModel[] ModelList;

        XmlDocument xmlDocument = new XmlDocument();

        RadioButton rbtnSelectName;

        enum SelectName {Temperature, Plasma, Hybrid}

        SelectName selectName;

        string pathfile = Application.StartupPath + "\\mod.xml";


        private void frModel_Load(object sender, EventArgs e)
        {
            #region MyRegion
            if (File.Exists(pathfile))
            {
                using (XmlReader rd = XmlReader.Create(pathfile))
                {
                    rd.Read();
                    int Max = rd.AttributeCount;
                    int Cnt = 0;

                    while (rd.Read())
                    {
                        if (rd.IsStartElement())
                        {
                            //Model List
                            if (rd.Name == "Model")
                            {
                                // attribute 읽기                            
                                string id = rd["Id"]; // rd.GetAttribute("Id");
                                if (!string.IsNullOrEmpty(id))
                                {
                                    rd.Read();   // 다음 노드로 이동        

                                    // Element 읽기
                                    string name = rd.ReadElementContentAsString("Name", "");
                                    string dept = rd.ReadElementContentAsString("Dept", "");


                                    dgvModelViewer.Rows.Add(id, name, dept);
                                }
                            }
                            if (rd.Name == "Selected")
                            {
                                string id = rd["Id"]; // rd.GetAttribute("Id");
                                rd.Read();   // 다음 노드로 이동        

                                // Element 읽기
                                string name = rd.ReadElementContentAsString("Name", "");
                                string dept = rd.ReadElementContentAsString("Dept", "");

                                txtSelectID.Text = id;
                                txtSelectName.Text = name;

                                if (txtSelectName.Text == "Temperature")
                                {
                                    rbtnTemperature.Checked = true;
                                }
                                else if (txtSelectName.Text == "Plasma")
                                {
                                    rbtnPlasma.Checked = true;
                                }
                                else if (txtSelectName.Text == "Hybrid")
                                {
                                    rbtnHybrid.Checked = true;
                                }
                                txtSelectDepatcher.Text = dept;
                            }
                        }
                    }
                    try
                    {
                        ModelList = new frMain.ST_SeletedModel[dgvModelViewer.RowCount - 1];      //-1
                        for (int row = 0; row < dgvModelViewer.RowCount - 1; row++)     //-1
                        {
                            ModelList[row].ID = dgvModelViewer.Rows[row].Cells[0].Value.ToString();
                            ModelList[row].Name = dgvModelViewer.Rows[row].Cells[1].Value.ToString();
                            ModelList[row].Depatcher = dgvModelViewer.Rows[row].Cells[2].Value.ToString();
                        }
                        dgvModelViewer.Rows.Clear();

                        for (int i = 0; i < ModelList.Length - 1; i++)      //-1
                        {
                            dgvModelViewer.Rows.Add();

                            if (dgvModelViewer.IsCurrentCellDirty)
                            {
                                dgvModelViewer.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            }
                        }

                        for (int row = 0; row < dgvModelViewer.RowCount; row++)             //2018.09.28 KimIkHwan Selected ID와 같은 Row Select하기
                        {
                            dgvModelViewer.Rows[row].Cells[0].Value = ModelList[row].ID;
                            dgvModelViewer.Rows[row].Cells[1].Value = ModelList[row].Name;
                            dgvModelViewer.Rows[row].Cells[2].Value = ModelList[row].Depatcher;

                            string dgvModelViewerValue = (string)dgvModelViewer.Rows[row].Cells[0].Value;
                            if (txtSelectID.Text == dgvModelViewerValue)
                            {
                                dgvModelViewer.Rows[row].Selected = true;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            } 
            #endregion
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //xmlRelation();
            Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int row = 0;
            try
            {
                for (row = 0; row < dgvModelViewer.RowCount; row++)
                {
                    if (dgvModelViewer.Rows[row].Selected == true)
                    {
                        if (dgvModelViewer.Rows[row].Cells[0].Value.ToString() == txtSelectID.Text)
                        {
                            dgvModelViewer.Rows.RemoveAt(row);
                            txtSelectID.Text = dgvModelViewer.Rows[row].Cells[0].Value.ToString();
                            rbtnSelectName.Text = dgvModelViewer.Rows[row].Cells[1].Value.ToString();
                            txtSelectDepatcher.Text = dgvModelViewer.Rows[row].Cells[2].Value.ToString();
                        }
                        else
                        {
                            dgvModelViewer.Rows.RemoveAt(row);
                        }
                        break;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool isCompare = false;
            dgvModelViewer.AllowUserToAddRows = true;

            string[,] buff = new string[dgvModelViewer.RowCount, dgvModelViewer.ColumnCount];
            try
            {
                for (int row = 0; row < buff.GetLength(0); row++)
                {
                    for (int col = 0; col < buff.GetLength(1); col++)
                    {
                        buff[row, col] = dgvModelViewer.Rows[row].Cells[col].Value.ToString();
                    }
                }

                for(int row = 0; row<buff.GetLength(0); row++)
                {
                    if ((buff[row, 2] == (txtSelectDepatcher.Text)) && (buff[row, 1] == (rbtnSelectName.Text)))
                    {
                        isCompare = true;
                        break;
                    }
                }
            }
            catch
            {
            }


            if (rbtnSelectName.Text == "" || txtSelectDepatcher.Text == "")
            {
                MessageBox.Show("값이 없습니다.");
            }
            else
            {
                if (isCompare)
                {
                    MessageBox.Show("같은 값이 있습니다.");
                }
                else
                {
                    addData(txtSelectID.Text, rbtnSelectName.Text, txtSelectDepatcher.Text);
                }
            }
        }

        private void addData(string ID, string Name, string Dept)
        {
            dgvModelViewer.Rows.Add();
            dgvModelViewer.Rows[dgvModelViewer.RowCount - 2].Cells[0].Value = dgvModelViewer.Rows[dgvModelViewer.RowCount - 1].Cells[0].Value;
            dgvModelViewer.Rows[dgvModelViewer.RowCount - 2].Cells[1].Value = dgvModelViewer.Rows[dgvModelViewer.RowCount - 1].Cells[1].Value;
            dgvModelViewer.Rows[dgvModelViewer.RowCount - 2].Cells[2].Value = dgvModelViewer.Rows[dgvModelViewer.RowCount - 1].Cells[2].Value;

            dgvModelViewer.Rows[dgvModelViewer.RowCount-1].Cells[0].Value = null;
            dgvModelViewer.Rows[dgvModelViewer.RowCount-1].Cells[1].Value = null;
            dgvModelViewer.Rows[dgvModelViewer.RowCount-1].Cells[2].Value = null;

            try
            {
                    int[] buff = new int[dgvModelViewer.RowCount];
                    int row;
                    int temp = 0;

                    for (row = 0; row < dgvModelViewer.RowCount - 1; row++)
                    {
                        buff[row] = Convert.ToInt32(dgvModelViewer.Rows[row].Cells[0].Value.ToString());
                    }

                    for (int i = 0; i < row; i++)
                    {
                        for (int j = i + 1; j < row; j++)
                        {
                            if(buff[i] > buff[j])
                            {
                                temp = buff[i];
                                buff[i] = buff[j];
                                buff[j] = temp;
                            }
                        }
                    }

                    int Value = 1001;
                    for (int i = 0; i< row+1; i++)
                    {

                        if(buff[i] != Value)
                        {
                            txtSelectID.Text = Value.ToString();
                            break;
                        }
                        else
                        {
                            Value++;
                        }
                        
                    }

                    dgvModelViewer.Rows[dgvModelViewer.RowCount - 1].Cells[0].Value = Value.ToString();
                    dgvModelViewer.Rows[dgvModelViewer.RowCount-1].Cells[1].Value = Name;
                    dgvModelViewer.Rows[dgvModelViewer.RowCount-1].Cells[2].Value = Dept;
                    dgvModelViewer.Sort(dgvModelViewer.Columns[0], ListSortDirection.Ascending);        //2018.10.02 KimIkHwan 오름차순
            }
            catch
            {

            }
        }

        private void dgvModelViewer_DoubleClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("ID를 변경 하시겠습니까?", "ID 변경", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                xmlRelation();
                MessageBox.Show("Save Succes");
                main.lbDebugMsg.Text = "Model Change";
            }
        }

        private void xmlRelation()      //2018.09.28 Kim
        {
            #region XmlSelected
            try
            {
                for (int row = 0; row < dgvModelViewer.RowCount; row++)
                {
                    if (dgvModelViewer.Rows[row].Selected == true)
                    {
                        txtSelectID.Text = dgvModelViewer.Rows[row].Cells[0].Value.ToString();
                        txtSelectName.Text = dgvModelViewer.Rows[row].Cells[1].Value.ToString();
                        txtSelectDepatcher.Text = dgvModelViewer.Rows[row].Cells[2].Value.ToString();

                        if (txtSelectName.Text == "Temperature")
                        {
                            rbtnTemperature.Checked = true;
                        }
                        else if (txtSelectName.Text == "Plasma")
                        {
                            rbtnPlasma.Checked = true;
                        }
                        else if (txtSelectName.Text == "Hybrid")
                        {
                            rbtnHybrid.Checked = true;
                        }
                        break;
                    }
                }
                XmlNodeList xmlNodeList = xmlDocument.DocumentElement.SelectNodes("Selected");               //2018.09.28 KimIkHwan Xml에 있는 Selected ID를 찾아 선택한 ID로 변경
                string selectedID = xmlNodeList[0].Attributes["Id"].Value;
                xmlNodeList[0].Attributes["Id"].Value = txtSelectID.Text;

                XmlNode xmlNodeName = xmlNodeList[0].SelectSingleNode("Name");               //2018.09.28 KimIkHwan Xml에 있는 Selected Name을 찾아 선택한 Name으로 변경
                xmlNodeName.InnerText = txtSelectName.Text;

                XmlNode xmlNodeDepatcher = xmlNodeList[0].SelectSingleNode("Dept");               //2018.09.28 KimIkHwan Xml에 있는 Selected Dept를 찾아 선택한 Dept로 변경
                xmlNodeDepatcher.InnerText = txtSelectDepatcher.Text;
            }
            catch
            {

            }

            xmlDocument.PreserveWhitespace = true;              //2018.09.28 KimIkHwan Save시 공백 제거
            xmlDocument.Save(Console.Out);
            #endregion

            #region XmlWrite
            using (XmlWriter wr = XmlWriter.Create(Application.StartupPath + "\\mod.xml"))
            {
                try
                {
                    wr.WriteStartElement("Model");
                    foreach (DataGridViewRow row in dgvModelViewer.Rows)
                    {
                        wr.WriteStartElement("Model");
                        wr.WriteAttributeString("Id", row.Cells[0].Value.ToString());  // attribute 쓰기
                        wr.WriteElementString("Name", row.Cells[1].Value.ToString());   // Element 쓰기
                        wr.WriteElementString("Dept", row.Cells[2].Value.ToString());
                        wr.WriteEndElement();
                    }

                    wr.WriteStartElement("Selected");
                    wr.WriteAttributeString("Id", txtSelectID.Text);
                    wr.WriteElementString("Name", rbtnSelectName.Text);
                    wr.WriteElementString("Dept", txtSelectDepatcher.Text);
                    wr.WriteEndElement();

                    wr.WriteEndDocument();
                }
                catch
                {

                }
            }
            #endregion
        }


        private void rbtnSelectName_CheckedChanged(object sender, EventArgs e)
        {
            rbtnSelectName = sender as RadioButton;

            if(rbtnSelectName.Checked == false)
            {
                return;
            }

            switch(rbtnSelectName.Text)
            {
                case "Temperature": selectName = SelectName.Temperature; break;
                case "Plasma": selectName = SelectName.Plasma; break;
                case "Hybrid": selectName = SelectName.Hybrid; break;
            }
        }
    }
}
