﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP_SSJ_Station
{
    public partial class frConfigUser : Form
    {
        public frConfigUser()
        {
            InitializeComponent();
        }

        public frConfigUser(frMain _this)
        {
            InitializeComponent();

            main = _this;
        }

        frMain main;
        frConfigDev configdev;
        RadioButton[] rbMode = new RadioButton[3];

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {

            try
            {
                //test
                //main.strGetConfigData = "S:00/M:00/B:38/RC:001/EC:000/SN:17,11,02/PN:00000,00000/TP:00000/T:14,56,20/IV:0001/V:3.0.3/R:0/DR:0/CL:33/DL:0000/CN:0000/ST:000/SS:0/UC:00023/UT:01561/MT:14400/WT:00004004/CC:00004/ID:00000/";

                if (main.strGetConfigData != null)
                {
                    int mode = -1;
                    int delay = int.Parse(txtDelay.Text);
                    int templimit = int.Parse(txtTemp.Text);
                    int interval = int.Parse(txtInterval.Text);
                    int countlimit = int.Parse(txtCntLimit.Text);

                    for (int i = 0; i < 3; i++)
                    {
                        if (rbMode[i].Checked)
                        {
                            mode = i;
                            break;
                        }
                    }
                    main.work.Set_Mode(mode, delay, templimit);
                    System.Threading.Thread.Sleep(50);
                    main.work.Set_LimitCount(countlimit);
                    System.Threading.Thread.Sleep(50);
                    main.work.Set_Interval(interval);
                    System.Threading.Thread.Sleep(50);
                    main.bUserConfig = true;
                    main.work.Get_Config();

                    #region 컨피그 설정 변경으로 인한 삭제 //2018.10.03 jace
                    //main.strGetConfigData = main.strGetConfigData.Replace("\0", null);
                    //string[] buff = main.strGetConfigData.Split('/');
                    //int[] iLenth = new int[buff.Length];

                    //for (int i = 0; i < buff.Length; i++)
                    //{
                    //    int last = buff[i].LastIndexOf(':');

                    //    if (!string.IsNullOrEmpty(buff[i]))
                    //    {

                    //        if (buff[i].Contains("M:"))
                    //        {
                    //            string tmp = string.Format("{0}", buff[i].Substring(last + 1));

                    //            buff[i] = buff[i].Replace(tmp, null);
                    //            buff[i] += mode;
                    //        }

                    //        if (buff[i].Contains("DL:"))
                    //        {
                    //            string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //            txtSendDelay.Text = delay.ToString();

                    //            buff[i] = buff[i].Replace(tmp, null);
                    //            buff[i] += delay.ToString("0000");
                    //        }
                    //        if (buff[i].Contains("ST:"))
                    //        {
                    //            string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //            txtSendTemp.Text = templimit.ToString();

                    //            buff[i] = buff[i].Replace(tmp, null);
                    //            buff[i] += templimit.ToString("000");
                    //        }
                    //        if (buff[i].Contains("IV:"))
                    //        {
                    //            string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //            txtSendInterval.Text = interval.ToString() ;

                    //            buff[i] = buff[i].Replace(tmp, null);
                    //            buff[i] += interval.ToString("0000");
                    //        }
                    //        if (buff[i].Contains("CN:"))
                    //        {
                    //            string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //            txtSendCntLimit.Text = countlimit.ToString();

                    //            buff[i] = buff[i].Replace(tmp, null);
                    //            buff[i] += countlimit.ToString("0000");
                    //        }
                    //    }
                    //}

                    //string SetConfig = null;
                    //for (int i = 0; i < buff.Length; i++)
                    //{
                    //    SetConfig += buff[i] + "/";
                    //}

                    //main.work.SetConfigData(SetConfig);
                    //main.strGetConfigData = SetConfig;

                    //System.Threading.Thread.Sleep(300);
                    //main.work.Get_Config(); 
                    #endregion

                }
            }
            catch
            { }
        }

        private void frConfigUser_Load(object sender, EventArgs e)
        {
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
            
            //Array init
            rbMode[0] = rbtnNormal;
            rbMode[1] = rbtnDelay;
            rbMode[2] = rbtnTemp;

            //main.work.Get_Config();


            Configdata_Update();
        }

        public void Configdata_Update()
        {
            if (main.strGetConfigData == null)
                return;
            string[] buff = main.strGetConfigData.Split('/');
            int[] iLenth = new int[buff.Length];

            //string[] config = new string[] { "S:", "M:", "B:", "RC:", "EC:", "SN:", "PN:", "TP:", "T:", "IV:", "V:", "R:", "DR:", "CL:", "DL:", "CN:", "ST:" };

            for (int i = 0; i < buff.Length; i++)
            {
                int last = buff[i].LastIndexOf(':');

                if (!string.IsNullOrEmpty(buff[i]))
                {
                    if (buff[i].Contains("M:"))
                    {
                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                        int mode = 0;

                        mode = int.Parse(tmp);

                        rbMode[mode].Checked = true;
                    }



                    if (buff[i].Contains("IV:"))
                    {
                        //lbConfigCycle.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        txtSendInterval.Text = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("DL:"))
                    {
                        //lbConfigDelaytime.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        txtSendDelay.Text = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("CN:"))
                    {
                        //lbConfigLimitcount.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        txtSendCntLimit.Text = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("ST:"))
                    {
                        //lbConfigSetTemperature.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " ℃";
                        txtSendTemp.Text = string.Format("{0}", buff[i].Substring(last + 1));
                    }




                }
            }
        }

        //public void Set_Txt_Update(int sta, string a, string b, string c, string d)
        //{

        //    txtSendDelay.Text = a;
        //    txtSendTemp.Text = b;
        //    txtSendInterval.Text = c;
        //    txtSendCntLimit.Text = d;
        //}

        //2018.08.09 Sensor Board, Station Reset Add jace
        private void btnSensorBdReset_Click(object sender, EventArgs e)
        {
            main.work.Set_SbReset();
        }

        private void btnStationReset_Click(object sender, EventArgs e)
        {
            main.work.Set_Reset();
        }
    }
}