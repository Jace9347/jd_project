﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace APP_SSJ_Station
{
    using JDX_Library_X86;
    using JDX_Library_X86.Common;
    using JDS_Library_X86;
    using System.IO;

    public partial class frOffset : Form
    {
        public frOffset()
        {
            InitializeComponent();
        }

        public frOffset(frMain _this)
        {
            InitializeComponent();

            main = _this;
        }

        frMain main;
        frModel model;
        frMain.ST_SeletedModel[] Init;
        int Dept = 0;
        string[] column = new string[18];

        ClsXmlFile xmlFunc = new ClsXmlFile();

        private void _Array()
        {
            column[1] = ColumnLocationX.HeaderText;     //LocationX
            column[2] = ColumnLocationY.HeaderText;     //LocationY
            column[3] = Column1.HeaderText;             //K(010)
            column[4] = Column2.HeaderText;             //K(020)
            column[5] = Column3.HeaderText;             //K(030)
            column[6] = Column4.HeaderText;             //K(040)
            column[7] = Column5.HeaderText;             //K(050)
            column[8] = Column6.HeaderText;             //K(060)
            column[9] = Column7.HeaderText;             //K(070)
            column[10] = Column8.HeaderText;            //K(080)
            column[11] = Column9.HeaderText;            //K(090)
            column[12] = Column10.HeaderText;           //K(100)
            column[13] = Column11.HeaderText;           //K(110)
            column[14] = Column12.HeaderText;           //K(120)
            column[15] = Column13.HeaderText;           //K(130)
            column[16] = Column14.HeaderText;           //K(140)
            column[17] = Column15.HeaderText;           //K(150)
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            main.OffSetWaferID = txtWaferID.Text;
            Close();
        }

        //Load
        string strXmlLoadPath = null;
        string strXmlLoadDefault = Application.StartupPath + "\\" + "CalibrationData" + "\\";
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region 2018.10.05 - jace delete
            //if (string.IsNullOrEmpty(txtFileName.Text))
            //{
            //    return;
            //}
            //else
            //{
            //    //frMain.Offset.File = txtFileName.Text.Substring(0,txtFileName.Text.LastIndexOf('.')) + ".dat";
            //    //frMain.Offset.File = frMain.Offset.File.Replace("Mask", "OS");

            //    clsINIFile.SetValue(frMain.AppInfo.Key_Section, frMain.AppInfo.Key_File, frMain.Offset.File, frMain.AppInfo.SettingPath + frMain.AppInfo.SettingFile);

            //    try
            //    {
            //        //frMain.Offset.dValue = double.Parse(txtOffsetValue.Text);
            //    }
            //    catch
            //    {
            //        frMain.Offset.dValue = 0;
            //    }


            //    for (int i = 0; i < 2; i++)
            //    {
            //        for (int idx = 0; idx < Dept; idx++)
            //        {
            //            if (dgvOffset.Rows[idx].Cells[i + 1].Value == null)
            //            {
            //                frMain.Offset.Data[i, idx] = "0";
            //            }
            //            else
            //            {
            //                frMain.Offset.Data[i, idx] = dgvOffset.Rows[idx].Cells[i + 1].Value.ToString();
            //            }

            //            clsINIFile.SetValue(frMain.Offset.Key_XY, i.ToString() + idx.ToString(), frMain.Offset.Data[i, idx], frMain.Offset.Path + frMain.Offset.File);
            //        }
            //    }

            //    for(int i =0; i< 4; i++)
            //    {
            //        for (int idx = 0; idx < Dept; idx++)
            //        {
            //            if (dgvOffset.Rows[idx].Cells[frMain.Offset.dat + i + 1].Value == null)
            //            {
            //                frMain.Offset.Data[frMain.Offset.dat + i, idx] = "0";
            //            }
            //            else
            //            {
            //                frMain.Offset.Data[frMain.Offset.dat + i, idx] = dgvOffset.Rows[idx].Cells[frMain.Offset.dat + i + 1].Value.ToString();
            //            }


            //            if(i == 0)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Offset, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 1)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_High, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 2)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Low, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 3)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Avg, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //        }

            //    }

            //    clsINIFile.SetValue(frMain.Offset.Key_Dat, "Val", frMain.Offset.dValue.ToString(), frMain.Offset.Path + frMain.Offset.File);

            //    string[,] conform = frMain.Offset.Data;
            //} 
            #endregion

            OpenFileDialog openModeldlg = new OpenFileDialog();
            openModeldlg.Filter = "*.xml|*.xml| All-File *.* | *.*";

            if (string.IsNullOrEmpty(strXmlLoadPath))
            {
                openModeldlg.InitialDirectory = Application.StartupPath + "\\";
            }
            else
            {
                openModeldlg.InitialDirectory = strXmlLoadPath;
            }


            if (openModeldlg.ShowDialog() == DialogResult.OK)
            {
                strXmlLoadPath = System.IO.Path.GetDirectoryName(openModeldlg.FileName);
                string file = openModeldlg.FileName;


                if (strXmlLoadDefault != strXmlLoadPath)
                {

                    string strTemp = file.Substring(file.LastIndexOf('\\') + 1);

                    Directory.CreateDirectory(strXmlLoadDefault);

                    if (File.Exists(strXmlLoadDefault + file))
                    {
                        //알람
                    }
                    else
                    {
                        File.Copy(file, strXmlLoadDefault + strTemp);
                        CalibrationFileLoad(strXmlLoadDefault + strTemp);
                    }
                    //읽기

                }
                else
                {
                    //읽기
                    //CalibrationFileLoad()
                    //알람
                }
            }


        }

        void CalibrationFileLoad(string xmlPath)
        {
            //List<List<string>> list =  xmlFunc.Read(xmlPath);


            string id = xmlFunc.Read(xmlPath, "Calibration", "Wafer ID");
            string name = xmlFunc.Read(xmlPath, "Calibration", "Name");
            string sensorCount = xmlFunc.Read(xmlPath, "Calibration", "SensorCount");
            string calCount = xmlFunc.Read(xmlPath, "Calibration", "CalCount");

            try
            {

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void frOffset_Load(object sender, EventArgs e)
        {
            frMain.Offset.Path = strXmlLoadDefault;
            #region MyRegion
            string pathfile = Application.StartupPath + "\\mod.xml";
            if (File.Exists(pathfile))
            {
                using (XmlReader rd = XmlReader.Create(pathfile))
                {
                    rd.Read();
                    int Max = rd.AttributeCount;
                    Init = new frMain.ST_SeletedModel[Max];
                    while (rd.Read())
                    {
                        if (rd.IsStartElement())
                        {
                            if (rd.Name == "Selected")
                            {
                                string id = rd["Id"]; // rd.GetAttribute("Id");
                                rd.Read();   // 다음 노드로 이동        

                                // Element 읽기
                                string name = rd.ReadElementContentAsString("Name", "");
                                string dept = rd.ReadElementContentAsString("Dept", "");


                                lbModelID.Text = id;
                                lbName.Text = name;
                                lbDepatcher.Text = dept;
                            }
                            else
                            {
                                lbModelID.Text = Init[0].ID;
                                lbName.Text = Init[0].Name;
                                lbDepatcher.Text = Init[0].Depatcher;
                            }
                        }
                    }
                }
            }
            Dept = Convert.ToInt32(lbDepatcher.Text);       //2018.10.03 Kim 
            #endregion

            frMain.Offset.Data = new string[11, Dept];
            //file list
            if (!Directory.Exists(frMain.Offset.Path))
            {
                Directory.CreateDirectory(frMain.Offset.Path);
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(frMain.Offset.Path);
                foreach (var item in di.GetFiles())
                {
                    if (item.ToString().Contains("xml") || (item.ToString().Contains("Xml")))
                    {
                        lstModel.Items.Insert(0, item);
                    }
                }
            }

            _Array();

            try
            {
                for (int i = 0; i < Dept; i++)
                {
                    dgvOffset.Rows.Add((i + 1).ToString(), frMain.Offset.Data[frMain.Offset.x, i], frMain.Offset.Data[frMain.Offset.y, i],
                        frMain.Offset.Data[frMain.Offset.dat, i], frMain.Offset.Data[frMain.Offset.dat + 1, i],
                        frMain.Offset.Data[frMain.Offset.dat + 2, i], frMain.Offset.Data[frMain.Offset.dat + 3, i]);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            string Calpathfile = Application.StartupPath + "\\8.xml";
            XmlFileLoad(Calpathfile);
            txtFileName.Text = System.IO.Path.GetFileName(Calpathfile);
        }

        private void dgvOffset_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //var grid = (DataGridView)sender;
            //if (grid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            //{
            //    clsINIFile.SetValue(head, "0" + e.RowIndex.ToString(), dgvOffset.Rows[e.RowIndex].Cells[1].Value.ToString(), path + file);
            //    clsINIFile.SetValue(head, "1" + e.RowIndex.ToString(), dgvOffset.Rows[e.RowIndex].Cells[2].Value.ToString(), path + file);
            //    clsINIFile.SetValue(key, e.RowIndex.ToString(), dgvOffset.Rows[e.RowIndex].Cells[3].Value.ToString(), path + file);
            //}
        }

        private void btnMask_Click(object sender, EventArgs e)
        {
            //Master Data
            //if()
            if (main.dRawData != null)
            {
                const int iDefault = 17;

                string[] tmp = new string[iDefault + main.dRawData.GetLength(0)];

                string log = null;

                string[] info = main.Get_Info();


                bool contains = false;
                string file = info[0] + "-Mask.csv";
                for (int i = 0; i < lstModel.Items.Count; i++)
                {
                    if (lstModel.Items[i].ToString().Contains(file))
                    {
                        contains = true;
                    }
                }

                if (!contains)
                {
                    lstModel.Items.Insert(0, file);
                    //lstModel.SelectedIndex = 0;
                }
                else
                {
                    FileInfo fi = new FileInfo(file);
                    fi.Delete();
                }


                for (int i = 0; i < iDefault; i++)
                {
                    if (i < info.Length)
                    {
                        tmp[i] = info[i];
                    }
                    else
                    {
                        tmp[i] = "DUMMY";
                    }
                }

                for (int i = iDefault; i < tmp.Length; i++)
                {
                    log = (i-iDefault + 1).ToString();
                    for (int idx = 0; idx < main.dRawData.GetLength(1); idx++)
                    {
                        log += "," + main.dRawData[i - iDefault, idx];
                    }
                    tmp[i] = log;
                }
                clsLog.SowLog(tmp, frMain.Offset.Path, file);

                file = file.Replace("-Mask", "-calc");
                file = file.Replace("csv", "dat");

                frMain.Offset.File = file;
                txtFileName.Text = file;
                btnSave_Click(null, null);
            }
        }

        string[] strMaster;
        double[,] dMaster;
        double[,] dOffset;

        private void lstModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            string file = lstModel.Items[lstModel.SelectedIndex].ToString();        //2018.10.05 Kim lstModel Select한거 찾기

            XmlFileLoad(file);

            #region MyRegion
            //try
            //{

            //    #region MyRegion
            //    const int iDefaultNumber = 17;

            //    string[] mask = clsLog.ReadLog(frMain.Offset.Path + lstModel.Items[lstModel.SelectedIndex].ToString());

            //    strMaster = new string[mask.Length - iDefaultNumber];

            //    for (int i = 0; i < strMaster.Length; i++)
            //    {
            //        strMaster[i] = mask[i + iDefaultNumber];
            //    }

            //    dMaster = new double[strMaster.Length, strMaster[0].Split(',').Length - 1];

            //    int ZeroLenth = 0;

            //    for (int i = 0; i < strMaster.Length; i++)
            //    {
            //        string[] buf = strMaster[i].Split(',');

            //        if (buf.Length >= 50)
            //        {
            //            for (int u = 0; u < buf.Length - 1; u++)
            //            {
            //                if (!string.IsNullOrEmpty(buf[u + 1]))
            //                {
            //                    dMaster[i, u] = double.Parse(buf[u + 1]);
            //                }
            //                else
            //                {
            //                    dMaster[i, u] = 0;
            //                    if (i == 0)
            //                    {
            //                        ZeroLenth++;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    //Buff lenth convert
            //    double[,] dBuffer = new double[strMaster.Length, dMaster.GetLength(1) - ZeroLenth];
            //    for (int i = 0; i < dBuffer.GetLength(0); i++)
            //    {
            //        for (int u = 0; u < dBuffer.GetLength(1); u++)
            //        {
            //            dBuffer[i, u] = dMaster[i, u];
            //        }
            //    }

            //    dMaster = dBuffer;

            //    dOffset = new double[4, dMaster.GetLength(1)];

            //    double[] avg = new double[dMaster.GetLength(0)];


            //    int cnt = dMaster.GetLength(0);

            //    double[] low = new double[dMaster.GetLength(1)];
            //    double[] high = new double[dMaster.GetLength(1)];
            //    double[] _resAvg = new double[dMaster.GetLength(1)];

            //    double min = 0;
            //    double max = 0;
            //    for (int i = 0; i < avg.Length; i++)
            //    {
            //        double sum = 0;
            //        for (int u = 0; u < dMaster.GetLength(1); u++)
            //        {
            //            sum += dMaster[i, u];
            //        }
            //        avg[i] = sum / dMaster.GetLength(1);
            //    }

            //    min = avg.Min();
            //    max = avg.Max();

            //    double center = (max + min) / 2;

            //    for (int i = 0; i < cnt; i++)
            //    {
            //        if (avg[i] == min)
            //        {
            //            Console.WriteLine("MIN IDX   " + i.ToString());
            //            for (int idx = 0; idx < dMaster.GetLength(1); idx++)
            //            {
            //                low[idx] = min - dMaster[i, idx];
            //                dOffset[1, idx] = low[idx];

            //            }

            //        }
            //        if (avg[i] == max)
            //        {
            //            Console.WriteLine("MAX IDX   " + i.ToString());
            //            for (int idx = 0; idx < dMaster.GetLength(1); idx++)
            //            {
            //                high[idx] = max - dMaster[i, idx];
            //                dOffset[2, idx] = high[idx];
            //            }
            //        }

            //        if (avg[i] <= center)
            //        {

            //        }
            //        else
            //        {

            //        }
            //    }

            //    for (int i = 0; i < dgvOffset.RowCount; i++)
            //    {
            //        if (i < high.Length)
            //        {
            //            dgvOffset.Rows[i].Cells[4].Value = high[i].ToString("0.000");
            //            dgvOffset.Rows[i].Cells[5].Value = low[i].ToString("0.000");
            //        }
            //        else
            //        {
            //            dgvOffset.Rows[i].Cells[4].Value = 0;
            //            dgvOffset.Rows[i].Cells[5].Value = 0;
            //        }

            //    }

            //    for (int i = 0; i < dgvOffset.RowCount; i++)
            //    {
            //        for (int u = 1; u < dgvOffset.ColumnCount; u++)
            //        {
            //            if (dgvOffset.Rows[i].Cells[u].Value != null)
            //            {
            //                frMain.Offset.Data[u - 1, i] = (dgvOffset.Rows[i].Cells[u].Value.ToString());
            //            }
            //            else
            //            {
            //                frMain.Offset.Data[u - 1, i] = "0";
            //            }

            //        }
            //    }


            //    string file = lstModel.Items[lstModel.SelectedIndex].ToString();
            //    file = file.Substring(0, file.LastIndexOf('.')) + ".dat";
            //    file = file.Replace("Mask", "calc");
            //    txtFileName.Text = file;
            //    frMain.Offset.File = file;

            //    clsINIFile.SetValue("INFO", "OFFSET_FILE", Application.StartupPath + "\\OffsetData\\" + file, Application.StartupPath + "\\" + "mdsys.dat");


            //    for (int i = 0; i < frMain.Offset.Data.GetLength(0); i++)
            //    {
            //        for (int idx = 0; idx < frMain.Offset.Data.GetLength(1); idx++)
            //        {

            //            if (i == 0 || i == 1)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_XY, i.ToString() + idx.ToString(), frMain.Offset.Data[i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }

            //            if (i == 0)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Offset, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 1)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_High, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 2)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Low, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //            if (i == 3)
            //            {
            //                clsINIFile.SetValue(frMain.Offset.Key_Avg, (idx + 1).ToString(), frMain.Offset.Data[frMain.Offset.dat + i, idx], frMain.Offset.Path + frMain.Offset.File);
            //            }
            //        }
            //    }
            //    #endregion
            //}
            //catch
            //{ } 
            #endregion
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            //dgvOffset 저장 기본 값으로 Xml파일 저장

            //string strFileName = "XmlData.xml";       //2018.10.06 Kim 수정중

            //using (XmlWriter wr = XmlWriter.Create(strXmlLoadDefault + strFileName))
            //{
            //    try
            //    {
            //        wr.WriteStartElement("Calibration");
            //        wr.WriteStartElement("Wafer");
            //        wr.WriteAttributeString("ID", txtWaferID.Text);     //WaferID
            //        wr.WriteElementString("Name", lbName.Text);  //Temperature, Plazma, Hybrid
            //        wr.WriteElementString("SensorCount", lbDepatcher.Text);          //73, 103, 44
            //        wr.WriteElementString("CalCount", calibrationCount.ToString());
            //        wr.WriteEndDocument();

            //        int calibrationCount = 0;
            //        string[] buff;
            //        for (int i = 1; i < dgvOffset.ColumnCount; i++)
            //        {
            //            if (dgvOffset.Rows[0].Cells[i].Value.ToString() != "")
            //            {
            //                calibrationCount++;

            //                buff = new string[int.Parse(lbDepatcher.Text) + 1];

            //                for (int j = 0; j < int.Parse(lbDepatcher.Text); j++)
            //                {
            //                    buff[j + 1] = dgvOffset.Rows[j].Cells[i].Value.ToString();
            //                    wr.WriteElementString("k" + j+1, buff[j + 1]);
            //                }
            //            }
            //        }
            //    }
            //    catch
            //    {

            //    }
            //}
        }

        void XmlFileLoad(string file)      //2018.10.06 Kim
        {
            #region MyRegion
            //string file = lstModel.Items[lstModel.SelectedIndex].ToString();        //2018.10.05 Kim lstModel Select한거 찾기 10.06주석 처리 
            txtFileName.Text = file;

            for (int row = 0; row < dgvOffset.RowCount; row++)      //row, column 값만 초기화
            {
                for (int cell = 1; cell < dgvOffset.Columns.Count; cell++)
                {
                    dgvOffset.Rows[row].Cells[cell].Value = "";
                }
            }

            List<List<string>> list = xmlFunc.Read(file);
            try
            {
                int CalCount = int.Parse(list[0][3].ToString());        //2018.10.05 Kim
                int SensorCount = int.Parse(list[0][2].ToString());
                int K = 4;      //CalData 처음에 [0][4]로 들어가서 생성

                for (int i = 0; i < CalCount; i++)
                {
                    for (int ColumCount = 3; ColumCount < column.Length; ColumCount++)
                    {
                        if (list[0][K] == column[ColumCount].Replace("K(0", "").Replace(")", "").Replace("K(", ""))
                        {
                            for (int j = 0; j < SensorCount; j++)
                            {
                                dgvOffset.Rows[j].Cells[ColumCount].Value = list[0][j + K + 1];
                            }
                            K = K + 74;
                            break;
                        }
                    }

                }

                if (File.Exists(file.ToString()))        //2018.10.05 Kim
                {
                    using (XmlReader rd = XmlReader.Create(file.ToString()))
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            rd.Read();

                            if (rd.IsStartElement())
                            {
                                //Model List
                                if (rd.Name == "Wafer")
                                {
                                    string ID = rd["ID"];

                                    txtWaferID.Text = ID;
                                    break;
                                }

                                else
                                {
                                    txtWaferID.Text = "WaferID 이상.";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Calibration File 이상.");
                txtWaferID.Text = "Calibration File 이상.";
                Console.WriteLine(ex);
            }
            #endregion
        }
    }
}
