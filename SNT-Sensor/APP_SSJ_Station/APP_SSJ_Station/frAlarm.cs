﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP_SSJ_Station
{
    using System.Diagnostics;
    public partial class frAlarm : Form
    {
        public frAlarm()
        {
            InitializeComponent();
        }

        int iTimeCount = 5;
        Stopwatch swWatch;
        TimeSpan TimeSpan = new TimeSpan();
        public frAlarm(int idx, string msg)
        {
            InitializeComponent();

            lbHeader.Text = "Warning";
            lbMsg.Text = msg;

            switch (idx)
            { 
                case 0:
                    pnTitle.BackColor = Color.Ivory;
                    break;

                case 1:
                    pnTitle.BackColor = Color.Salmon;
                    break;
                default:
                    pnTitle.BackColor = Color.White;
                    break;
            }

            tmrToggle.Interval = 1000;
            tmrToggle.Start();
            lbTimeOut.Text = string.Format("Auto Close [{0:00}] --->  ", iTimeCount--);
        }

        public frAlarm(int idx, string msg, Point pt)
        {
            InitializeComponent();

            lbHeader.Text = "Warning";
            lbMsg.Text = msg;

            this.Location = pt;

            switch (idx)
            {
                case 0:
                    pnTitle.BackColor = Color.Ivory;
                    break;

                case 1:
                    pnTitle.BackColor = Color.Salmon;
                    break;
                default:
                    pnTitle.BackColor = Color.White;
                    break;
            }

            tmrToggle.Interval = 1000;
            tmrToggle.Start();
            lbTimeOut.Text = string.Format("Auto Close [{0:00}] --->  ", iTimeCount--);
        }

        public frAlarm(int idx, string msg, Stopwatch sw)
        {
            InitializeComponent();

            
            lbMsg.Text = msg;
            swWatch = sw;
            switch (idx)
            {
                case 0:
                    pnTitle.BackColor = Color.Ivory;
                    lbHeader.Text = "Warning";
                    lbHeader.ForeColor = Color.BlueViolet;
                    break;
                case 1:
                    pnTitle.BackColor = Color.Salmon;
                    lbHeader.Text = "Alarm";
                    lbHeader.ForeColor = Color.White;
                    break;
                default:
                    pnTitle.BackColor = Color.White;
                    lbHeader.Text = "";
                    lbHeader.ForeColor = Color.Black;
                    break;
            }

            tmrToggle.Interval = 1000;
            tmrToggle.Start();
            lbTimeOut.Text = string.Format("경과시간 : {1:00}:{2:00} / Auto Close [{0:00}] --->  ", iTimeCount--, swWatch.Elapsed.Minutes, swWatch.Elapsed.Seconds);
        }
        public frAlarm(int idx, string msg, Stopwatch sw, Point pt)
        {
            InitializeComponent();

            this.Location = pt;

            //this.TopMost = true;

            lbMsg.Text = msg;
            swWatch = sw;
            switch (idx)
            {
                case 0:
                    pnTitle.BackColor = Color.Ivory;
                    lbHeader.Text = "Warning";
                    lbHeader.ForeColor = Color.BlueViolet;
                    break;
                case 1:
                    pnTitle.BackColor = Color.Salmon;
                    lbHeader.Text = "Alarm";
                    lbHeader.ForeColor = Color.White;
                    break;
                default:
                    pnTitle.BackColor = Color.White;
                    lbHeader.Text = "";
                    lbHeader.ForeColor = Color.Black;
                    break;
            }
            //swWatch.Elapsed.Add(TimeSpan)
            tmrToggle.Interval = 1000;
            tmrToggle.Start();
            lbTimeOut.Text = string.Format("경과시간 : {1:00}:{2:00} / Auto Close [{0:00}] --->  ", iTimeCount--, swWatch.Elapsed.Minutes, swWatch.Elapsed.Seconds);
        }

        public frAlarm(int idx, string msg, Stopwatch sw, TimeSpan ts, Point pt)
        {
            InitializeComponent();

            this.Location = pt;

            //this.TopMost = true;

            lbMsg.Text = msg;
            swWatch = sw;
            //swWatch.Elapsed.Add(ts);
            switch (idx)
            {
                case 0:
                    pnTitle.BackColor = Color.Ivory;
                    lbHeader.Text = "Warning";
                    lbHeader.ForeColor = Color.BlueViolet;
                    break;
                case 1:
                    pnTitle.BackColor = Color.Salmon;
                    lbHeader.Text = "Alarm";
                    lbHeader.ForeColor = Color.White;
                    break;
                default:
                    pnTitle.BackColor = Color.White;
                    lbHeader.Text = "";
                    lbHeader.ForeColor = Color.Black;
                    break;
            }
            //swWatch.Elapsed.Add(TimeSpan)
            tmrToggle.Interval = 1000;
            tmrToggle.Start();
            TimeSpan = ts;
            int min = swWatch.Elapsed.Add(ts).Minutes;
            int sec = swWatch.Elapsed.Add(ts).Seconds;
            lbTimeOut.Text = string.Format("경과시간 : {1:00}:{2:00} / Auto Close [{0:00}] --->  ", iTimeCount--, min, sec);
            //Console.WriteLine
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            tmrToggle.Stop();
            Close();
        }

        private void tmrToggle_Tick(object sender, EventArgs e)
        {
            if (swWatch != null)
            {

                lbTimeOut.Text = string.Format("경과시간 : {1:00}:{2:00} / Auto Close [{0:00}] --->  ", iTimeCount--, swWatch.Elapsed.Add(TimeSpan).Minutes, swWatch.Elapsed.Add(TimeSpan).Seconds);
            }
            else
            {
                lbTimeOut.Text = string.Format("Auto Close [{0:00}] --->  ", iTimeCount--);
            }
            if (iTimeCount < 0)
            {
                tmrToggle.Stop();
                Close();
            }
        }
    }
}
