﻿//test

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP_SSJ_Station
{
    using System.IO;
    using JDX_Library_X86.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Threading;
    using System.Xml;
    
    public partial class frMain : Form
    {
        public frMain()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size;
        }
       
        #region Var
        //public clsFunExcel excel;
        public clsWork work;

        public frConfigDev _frConfigDev;
        public frConfigUser _frConfigUser;
        public frOffset _frOffset;
        public frAlarm _frAlarm;
        public frModel _frModel;
        

        public TextBox[] txtLogInfo = new TextBox[12];
        public Label[] lbStatus = new Label[5];
        public Stopwatch swRxflagTime = new Stopwatch();
        public Stopwatch swTxflagTime = new Stopwatch();
        public Stopwatch swBattLevel = new Stopwatch();
        public DateTime dtGetStartTime = new DateTime();
        public DateTime dtGetEndTime = new DateTime();

        public Stopwatch swGetteringTime = new Stopwatch();
        public Stopwatch swReadTimeOut = new Stopwatch();
        public Stopwatch swReadTime = new Stopwatch();

        public TimeSpan tsOverStack = new TimeSpan();
        public TimeSpan tsCurrStack = new TimeSpan();

        public Stopwatch swStatusTime = new Stopwatch();

        public Stopwatch swAlarmTime = new Stopwatch();
        public Stopwatch swSowToRecvRemainTime = new Stopwatch();
        public Stopwatch swStationCheckingTime = new Stopwatch();
        public Stopwatch swStationStayTime = new Stopwatch();

        public int[] iStatus = new int[8];

        public bool bTempGetStart = false;                  //온도수집시작
        //public bool bTempRead = false;                      //온도데이터 읽기
        public bool bTempGetEnd = false;                    //온도수집종료
        //public bool bWaiting = false;                           //대기중
        //public bool bCharging = false;                          //충전중

        //public bool bRawsDataLoad = false;
        public bool bOnlyReadData = false;
        public bool bUserConfig = false;
        public string OffSetWaferID;



        public enum EModel
        {
            Temp73, Temp103, Plazma40
        }

        public struct ST_SeletedModel
        {
            public string ID;
            public string Name;
            public string Depatcher;
        }

        public ST_SeletedModel SelectdModel = new ST_SeletedModel();

        #endregion

        #region Form Struct
        void _Array()
        {
            lbStatus[0] = lbState0;
            lbStatus[1] = lbState1;
            lbStatus[2] = lbState2;
            lbStatus[3] = lbState3;
            lbStatus[4] = lbState4;

            txtLogInfo[0] = txtLog0;
            txtLogInfo[1] = txtLog1;
            txtLogInfo[2] = txtLog2;
            txtLogInfo[3] = txtLog3;
            txtLogInfo[4] = txtLog4;
            txtLogInfo[5] = txtLog5;
            txtLogInfo[6] = txtLog6;
            txtLogInfo[7] = txtLog7;
            txtLogInfo[8] = txtLog8;
            txtLogInfo[9] = txtLog9;
            txtLogInfo[10] = txtLog10;
            txtLogInfo[11] = txtLog11;
        }

        void _InitDataGridview()
        {
            dgvData.Rows.Clear();
            dgvData.Columns.Clear();

            int SensorCount = int.Parse(SelectdModel.Depatcher);//2018.09.15 jace

            dgvData.ColumnCount = SensorCount + 1; //Columan 0 Default Data
            dgvData.Columns[0].Name = "DATA";

            for (int i = 0; i < SensorCount; i++)
            {
                dgvData.Columns[i + 1].Name = "S" + (i).ToString();
            }
        }

        void _OffsetLoad()
        {
            //file data
            _DefaultOffsetSave();
            if (!Directory.Exists(Offset.Path))
            {
                //Directory 없는 경우 생성후 Open Directory 경로 설정
                Directory.CreateDirectory(Offset.Path);
                //clsINIFile.SetValue("INFO", "OFFSET_PATH", Offset.Path, AppInfo.SettingPath + AppInfo.SettingFile);
                clsINIFile.SetValue("INFO", "OFFSET_FILE", Offset.File, AppInfo.SettingPath + AppInfo.SettingFile);
            }
            else
            {
                //Offset.Path = clsINIFile.GetValue("INFO", "OFFSET_PATH", AppInfo.SettingPath + AppInfo.SettingFile);
                Offset.Path = Application.StartupPath + "\\OffSet_Data\\";
                Offset.File = clsINIFile.GetValue("INFO", "OFFSET_FILE", AppInfo.SettingPath + AppInfo.SettingFile);
                try
                {

                    Offset.dValue = double.Parse(clsINIFile.GetValue(Offset.Key_Dat, "Val", Offset.Path + Offset.File));
                }
                catch
                {
                    Offset.dValue = 0;
                    clsINIFile.SetValue(Offset.Key_Dat, "Val", "0",Offset.Path + Offset.File);
                }
                

                for (int i = 0; i < 2; i++)
                {
                    for (int idx = 0; idx < int.Parse(SelectdModel.Depatcher); idx++)
                    {
                        Offset.Data[i, idx] = clsINIFile.GetValue("XY", i.ToString() + idx.ToString(), Offset.Path + Offset.File);

                    }
                }
            }

            Console.WriteLine("OFFSET Sucess");
        }

        void _DefaultOffsetSave()
        {
            //if (File.Exists(Application.StartupPath + "\\mdcek.dat"))
            //    return;
            Offset.Data = new string[11, int.Parse(SelectdModel.Depatcher)];
            string[] dat = clsLog.ReadLog(Application.StartupPath+"\\mdcek.dat");

            string[] con = new string[dat.Length];

            for (int i = 0; i < dat.Length; i++)
            {
                string[] userDat = dat[i].Split(',');
                con[i] = userDat[1];
            }
            //using (XmlWriter wr = XmlWriter.Create(Application.StartupPath + "\\calcMaster.xml"))
            //{
            //    //wr.WriteStartElement("Calibration");
            //    for (int i = 0; i < con.Length; i++)
            //    {
            //        //wr.WriteStartElement("K" + ((40 + (i * 10)).ToString()));
            //        string[] val = con[i].Split('\t');
            //        for (int idx = 0; idx < val.Length; idx++)
            //        {
            //            //wr.WriteElementString("k" + (idx + 1).ToString(), val[idx]);
            //            Offset.Data[i + 2, idx] = val[idx];
            //        }
            //        //wr.WriteEndElement();
            //    }
            //    //wr.WriteEndElement();
            //    //wr.WriteEndDocument();
            //}
            try
            {
            for (int i = 0; i < con.Length; i++)
            {
                //wr.WriteStartElement("K" + ((40 + (i * 10)).ToString()));
                string[] val = con[i].Split('\t');
                for (int idx = 0; idx < val.Length; idx++)
                {
                    //wr.WriteElementString("k" + (idx + 1).ToString(), val[idx]);
                    Offset.Data[i + 2, idx] = val[idx];
                }
                //wr.WriteEndElement();
            }

            Console.WriteLine("Calibration Load Sucess");
            }

            catch
            {

            }
        }

        public struct AppInfo
        {
            public static bool bMainLocation = false;
            public static Point ptBeforeMouse = new Point();


            public static string SettingFile = "mdsys.dat";
            public static string SettingPath = Application.StartupPath + "\\";

            //Offset File info
            public static string Key_Section = "INFO";
            public static string Key_Path = "OFFSET_PATH";
            public static string Key_File = "OFFSET_FILE";
        }

        public struct Offset
        {
            //0:X, 1:Y, 2:TempCalc
            public static string[,] Data = new string[11, 73];
            
            public static int x = 0;
            public static int y = 1;
            public static int dat = 2;

            public static double dValue = 0;

            public static string File = "cen.dat";
            public static string Path = Application.StartupPath + "\\CalibrationData\\";

            public static string Key_XY = "XY";
            public static string Key_Offset = "MTP";
            public static string Key_High = "HIGH";
            public static string Key_Low = "LOW";
            public static string Key_Avg = "AVG";
            public static string Key_Dat = "Dat";
        } 
        #endregion


        #region Form Load Close
        private void frMain_Load(object sender, EventArgs e)
        {
            _Array();

            //2018.09.10
            ModelLoad();
            Offset.Data = new string[11, int.Parse(SelectdModel.Depatcher)];

            _InitDataGridview();

            _OffsetLoad();

            //excel = new clsFunExcel();

            btnSerialReflash_Click(null, null);

            work = new clsWork();
            work.RxEvent += new clsWork.RxDataEventHandler(WorkEvent);

            tmrUpdate.Start();

            iStatusFlag = iReady;
        }

       
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            clsINIFile.SetValue(AppInfo.Key_Section, AppInfo.Key_File, Offset.File, AppInfo.SettingPath + AppInfo.SettingFile);
            string strPath = Application.StartupPath + "\\OffSet_Data\\";
            clsINIFile.SetValue(AppInfo.Key_Section, AppInfo.Key_Path, strPath, AppInfo.SettingPath + AppInfo.SettingFile);

            Close();
        }

        private void frMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
            Environment.Exit(0);
            Application.ExitThread();
        }
        #endregion

        #region Model Load Save
        void ModelLoad()
        {
            XmlDocument xmlDocument = new XmlDocument();
            string pathfile = Application.StartupPath + "\\mod.xml";
            if (File.Exists(pathfile))
            {
                using (XmlReader rd = XmlReader.Create(pathfile))
                {
                    while (rd.Read())
                    {

                        if (rd.IsStartElement())
                        {
                            //Model List
                            //if (rd.Name == "Model")
                            //{
                            //    // attribute 읽기                            
                            //    string id = rd["Id"]; // rd.GetAttribute("Id");
                            //    if(!string.IsNullOrEmpty(id))
                            //    {
                            //        rd.Read();   // 다음 노드로 이동        

                            //        // Element 읽기
                            //        string name = rd.ReadElementContentAsString("Name", "");
                            //        string dept = rd.ReadElementContentAsString("Dept", "");

                            //        Console.WriteLine(id + "," + name + "," + dept);
                            //    }
                            //}
                            if (rd.Name == "Selected")
                            {
                                string id = rd["Id"]; // rd.GetAttribute("Id");
                                rd.Read();   // 다음 노드로 이동        

                                // Element 읽기
                                string name = rd.ReadElementContentAsString("Name", "");
                                string dept = rd.ReadElementContentAsString("Dept", "");

                                //Console.WriteLine(id + "," + name + "," + dept);

                                SelectdModel.ID = id;
                                SelectdModel.Name = name;
                                SelectdModel.Depatcher = dept;
                            }
                        }
                    }
                }
            }
            else
            {
                ModelSave();
                ModelLoad();
            }
        }

        void ModelSave()
        {
            //디폴트
            using (XmlWriter wr = XmlWriter.Create(Application.StartupPath + "\\mod.xml"))
            {
                wr.WriteStartElement("Model");

                // Employee#1001
                wr.WriteStartElement("Model");
                wr.WriteAttributeString("Id", "1001");  // attribute 쓰기
                wr.WriteElementString("Name", "Temperature");   // Element 쓰기
                wr.WriteElementString("Dept", "73");
                wr.WriteEndElement();

                // Employee#1002
                wr.WriteStartElement("Model");
                wr.WriteAttributeString("Id", "1002");
                wr.WriteElementString("Name", "Temperature");
                wr.WriteElementString("Dept", "103");
                wr.WriteEndElement();

                // Employee#1003
                wr.WriteStartElement("Model");
                wr.WriteAttributeString("Id", "1003");
                wr.WriteElementString("Name", "Plasma");
                wr.WriteElementString("Dept", "44");
                wr.WriteEndElement();

                wr.WriteStartElement("Selected");
                wr.WriteAttributeString("Id", "1001");
                wr.WriteElementString("Name", "Temperature");
                wr.WriteElementString("Dept", "73");
                wr.WriteEndElement();

                wr.WriteEndDocument();
            }
        } 
        #endregion

        #region Func Main Move
        private void lbTitle_DoubleClick(object sender, EventArgs e)
        {
            //폼 확대 축소
        }

        private void lbTitle_MouseDown(object sender, MouseEventArgs e)
        {
            AppInfo.bMainLocation = true;
            AppInfo.ptBeforeMouse = new Point(e.X, e.Y);
        }

        private void lbTitle_MouseUp(object sender, MouseEventArgs e)
        {
            AppInfo.bMainLocation = false;
        }

        private void lbTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (AppInfo.bMainLocation)
            {
                if (AppInfo.ptBeforeMouse.X != e.X)
                {
                    this.Location = new Point(this.Location.X - AppInfo.ptBeforeMouse.X + e.X, this.Location.Y - AppInfo.ptBeforeMouse.Y + e.Y);
                }
                else if (AppInfo.ptBeforeMouse.Y != e.Y)
                {
                    this.Location = new Point(this.Location.X - AppInfo.ptBeforeMouse.X + e.X, this.Location.Y - AppInfo.ptBeforeMouse.Y + e.Y);
                }
            }
        }
        #endregion

        #region Func Timer Gui Update

        Point ptAlarmLos = new Point(0, 0);
        Stopwatch swSowTimeOut = new Stopwatch();

        private void tmrUpdate_Tick(object sender, EventArgs e)
        {
            #region Hidden Config Form
            if (swRecognition.ElapsedMilliseconds > 3000)
            {
                swRecognition.Reset();
                swRecognition.Stop();

                if (_frConfigDev == null || _frConfigDev.IsDisposed)
                {
                    _frConfigDev = new frConfigDev(this);
                    _frConfigDev.Show();
                }
                else
                {
                    _frConfigDev.Focus();
                }
            }
            if (swModelFormLoadTime.ElapsedMilliseconds > 1000)
            {
                swModelFormLoadTime.Reset();
                swModelFormLoadTime.Stop();

                if (_frModel == null || _frModel.IsDisposed)
                {
                    _frModel = new frModel(this);
                    _frModel.Show();
                }
                else
                {
                    _frModel.Focus();
                }
            }
            #endregion

            if (lbDebugMsg.Text == "Model Change")
            {
                lbDebugMsg.Text = "Serial Msg";
                ModelLoad();
                _InitDataGridview();
                _OffsetLoad();
            }

            //Status Input
            if (work.Status() && !string.IsNullOrEmpty(strGetConfigData))
            {
                swStatusTime.Start();

                 if (swStatusTime.ElapsedMilliseconds > 300)
                {
                    swStatusTime.Reset();

                    if (iStatus[4] == 2)
                    {
                        if (iStatusFlag != iDatConverting)
                        {
                            if (iStatus[2] == 1)
                            {
                                work.Get_Status();
                            }
                            else if (iStatus[4] == 1)
                            {
                                work.Get_Batt();
                            }
                            else if (iStatus[0] == 1)
                            {
                                if (strStartAcquisition != lbTempStartTime.Text)
                                {
                                    lbTempStartTime.Text = strStartAcquisition;     //09.21 KimIkHwan 수정
                                }

                                if (txtLog4.Text == "")
                                {
                                    work.Get_AcqisitionDate();
                                }

                                if (!bTempGetStart)
                                {
                                    work.Get_AcqisitionDate();
                                    bTempGetStart = true;
                                    bTempGetEnd = false;
                                    iStatusFlag = iGettering;
                                    dtGetStartTime = DateTime.Now;

                                    lbTempStartTime.Text = strStartAcquisition;     //09.21 KimIkHwan 수정

                                    lbTempEndTime.Text = "00시 00분 00초";
                                    pBarSend.Value = 0;
                                    pBarRecevied.Value = 0;
                                    swAlarmTime.Restart();
                                    //swGetteringTime.Restart();
                                    swStationStayTime.Restart();
                                    //bOnlyReadData = true;     09.21 KimIkHwan 수정


                                    if (iStatus[3] == 2)
                                    {
                                        swStationStayTime.Stop();
                                    }

                                    if (_frAlarm == null || _frAlarm.IsDisposed)
                                    {
                                        _frAlarm = new frAlarm(100, "[PC -> 온도 수집 시작] 온도 수집 진행 상태", swAlarmTime, ptAlarmLos);
                                        _frAlarm.Show();
                                    }
                                    else
                                    {
                                        _frAlarm.Focus();
                                    }
                                }

                                if (bTempGetStart)
                                {
                                    iStatusFlag = iGettering;
                                    //분초
                                    int min = swGetteringTime.Elapsed.Add(timeSpan).Minutes;
                                    int sec = swGetteringTime.Elapsed.Add(timeSpan).Seconds;

                                    if(iTempcolPossibleTime <= 0)       //2018.10.03 Kim
                                    {
                                        iTempcolPossibleTime = 15;
                                        lbBattState.Text = "매우좋음";
                                    }

                                    iTotalSec = iTempcolPossibleTime * 60;
                                    // iTotalSec = iTempAvailableTime * 60;

                                    int iRemainSec = min * 60 + sec;
                                    if (iRemainSec != 0)
                                    {
                                        int rate = ((iRemainSec) * 100) / iTotalSec;
                                        if (rate <= 100)
                                        {
                                            pBarDiscard.Value = rate;
                                        }
                                    }
                                }

                                work.Get_Status();
                            }
                            else if (iStatus[1] == 1 || iStatus[6] == 1)
                            {
                                if (!bTempGetEnd)
                                {
                                    bTempGetEnd = true;
                                    //iStatusFlag = iDatSending;
                                    swAlarmTime.Stop();
                                    if(bOnlyReadData == true)
                                    {
                                        dtGetEndTime = DateTime.Now;
                                        lbTempEndTime.Text = string.Format("{0:00}시 {1:00}분 {2:00}초", dtGetEndTime.Hour, dtGetEndTime.Minute, dtGetEndTime.Second);
                                    }
                                    swGetteringTime.Stop();

                                    if (_frAlarm == null || _frAlarm.IsDisposed)
                                    {
                                        _frAlarm = new frAlarm(100, "[PC -> 온도 수집 완료] Sow Data --> Station", swAlarmTime, ptAlarmLos);
                                        _frAlarm.Show();
                                    }
                                    else
                                    {
                                        _frAlarm.Focus();
                                    }
                                }

                                work.Get_Sen_Sta_Status();

                                if (!swSowToRecvRemainTime.IsRunning)
                                {
                                    swSowToRecvRemainTime.Reset();
                                    swSowToRecvRemainTime.Start();
                                }
                            }
                            else
                            {
                                work.Get_Status();
                            }
                        }
                        else
                        {
                            //전체 페이지 개수가 있으면 읽기 요청
                            if (iStatusFlag == iReady)
                            {
                                work.Get_Status();
                            }
                        }
                    }
                    else
                    {
                        work.Get_Batt();
                    }

                    if (iStatus[0] == 2 && iStatus[1] == 2)
                    {
                        if (iStatusFlag != iDatConverting)
                        {
                            work.Get_Status();
                        }
                    }
                }
            }

            //Alarm Location
            
            try
            {
                Point ptDgv = dgvData.Location;
                ptDgv = dgvData.PointToScreen(ptDgv);
                Size szDgv = dgvData.Size;
                Size szAlarmForm = new Size(1248, 347);
                
                int GetLosX = (szDgv.Width - szAlarmForm.Width) /2 + ptDgv.X;
                int GetLosY = (szDgv.Height - szAlarmForm.Height) / 2 + ptDgv.Y;
                ptAlarmLos = new Point(GetLosX, GetLosY);
            }
            catch
            { }

            //swAlarmTime.Start();
            if (btnNfcState.BackColor == Color.Red)
            {
                swSowTimeOut.Start();

                if (swSowTimeOut.Elapsed.Seconds > 15)
                {
                    if (iStatus[0] == 2 && iStatus[1] == 2 && iStatus[3] == 2 && iStatus[4] == 2)
                    {
                        if (iStatusFlag != iDatConverting)
                        {
                            if (_frAlarm == null || _frAlarm.IsDisposed)
                            {
                                _frAlarm = new frAlarm(1, "[SOW] 센서보드 임의 이동 되었습니다.", swSowTimeOut, ptAlarmLos);
                                _frAlarm.Show();
                            }
                            else
                            {
                                //_frAlarm.Focus();

                            }
                        }
                    }
                    else if ((iStatus[0] == 2 && iStatus[3] == 1 && iStatus[4] == 1))
                    {
                        _frAlarm.Close();
                    }
                }
            }
            else
            {
                swSowTimeOut.Stop();
                swSowTimeOut.Reset();
            }

            #region Alarm Display
            if (swAlarmTime.IsRunning)
            {
                if (iStatus[0] == 1)
                {
                    if (swAlarmTime.Elapsed.Minutes < 15)
                    {
                        if (_frAlarm == null || _frAlarm.IsDisposed)
                        {
                            //swGetteringTime.Elapsed.Add(timeSpan);
                            //_frAlarm = new frAlarm(100, "[SOW] 온도 측정 데이터 수집 중 입니다.", swGetteringTime.Elapsed.Add(timeSpan).Minutes, swGetteringTime.Elapsed.Add(timeSpan).Seconds, ptAlarmLos);
                            _frAlarm = new frAlarm(100, "[SOW] 온도 측정 데이터 수집 중 입니다.", swGetteringTime, timeSpan, ptAlarmLos);
                            _frAlarm.Show();
                        }
                    }

                    //if (iStatus[3] == 1)
                    //{
                    //    if (swStationStayTime.Elapsed.Seconds >= 175 && swStationStayTime.Elapsed.Seconds <= 185)
                    //    {
                    //        _frAlarm = new frAlarm(1, "[스테이션 초기화] 3분동안 동작을 안해 스테이션이 종료 되었습니다.", ptAlarmLos);
                    //        _frAlarm.Show();
                    //        swStationStayTime.Stop();

                    //        this.btnResetTemperature_Click(null, null);
                    //    }
                    //}
                    //else if (iStatus[3] == 2)
                    //{
                    //    swStationStayTime.Stop();
                    //}

                    //else
                    //{
                    //    //_frAlarm.Focus();
                    //}

                    //if (swAlarmTime.Elapsed.Minutes >= 1)
                    //{
                    //    _frAlarm.Close();
                    //}
                    //else
                    //{
                    //    if (_frAlarm == null || _frAlarm.IsDisposed)
                    //    {
                    //        _frAlarm = new frAlarm(100, "[SOW] 온도 측정 데이터 수집 중 입니다.", swAlarmTime, ptAlarmLos);
                    //        _frAlarm.Show();
                    //    }
                    //    else
                    //    {
                    //        _frAlarm.Focus();
                    //    }
                    //}

                    else if (swAlarmTime.Elapsed.Minutes <= 20)
                    {
                        if (swAlarmTime.Elapsed.Minutes >= 16)
                        {
                            _frAlarm.Close();
                        }
                        else
                        {
                            if (_frAlarm == null || _frAlarm.IsDisposed)
                            {
                                _frAlarm = new frAlarm(0, "[SOW] 스테이션으로 이동하여 주시기 바랍니다.", swAlarmTime, ptAlarmLos);
                                _frAlarm.Show();
                            }
                            else
                            {
                                //_frAlarm.Focus();
                            }
                        }
                    }
                    else
                    {
                        if (_frAlarm == null || _frAlarm.IsDisposed)
                        {
                            _frAlarm = new frAlarm(1, "[SOW] 스테이션으로 { 즉시 } 이동하여 주시기 바랍니다.", swAlarmTime, ptAlarmLos);
                            _frAlarm.Show();
                        }
                        else
                        {
                            _frAlarm.Focus();
                        }
                    }
                }
            }

            if(work.iGetPage == 0)
            {
                if (iStatusFlag == iBatChaging)
                {

                    if (_frAlarm == null || _frAlarm.IsDisposed)
                    {
                        _frAlarm = new frAlarm(100, "[스테이션 충전중] 잠시만 기다려 주세요.", ptAlarmLos);
                        _frAlarm.Show();
                    }
                }
            }


            if (swStationCheckingTime.IsRunning)
            {
                if (iStatus[0] == 2 && iStatus[1] == 2)
                {
                    if (swStationCheckingTime.Elapsed.Minutes < 3)
                    {
                        if (_frAlarm == null || _frAlarm.IsDisposed)
                        {
                            _frAlarm = new frAlarm(100, "[스테이션 상태 확인중] 잠시만 기다려 주세요.", swStationCheckingTime, ptAlarmLos);
                            _frAlarm.Show();
                        }
                    }
                }

                else if (iStatus[0] == 1 || iStatus[1] == 1)
                {
                    _frAlarm.Close();
                    swStationCheckingTime.Stop();
                }
            }
            #endregion

            #region Update
            ConfigDataUpdate();
            lbDateTime.Text = DateTime.Now.ToString("yyyy-MM-dd\n tt hh:mm:ss");
            lbDataCount.Text = string.Format("Total : {0} / Page : {1} / Error : {2}", work.iTotalPage, work.iGetPage, work.iReadCount);
            lbTempDebug.Text = string.Format("Read Page : {0}", dgvData.Rows.Count - 1);
            lbRecivedRate.Text = pBarRecevied.Value.ToString() + "%";
            lbSendRate.Text = pBarSend.Value.ToString() + "%";
            lbVoltageRate.Text = pBarVoltage.Value.ToString() + "%";
            lbDiscardRate.Text = pBarDiscard.Value.ToString() + "%";
            lbTempTime.Text = lbDiscardRate.Text;
            if (bOnlyReadData == true)
            {
                //iTemppingHours = swGetteringTime.Elapsed.Hours + timeSpan.Hours;              09.21 KimIkHwan 수정
                //iTemppingMin = swGetteringTime.Elapsed.Minutes + timeSpan.Minutes;
                //iTemppingSec = swGetteringTime.Elapsed.Seconds + timeSpan.Seconds;
                //lbTemppingTime.Text = string.Format("{0}시간 {1:00}분 {2:00}초", iTemppingHours, iTemppingMin, iTemppingSec);
                lbTemppingTime.Text = string.Format("{0}시간 {1:00}분 {2:00}초", swGetteringTime.Elapsed.Add(timeSpan).Hours, swGetteringTime.Elapsed.Add(timeSpan).Minutes, swGetteringTime.Elapsed.Add(timeSpan).Seconds);
            }
            //if (work.iTotalPage < work.iGetPage)
            //{
            //    work.iReadCount++;
            //}

            try
            {
                if (work.iTotalPage != 0)
                {

                    pBarRecevied.Value = (int)(work.iGetPage * 100 / work.iTotalPage);
                }
                else
                {
                    pBarRecevied.Value = 0;
                }
            }
            catch
            {

            }

            if (TotalTimePcDat > 0)                //2018.09.03 수정
            {
                //double remaintime = (double)(TotalTimePcDat - swReadTime.ElapsedMilliseconds) / 1000d;
                double remaintime = TotalTimePcDat - swReadTime.ElapsedMilliseconds;
                int time = Convert.ToInt32(remaintime * 0.001 % 3600);
                int min = time / 60;
                int sec = time % 60;

                if (remaintime < 0)
                {
                    lbRecivedTime.Text = string.Format("예상시간 : {0}초", 0);
                }
                else
                {
                    if(min <= 0)
                    {
                        lbRecivedTime.Text = string.Format("예상시간 : {0}초", sec);
                    }
                    else
                    {
                        lbRecivedTime.Text = string.Format("예상시간 : {0}분 {1}초", min, sec);
                    }
                }

                if (!swReadTime.IsRunning)
                {
                    lbRecivedTime.Text = string.Format("예상시간 : {0}초", 0);
                }
            }
            else
            {
                lbRecivedTime.Text = string.Format("예상시간 : {0}초", 0);
            }


            if (TotalTime > 0)
            {

                if (iStatusFlag == iDatSending)
                {
                    //double remaintime = (double)(TotalTime - swSowToRecvRemainTime.ElapsedMilliseconds) / 1000d;
                    double remaintime = TotalTime - swSowToRecvRemainTime.ElapsedMilliseconds;
                    int time = Convert.ToInt32(remaintime * 0.001 % 3600);
                    int min = time / 60;
                    int sec = time % 60;

                    if (remaintime < 0)
                    {
                        lbSendTime.Text = string.Format("예상시간 : {0}초", 0);
                    }
                    else
                    {
                        if (min <= 0)
                        {
                            lbSendTime.Text = string.Format("예상시간 : {0}초", sec);

                            iStatusFlag = iDatSending;
                        }
                        else
                        {
                            lbSendTime.Text = string.Format("예상시간 : {0}분 {1}초", min, sec);
                            iStatusFlag = iDatSending;
                        }
                    }
                }
                else if (iStatusFlag == iReady)
                {
                    lbSendTime.Text = string.Format("예상시간 : {0}초", 0);
                }
            }
            else
            {
                lbSendTime.Text = string.Format("예상시간 : {0}초", 0);
            }

            if(pBarSend.Value == 100)
            {
                lbSendTime.Text = string.Format("예상시간 : {0}초", 0);
            }



            if (!string.IsNullOrEmpty(lbBattLevel.Text))
            {
                lbBattLevel.Text = lbBattLevel.Text.Replace("V", null);
                lbBattLevel.Text = lbBattLevel.Text.Replace(" ", null);
                lbBattLevel.Text = lbBattLevel.Text.PadRight(4);
                double dVal = double.Parse(lbBattLevel.Text);

                double iBattMin = 0.0d;
                double iBattMax = 4.2d - iBattMin;
                if (dVal >= 0.1)
                {
                    try
                    {
                        pBarVoltage.Value = (int)(((dVal - iBattMin)) * 100 / iBattMax);
                    }
                    catch
                    { 
                    
                    }
                }

                lbBattLevel.Text = lbBattLevel.Text + " V";
            }
            else
            {
                lbBattState.Text = "";
            } 
            #endregion

            #region MyRegion
            if (work.Status())
            {
                btnSerialOpen.BackColor = Color.Lime;
            }
            else
            {
                btnSerialOpen.BackColor = SystemColors.Control;
            }

            if (work.isRxState)
            {
                swRxflagTime.Restart();
                btnRx.BackColor = Color.Lime;
                work.isRxState = false;
            }

            if (swRxflagTime.ElapsedMilliseconds > 100)
            {
                swRxflagTime.Stop();
                btnRx.BackColor = SystemColors.Control;
            }

            if (work.isTxState)
            {
                swTxflagTime.Restart();
                btnTx.BackColor = Color.Lime;
                work.isTxState = false;
                //lbDebugMsg.Text = null;
            }

            if (swTxflagTime.ElapsedMilliseconds > 100)
            {
                swTxflagTime.Stop();
                btnTx.BackColor = SystemColors.Control;
            } 
            #endregion

            #region Temp Data Error
            if (swReadTimeOut.ElapsedMilliseconds > 1500)
            {
                if (iStatusFlag != iReady)
                {
                    work.iReadCount++;

                    //Work Reset
                    work.Reset();
                    if (work.iReadCount > 100 || work.iGetPage > work.iTotalPage)
                    {
                        iStatusFlag = iReady;

                        work.iGetPage = 0;
                        work.iReadCount = 0;
                        work.iTotalPage = 0;
                        //_InitDataGridview();
                        //work.Get_AcqisitionDate();
                        //Thread.Sleep(30);
                        //work.Get_TotalPage();
                        //Thread.Sleep(30);
                        //work.Get_TempData(1);
                        //Thread.Sleep(30);
                        //swReadTimeOut.Stop();
                        //swReadTime.Restart();           
                    }
                    else
                    {
                        if (work.iTotalPage != 0 && work.iGetPage < work.iTotalPage)
                        {
                            //Console.WriteLine(swReadTimeOut.ElapsedMilliseconds.ToString() + "ms --- Read Time Out");
                            work.Get_TempData(dgvData.Rows.Count);

                            swReadTimeOut.Restart();

                            //2018.09.20 jace
                            //work.iReadCount++;
                        }
                        else
                        {
                            swReadTimeOut.Stop();
                            swReadTimeOut.Reset();
                            swReadTime.Stop();
                            swReadTime.Reset();
                            //iStatusFlag = iReady;
                        }
                    }


                }
                else
                {
                    swReadTimeOut.Stop();
                    swReadTimeOut.Reset();
                    swReadTime.Stop();
                    swReadTime.Reset();
                }
                
            } 
            #endregion

            StationStatus();
        } 
        #endregion

        #region Func Status
        int iStatusFlag = 4;
        const int iGettering = 0;
        const int iDatSending = 1;
        const int iDatConverting = 2;
        const int iBatChaging = 3;
        const int iReady = 4;
        Stopwatch swToggleTime = new Stopwatch();
        bool bStateToggle = false;

        void StationStatus()
        {
            if (!swToggleTime.IsRunning)
            {
                swToggleTime.Start();
            }

            if (swToggleTime.ElapsedMilliseconds > 500)
            {
                swToggleTime.Restart();

                if (!bStateToggle)
                {
                    bStateToggle = true;
                    lbStatus[iStatusFlag].BackColor = Color.Orange;
                }
                else
                {
                    bStateToggle = false;

                    for (int i = 0; i < lbStatus.Length; i++)
                    {
                        lbStatus[i].BackColor = SystemColors.Control;
                    }
                }
            }
        } 
        #endregion

        #region Func Work Event
        public string strGetConfigData = null;
        public string strStartAcquisition = "0시간 00분 00초";
        int iDataHeadCount = 0;
        int iTotalSec = 0;
        int iTempcolPossibleTime;
        int iRemainSec = 0;

        public double[,] dRawData;
        public double[,] dCalData;

        public string[] Get_Info()
        {
            string[] tmp = new string[12];

            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = txtLogInfo[i].Text;
            }
            return tmp;
        }

        void Cal_Data()
        {
            //if (dgvData.RowCount < 2)
            //    return;

            try
            {
                dCalData = new double[dgvData.RowCount - 1, int.Parse(SelectdModel.Depatcher)];
                dRawData = new double[dgvData.RowCount - 1, int.Parse(SelectdModel.Depatcher)];

                double min = 0;
                double max = 0;
                double[] avg = new double[dgvData.RowCount - 1];
                double sum = 0;
                double center = 0;

                //Raw AVG
                for (int i = 0; i < dgvData.RowCount - 1; i++)
                {
                    sum = 0;
                    for (int u = 0; u < dgvData.ColumnCount - 1; u++)
                    {
                        dRawData[i, u] = double.Parse(dgvData.Rows[i].Cells[u + 1].Value.ToString());
                        sum += dRawData[i, u];
                    }
                    avg[i] = sum / avg.Length;
                }

                //High Low ref Sel
                min = avg.Min();
                max = avg.Max();
                center = (max + min) / 2;
                
                for (int i = 0; i < dgvData.RowCount - 1; i++)
                {
                    for (int u = 0; u < dgvData.ColumnCount - 1; u++)
                    {
                        //Offset.Data[]  
                        double offset = 0;

                        if (string.IsNullOrEmpty(Offset.Data[Offset.dat, u]))
                        {

                        }
                        else
                        {
                            offset = double.Parse(Offset.Data[Offset.dat, u]);
                        }

                        double high = 0;

                        if (string.IsNullOrEmpty(Offset.Data[Offset.dat, u]))
                        {

                        }
                        else
                        {
                            high = double.Parse(Offset.Data[Offset.dat + 1, u]);
                        }

                        double low = 0;

                        if (string.IsNullOrEmpty(Offset.Data[Offset.dat, u]))
                        {

                        }
                        else
                        {
                            low = double.Parse(Offset.Data[Offset.dat + 2, u]);
                        }

                        if (dRawData[i, u] <= center) //low
                        {
                            dCalData[i, u] = dRawData[i, u] + low + offset;
                        }
                        else //high
                        {
                            dCalData[i, u] = dRawData[i, u] + high + offset;
                        }
                    }
                }
            }
            catch
            {
                //Excel File
            }
        }

        long TotalTime = 0;
        long TotalTimePcDat = 0;
        TimeSpan timeSpan = new TimeSpan();
        //int iTemppingHours = 0;       09.21 KimIkHwan 수정
        //int iTemppingMin = 0;
        //int iTemppingSec = 0;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="dat"></param>
        void WorkEvent(clsWork.Category category, byte[] dat)
        {
            switch (category)
            {
                case clsWork.Category.SbReset:
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        if (dat[0] == 1)
                        {   
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "SENSOR BOARD ACK";
                        }
                        else
                        {
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "SENSOR BOARD NACK";
                        }
                    }));
                    break;

                case clsWork.Category.BattAdc:
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        string tmp = string.Format("{0}", dat[0]);
                        lbBattLevel.Text = tmp.Insert(1, ".");
                    }));
                    break;

                case clsWork.Category.GetConfig:
                    #region MyRegion
                    strGetConfigData = System.Text.Encoding.Default.GetString(dat);

                    this.Invoke(new MethodInvoker(delegate()
                    {

                    string a = null; string b = null; string c = null; string d = null;
                    int mode = 0;

                    strGetConfigData = strGetConfigData.Replace("\0", null);


                    string[] buff = strGetConfigData.Split('/');
                    int[] iLenth = new int[buff.Length];

                        //string[] config = new string[] { "S:", "M:", "B:", "RC:", "EC:", "SN:", "PN:", "TP:", "T:", "IV:", "V:", "R:", "DR:", "CL:", "DL:", "CN:", "ST:" };

                        for (int i = 0; i < buff.Length; i++)
                        {
                            int last = buff[i].LastIndexOf(':');

                            if (!string.IsNullOrEmpty(buff[i]))
                            {
                                if (buff[i].Contains("S:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                                    if (tmp == "00")
                                    {
                                        //iStatusFlag = iReady;
                                        lbDebugMsg.Text = "스테이션 동작 00.";

                                    }
                                    if (tmp == "01")
                                    {
                                        //iStatusFlag = iGettering;
                                        lbDebugMsg.Text = "스테이션 동작 01.";
                                    }
                                    if (tmp == "02")
                                    {
                                        lbDebugMsg.Text = "스테이션 동작 02.";
                                    }
                                    mode = int.Parse(tmp);
                                }

                                if (buff[i].Contains("M:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                                    if (tmp == "00")
                                    {
                                        lbConfigMode.Text = "Nomal";
                                    }
                                    if (tmp == "01")
                                    {
                                        lbConfigMode.Text = "Delay";
                                    }
                                    if (tmp == "02")
                                    {
                                        lbConfigMode.Text = "Temp Limit";
                                    }
                                }

                                if (buff[i].Contains("EC:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                                    lbConfigError.Text = tmp;
                                }

                                if (bUserConfig == false)
                                {
                                    if (buff[i].Contains("TP:"))
                                    {
                                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                                        work.iTotalPage = int.Parse(tmp) + 1;
                                        if(lbConfigTotalpage.Text == "0" || lbConfigTotalpage.Text == "0000, 0000")
                                        {
                                            lbConfigTotalpage.Text = (int.Parse(tmp)).ToString();
                                        }
                                        else
                                        {
                                            lbConfigTotalpage.Text = (int.Parse(tmp) + 1).ToString();
                                        }
                                    }

                                    if (buff[i].Contains("PN:"))
                                    {
                                        lbConfigPagenumber.Text = string.Format("{0}", buff[i].Substring(last + 1));
                                    }
                                }
                                
                                if (buff[i].Contains("IV:"))
                                {
                                    lbConfigCycle.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                                    c = string.Format("{0}", buff[i].Substring(last + 1));
                                }

                                if (buff[i].Contains("DL:"))
                                {
                                    lbConfigDelaytime.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                                    a = string.Format("{0}", buff[i].Substring(last + 1));
                                }

                                if (buff[i].Contains("CN:"))
                                {
                                    lbConfigLimitcount.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                                    d = string.Format("{0}", buff[i].Substring(last + 1));
                                }

                                if (buff[i].Contains("ST:"))
                                {
                                    lbConfigSetTemperature.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " ℃";
                                    b = string.Format("{0}", buff[i].Substring(last + 1));
                                }

                                if (buff[i].Contains("ID:"))
                                {
                                    txtLog0.Text = string.Format("{0}", buff[i].Substring(last + 1));
                                    txtLog0.ReadOnly = true;
                                }
                                if (buff[i].Contains("B:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                                    lbBattLevel.Text = tmp.Insert(1, ".");
                                }
                                if (buff[i].Contains("UT:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));

                                    int num = int.Parse(tmp);
                                    iRemainSec = num;

                                    int hour = num / 3600;
                                    int min = num % 3600 / 60;
                                    int sec = num % 3600 % 60;

                                    lbRemainTime.Text = string.Format("누적 사용 시간 : {0} 시간 {1} 분 {2} 초", hour, min, sec);

                                    if (iRemainSec <= 0)
                                    {
                                        if (_frAlarm == null || _frAlarm.IsDisposed)
                                        {
                                            _frAlarm = new frAlarm(1, "[SOW] 남은 사용시간이 없습니다.", swAlarmTime, ptAlarmLos);
                                            _frAlarm.Show();
                                        }
                                        else
                                        {
                                            _frAlarm.Focus();
                                        }
                                    }
                                }
                                if (buff[i].Contains("MT:"))
                                {
                                    string tmp = string.Format("{0}", buff[i].Substring(last + 1));

                                    int num = int.Parse(tmp);
                                    iTotalSec = num;

                                    num = iTotalSec - iRemainSec;

                                    int hour = num / 3600;
                                    int min = num % 3600 / 60;
                                    int sec = num % 3600 % 60;

                                    lbTotalUseTime.Text = string.Format("남은 사용 시간 : {0} 시간 {1} 분 {2} 초", hour, min, sec);
                                }

                                if (_frConfigUser != null)
                                {
                                    _frConfigUser.Configdata_Update();
                                }
                            }
                        }

                        }));
                    #endregion
                    break;

                case clsWork.Category.SetConfig:
                    this.Invoke(new MethodInvoker(delegate()
                    {   
                        lbDebugMsg.Text = "설정 값이 변경 되었습니다.";
                    }));
                    break;

                case clsWork.Category.SetTime:
                    #region MyRegion
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        int[] datetime = new int[dat.Length];

                        for (int i = 0; i < datetime.Length; i++)
                        {
                            datetime[i] = clsHextoBcd.Bcd2Hex((int)dat[i]);
                        }
                        try
                        {
                            DateTime dt = new DateTime((int)datetime[0], (int)datetime[1], (int)datetime[2], (int)datetime[3], (int)datetime[4], (int)datetime[5]);
                            //lbTempStartTime.Text = string.Format("{0:00}시{1:00}분{2:00}초", dt.Hour, dt.Minute, dt.Second);

                            txtLog4.Text = string.Format("{0}", dt.ToString("MM/dd/yy HH:mm:ss tt", CultureInfo.CreateSpecificCulture("en-US")));
                            txtLog4.ReadOnly = true;

                            strStartAcquisition = string.Format("{0}", dt.ToString("HH시 mm분 ss초", CultureInfo.CreateSpecificCulture("en-US")));

                            //if (bOnlyReadData == true)        09.21 KimIkHwan 수정
                            //{
                            //2018.09.20 jace ?? Debug fill

                            swGetteringTime.Reset();
                            swGetteringTime.Start();
                            //if (lbTemppingTime.Text == "0시간 00분 00초")
                            //{
                                if(bOnlyReadData == true)
                                {
                                    lbTempStartTime.Text = strStartAcquisition;
                                    string CurrentTime = DateTime.Now.ToString("HH시 mm분 ss초");
                                    timeSpan = DateTime.Parse(CurrentTime) - DateTime.Parse(strStartAcquisition);
                                }
                            //}
                            //}
                            work.Get_TotalPage();
                        }
                        catch
                        {
                            txtLog4.Text = "ERROR";
                            txtLog4.ReadOnly = true;

                            _frAlarm = new frAlarm(100, "[Acquisition Date ERROR]", ptAlarmLos);
                            _frAlarm.Show();
                            lbDebugMsg.Text = "읽기 완료.";
                        }
                    }));
                    #endregion
                    break;

                case clsWork.Category.TotalPage:
                    #region MyRegion
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        work.iTotalPage = (int)dat[0] << 8 | (int)dat[1];
                        TotalTimePcDat = work.iTotalPage  * 450;
                        work.iGetPage = 0;
                        work.iReadCount = 0;
                        work.Get_TempData(1);
                        swReadTimeOut.Restart();
                        swReadTime.Restart();
                    }));
                    #endregion
                    break;

                case clsWork.Category.TempData:
                    #region MyRegion
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        int ModelCount = 0;

                        switch(SelectdModel.Depatcher)
                        {
                            case "73":
                                ModelCount = 147;
                                break;
                            case "103":
                                ModelCount = 207;
                                break;
                        }

                        int iDatCnt = 0;
                        int cnt = dat.Length / (ModelCount);

                        if (cnt == 0 && dat[0] == 2)
                        {
                            lbDebugMsg.Text = "Temp data recived error";
                            iStatusFlag = iReady;
                            swReadTimeOut.Stop();
                            swReadTimeOut.Reset();
                            return;
                        }

                        int LastPageCheker = work.iTotalPage - work.iGetPage;

                        if (LastPageCheker < 4)
                        {
                            cnt = LastPageCheker;
                        }

                        int[,] iTempData = new int[cnt, int.Parse(SelectdModel.Depatcher)];
                        for (int i = 0; i < cnt; i++)
                        {
                            iDatCnt = i * ModelCount;
                            object[] obj = new object[int.Parse(SelectdModel.Depatcher) + 1];
                            obj[0] = ++iDataHeadCount;

                            for (int j = 0; j < int.Parse(SelectdModel.Depatcher); j++)
                            {
                                if (SelectdModel.Depatcher == "73")
                                {
                                    iTempData[i, j] = (int)dat[iDatCnt + j * 2 + 1] << 8 | (int)dat[iDatCnt + j * 2 + 2];//온도 값 2바이트 결합
                                    iTempData[i, j] = iTempData[i, j] >> 3;//온도 값 인트형 3비트 쉬프트
                                    double dVal = iTempData[i, j] * 0.0625;//0.0625 온도 변환 값 상수
                                    //obj[j] = ((dVal%10000)/10).ToString("00.000");
                                    obj[j + 1] = dVal.ToString("000.000");
                                }
                                else if (SelectdModel.Depatcher == "103")
                                {
                                    int UpperByte = 0x00;
                                    int LowerByte = 0x00;

                                    UpperByte = (int)dat[iDatCnt + j * 2 + 1];
                                    LowerByte = (int)dat[iDatCnt + j * 2 + 2];

                                    UpperByte = UpperByte & 0x1F;
                                    double Temperature = -100;
                                    if ((UpperByte & 0x10) == 0x10)  //Temperature < 0도 보다 작으면
                                    {
                                        UpperByte = UpperByte & 0x0F;
                                        Temperature = 256 - (UpperByte * 16 + LowerByte / 16);
                                    }
                                    else
                                    {
                                        Temperature = ((UpperByte * 16) + ((double) LowerByte) / 16);//0도 보다 크거나 같으면
                                    }
                                    obj[j + 1] = Temperature.ToString("000.000");
                                }
                                else
                                {
                                    //Error
                                }
                            }
                           

                            //73번 센서 데이터와 1번 센서 데이터 위치를 바꾼다. 로케이션 정보를 맞추기 위함.
                            object obj73 = obj[int.Parse(SelectdModel.Depatcher)];
                            object obj1 = obj[1];
                            obj[1] = obj73;
                            obj[int.Parse(SelectdModel.Depatcher)] = obj1;

                            dgvData.Rows.Add(obj);
                        }

                        //Buffer Clear
                        work.Reset();


                        work.iGetPage = work.iGetPage + cnt;
                        if (work.iTotalPage != work.iGetPage && work.iGetPage < work.iTotalPage)
                        {
                            Thread.Sleep(30);
                            work.Get_TempData(work.iGetPage + 1);
                            swReadTimeOut.Restart();
                        }
                        else
                        {
                            //var
                            Cal_Data();

                            swReadTimeOut.Stop();
                            swReadTimeOut.Reset();
                            swReadTime.Stop();
                            iDataHeadCount = 0;
                            iStatusFlag = iReady;
                        }
                    }));
                    #endregion
                    break;

                case clsWork.Category.BatCharging:
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        iStatusFlag = iBatChaging;
                        
                       
                        
                        int Total = (int)dat[1] << 8 | (int)dat[2];
                        int Remain = (int)dat[3] << 8 | (int)dat[4];

                        int num = Remain;
                        iRemainSec = num;

                        int hour = num / 3600;
                        int min = num % 3600 / 60;
                        int sec = num % 3600 % 60;

                        lbBattTime.Text = string.Format("{0}시간 {1}분 {2}초", hour, min, sec);

                        string tmp = string.Format("{0}", dat[5]);
                        lbBattLevel.Text = tmp.Insert(1, ".");

                        //if (Remain == 0)
                        //{
                        //    iStatusFlag = iReady;
                        //    work.Get_Status();
                        //}

                        if (dat[0] == 2)
                        {
                            //bGetBattStatus = false;

                            iStatusFlag = iReady;
                            work.Get_Status();
                        }

                    }));
                    break;

                case clsWork.Category.Battfull:
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        //lbBattLevel.Text = "";
                        pBarVoltage.Value = 100;
                        lbDebugMsg.Text = "충전이 완료 되었습니다.";
                    }));
                    break;

                case clsWork.Category.Start:
                    #region MyRegion
		            this.Invoke(new MethodInvoker(delegate()
                    {
                        if (dat[0] == 1)
                        {   
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "START ACK";
                            bTempGetStart = false;
                        }
                        else
                        {
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "START NACK";
                        }
                    })); 
	#endregion
                    break;

                case clsWork.Category.Stop:
                    #region MyRegion
		            this.Invoke(new MethodInvoker(delegate()
                    {
                        if (dat[0] == 1)
                        {   
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "STOP ACK";
                        }
                        else
                        {
                            iStatusFlag = iReady;
                            lbDebugMsg.Text = "STOP NACK";
                        }
                    })); 
	#endregion
                    break;

                case clsWork.Category.BattStart:
                    lbDebugMsg.Text = "충전이 시작 되었습니다.";
                    break;

                case clsWork.Category.Nfc:
                    #region MyRegion
		            this.Invoke(new MethodInvoker(delegate()
                    {
                        if (dat[0] == 1)
                        {
                            btnNfcState.BackColor = Color.Lime;
                        }
                        else
                        {
                            btnNfcState.BackColor = Color.Red;
                        }
                    })); 
	#endregion
                    break;

                case clsWork.Category.Sensing:
                    #region MyRegion
		            this.Invoke(new MethodInvoker(delegate()
                    {
                        try
                        {
                            int tp = (int)clsStation.Serial.DAT[1] << 8 | (int)clsStation.Serial.DAT[2];
                            int cp = (int)clsStation.Serial.DAT[3] << 8 | (int)clsStation.Serial.DAT[4] -1;

                            if(cp <= 0)
                            {
                                cp = 0;
                            }
                            lbConfigTotalpage.Text = string.Format("{0}", tp);
                            lbConfigPagenumber.Text = string.Format("{0}, {1}", cp, tp);

                            if (iStatusFlag == iDatSending)
                            {
                                if (tp != 0)
                                {
                                    if (cp != 0)
                                    {
                                        pBarSend.Value = (cp+1) * 100 / tp;
                                    }
                                    lbSendRate.Text = pBarSend.Value.ToString() + "%";
                                }
                            }

                            lbConfigTotalpage.Text = (tp).ToString();
                            lbConfigPagenumber.Text = cp.ToString();

                            if (dat[0] == 2)
                            {
                                iStatusFlag = iReady;
                                dtGetEndTime = DateTime.Now;
                                swGetteringTime.Stop();

                                //End Data
                                dtGetEndTime = DateTime.Now;
                                //swSowToRecvRemainTime
                                work.iTotalPage = tp;
                                swSowToRecvRemainTime.Stop();
                                swSowToRecvRemainTime.Reset();

                                work.Get_Status(); //한번만 업데이트용 이다.
                            }
                            else
                            {
                                lbConfigTotalpage.Text = (tp).ToString();
                                lbConfigPagenumber.Text = cp.ToString();

                                //TotalTime = Math.Abs((int)(18.4375 * (tp - cp) * (tp - cp) - 600 * (tp - cp)));
                                TotalTime = ((tp - cp) * 1100 + 600);


                                //double dept = 1100;

                                //for (int i = 0; i < tp; i++)
                                //{
                                //    dept = (1100 * 0.00009) + 1;
                                //}

                                //if (tp < 101)
                                //{
                                //    TotalTime = (tp - cp) * 1100;
                                //}
                                //else if (tp < 201)
                                //{
                                //    TotalTime = (tp - cp) * 1200;
                                //}
                                //else if (tp < 300)
                                //{
                                //    TotalTime = (tp - cp) * 1300;
                                //}
                                //else if (tp < 400)
                                //{
                                //    TotalTime = (tp - cp) * 1400;
                                //}
                                //else if (tp < 500)
                                //{
                                //    TotalTime = (tp - cp) * 1500;
                                //}
                                //else if (tp < 600)
                                //{
                                //    TotalTime = (tp - cp) * 1600;
                                //}
                                //else if (tp < 700)
                                //{
                                //    TotalTime = (tp - cp) * 1700;
                                //}
                                //else if (tp < 800)
                                //{
                                //    TotalTime = (tp - cp) * 1800;
                                //}
                                //else if (tp <= 900)
                                //{
                                //    TotalTime = (tp - cp) * 2000;
                                //}
                                //else if (tp <= 1000)
                                //{
                                //    TotalTime = (tp - cp) * 2500;
                                //}

                                iStatusFlag = iDatSending;

                             

                                //if (!swSowToRecvRemainTime.IsRunning)
                                //{
                                //    int sec = swSowToRecvRemainTime.Elapsed.Seconds;
                                //    double millisec = swSowToRecvRemainTime.Elapsed.Milliseconds;
                                //    lbSendTime.Text = string.Format("{0}.{1}", sec, millisec);
                                //}
                            }

                            lbDebugMsg.Text = "ST-Station  " + dat[0].ToString();
                        }
                        catch
                        { }
                    })); 
	#endregion
                    break;

                case clsWork.Category.Debug:
                    #region MyRegion
		            this.Invoke(new MethodInvoker(delegate()
                    {
                        string tmp = null;
                        for (int i = 0; i < dat.Length; i++)
                        {
                            tmp += dat[i].ToString("X2") + " ";
                        }
                        lbDebugMsg.Text = tmp;
                    })); 
	#endregion
                    break;

                case clsWork.Category.Status:
                    #region MyRegion
                    this.Invoke(new MethodInvoker(delegate()
                    {   
                        string tmp = null;
		                string log = null;

                        try
                        {
                            //log = string.Format("TEMP_IN: [{0}] TEMP_OUT: [{1}] STS: [{2}] NFC: [{3}] BAT: [{4}] BATS: [{5}]", dat[0], dat[1], dat[2], dat[3], dat[4], dat[5]);
                            log = string.Format("TEMP_IN: [{0}] TEMP_OUT: [{1}] STS: [{2}] NFC: [{3}] BAT: [{4}] BATS: [{5}] SOW: [{6}] ABORT: [{7}]", dat[0], dat[1], dat[2], dat[3], dat[4], dat[5], dat[6], dat[7]);
                        }
                        catch
                        {
                            Console.WriteLine("DATA INDEX ERROR");
                        }
                        //Console.WriteLine(log);
                        
                        for (int i = 0; i < dat.Length; i++)
                        {
                            tmp += dat[i].ToString("X2") + " ";
                            iStatus[i] = (int)dat[i];
                        }
                        lbDebugMsg.Text = log;

                        #region RF(NFC) , BATT Status Update //4
                        if (dat[3] == 1) //NFC
                        {
                            btnNfcState.BackColor = Color.Lime;
                        }
                        else
                        {
                            btnNfcState.BackColor = Color.Red;
                        }

                        switch (dat[5])//Bat State
                        {
                            case 0:
                                lbBattState.Text = "초기상태";
                                break;

                            case 1:
                                lbBattState.Text = "매우좋음";
                                lbTempGetTime.Text = "0시간 15분 00초";
                                iTempcolPossibleTime = 15;
                                break;

                            case 2:
                                lbBattState.Text = "  좋음  ";
                                lbTempGetTime.Text = "0시간 15분 00초";
                                iTempcolPossibleTime = 15;
                                break;

                            case 3:
                                lbBattState.Text = "  보통  ";
                                lbTempGetTime.Text = "0시간 10분 00초";
                                iTempcolPossibleTime = 10;
                                break;

                            case 4:
                                lbBattState.Text = "  나쁨  ";
                                lbTempGetTime.Text = "0시간 05분 00초";
                                iTempcolPossibleTime = 5;
                                break;

                            case 5:
                                lbBattState.Text = "매우나쁨";
                                lbTempGetTime.Text = "0시간 05분 00초";
                                iTempcolPossibleTime = 5;
                                break;
                        } 
                        #endregion

                    })); 
	#endregion
                    break;

                case clsWork.Category.Error:
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        lbDebugMsg.Text = "ERROR" + dat[0].ToString("X2");

                    }));
                    break;
                //2018.10.03 jace 
                case clsWork.Category.Interval:
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        if (dat[0] == 1)
                        {
                            lbDebugMsg.Text = "Interval : " + dat[0].ToString("X2") + "정상 처리 되었습니다.";
                        }
                        else if (dat[0] == 2)
                        {
                            lbDebugMsg.Text = "Interval : " + dat[0].ToString("X2") + "비정상 처리 되었습니다.";
                        }
                        else
                        {
                            lbDebugMsg.Text = "Interval : " + dat[0].ToString("X2") + "스테이션 확인 바랍니다..";
                        }
                        

                    }));
                    break;
                case clsWork.Category.CountLimit:
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        if (dat[0] == 1)
                        {
                            lbDebugMsg.Text = "CountLimit : " + dat[0].ToString("X2") + "정상 처리 되었습니다.";
                        }
                        else if (dat[0] == 2)
                        {
                            lbDebugMsg.Text = "CountLimit : " + dat[0].ToString("X2") + "비정상 처리 되었습니다.";
                        }
                        else
                        {
                            lbDebugMsg.Text = "CountLimit : " + dat[0].ToString("X2") + "스테이션 확인 바랍니다..";
                        }


                    }));
                    break;
                case clsWork.Category.Mode:
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        if (dat[0] == 1)
                        {
                            lbDebugMsg.Text = "Mode : " + dat[0].ToString("X2") + "정상 처리 되었습니다.";
                        }
                        else if (dat[0] == 2)
                        {
                            lbDebugMsg.Text = "Mode : " + dat[0].ToString("X2") + "비정상 처리 되었습니다.";
                        }
                        else
                        {
                            lbDebugMsg.Text = "Mode : " + dat[0].ToString("X2") + "스테이션 확인 바랍니다..";
                        }
                    }));
                    break;

                    //2018.10.04 Kim
                case clsWork.Category.RemainTimeReset:
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        if (dat[0] == 1)
                        {
                            lbDebugMsg.Text = "RemainTimeReset : " + dat[0].ToString("X2") + "정상 처리 되었습니다.";
                        }
                        else if (dat[0] == 2)
                        {
                            lbDebugMsg.Text = "RemainTimeReset : " + dat[0].ToString("X2") + "비정상 처리 되었습니다.";
                        }
                        else
                        {
                            lbDebugMsg.Text = "RemainTimeReset : " + dat[0].ToString("X2") + "스테이션 확인 바랍니다..";
                        }
                    }));
                    break;

                default:
                    #region MyRegion
                    this.Invoke(new MethodInvoker(delegate()
                    {

                    }));
                    #endregion
                    break;
            }
        }       
        #endregion   

        #region Func Excel Save
        string[] strExcelData = new string[] { };
        Thread thSaveWorker;

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(txtLog5.Text))
            //{
            //    MessageBox.Show("Please, File Name Key in...");
            //    return;
            //}

            if (thSaveWorker != null && thSaveWorker.IsAlive)
            {
                MessageBox.Show("Please, Excel Data Saving...");
                return;
            }

            if (dgvData.RowCount <= 1)
            {
                MessageBox.Show("Please, Data is empty...");
                return;
            }

            //clsINIFile.NewMakeFolder(Application.StartupPath + "\\" + "TempSensorLog" + "\\");
            SaveFileDialog SaveModeldlg = new SaveFileDialog();
            string strDrectoryPath = Application.StartupPath + "\\" + "TempSensorLog" + "\\";
            if (!Directory.Exists(strDrectoryPath))
            {
                //Directory 없는 경우 생성후 Open Directory 경로 설정
                Directory.CreateDirectory(strDrectoryPath);
                SaveModeldlg.InitialDirectory = strDrectoryPath;
            }
            else
            {
                //Directory 있는 경우 Open Directory 경로 설정
                SaveModeldlg.InitialDirectory = strDrectoryPath;
            }

            SaveModeldlg.Filter = "*.csv|*.csv| All-File *.* | *.*";
            if (SaveModeldlg.ShowDialog() == DialogResult.OK)
            {
                iStatusFlag = iDatConverting;
                string log = null;
                string comma = null;
                strExcelData = new string[16 + dgvData.Rows.Count];
                int cnt = 0;

                for (int i = 0; i < int.Parse(SelectdModel.Depatcher); i++)
                {
                    comma += ",";
                }

                log = string.Format("{0},{1}{2}", lbLog0.Text, txtLog0.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog1.Text, txtLog1.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog2.Text, txtLog2.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog3.Text, txtLog3.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog4.Text, txtLog4.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog5.Text, txtLog5.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog6.Text, txtLog6.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog7.Text, txtLog7.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog8.Text, txtLog8.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog9.Text, txtLog9.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog10.Text, txtLog10.Text, comma);
                strExcelData[cnt++] = log;

                log = string.Format("{0},{1}{2}", lbLog11.Text, txtLog11.Text, comma);
                strExcelData[cnt++] = log;

                
                strExcelData[cnt++] = comma;

                log = "Label,";

                for (int i = 0; i < int.Parse(SelectdModel.Depatcher); i++)
                {
                    log += "S" + i.ToString() + ",";
                }
                strExcelData[cnt++] = log;

                log = "X(mm),";
                for (int i = 0; i < int.Parse(SelectdModel.Depatcher); i++)
                {
                    log += Offset.Data[Offset.x, i] + ",";
                }
                strExcelData[cnt++] = log;


                log = "Y(mm),";
                for (int i = 0; i < int.Parse(SelectdModel.Depatcher); i++)
                {
                    log += Offset.Data[Offset.y, i] + ",";
                }
                
                strExcelData[cnt++] = log;
                strExcelData[cnt++] = "Data,";

                for (int i = 0; i < dRawData.GetLength(0); i++)
                {
                    log = null;
                    log += (i + 1).ToString();
                    for (int j = 0; j < dRawData.GetLength(1); j++)
                    {   
                        log += "," + dRawData[i,j].ToString("000.000");
                    }
                    strExcelData[cnt++] = log;
                }

                //swDataConvertTime.Restart();

                //thSaveWorker = new Thread(new ThreadStart(SaveWork)) { IsBackground = true };
                //thSaveWorker.Start();

                string strFileName = SaveModeldlg.FileName;
                strPath = strFileName.Substring(0,strFileName.LastIndexOf('\\') + 1); ;
                string strTemp = strFileName.Substring(strFileName.LastIndexOf('\\') + 1);
                strFileNamed = strFileName = strTemp;
                txtLog5.Text = strFileName;

                SaveWork();
            }
        }

        string strFileNamed = null;
        string strPath = null;
        void SaveWork()
        {
            bool isSaved = false;

            //EXCEL NOT USE

            //if (excel.Save(strExcelData, Application.StartupPath + "\\TempSensorLog\\", strFileNamed))
            //{
            //    //MessageBox.Show("Save Success!!!", "Save_Check!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    isSaved = true;
            //}
            //else
            //{
            //    clsLog.SowLog(strExcelData, Application.StartupPath + "\\TempSensorLog\\", strFileNamed);
            //    //MessageBox.Show("Excel Version and License Check!!!", "CSV File Save_Check!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

            clsLog.SowLog(strExcelData, strPath, strFileNamed);

            if (chkCalc.Checked)
            {
                string offsetlog = null;
                for (int i = 0; i < dCalData.GetLength(0); i++)
                {
                    offsetlog = null;
                    offsetlog += (i + 1).ToString(); 
                    for (int u = 0; u < dCalData.GetLength(1); u++)
                    {
                        offsetlog += "," + dCalData[i, u].ToString("000.000");
                    }
                    strExcelData[17 + i] = offsetlog;
                }
                strFileNamed = strFileNamed.Insert(strFileNamed.LastIndexOf('.'), "-calc");
                clsLog.SowLog(strExcelData, strPath, strFileNamed);

                //EXCEL NOT USE
                //if (excel.Save(strExcelData, Application.StartupPath + "\\TempSensorLog\\", strFileNamed))
                //{
                //    //MessageBox.Show("Save Success!!!", "Save_Check!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
                //else
                //{
                //    clsLog.SowLog(strExcelData, Application.StartupPath + "\\TempSensorLog\\", strFileNamed);
                //    //MessageBox.Show("Excel Version and License Check!!!", "CSV File Save_Check!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //}
            }
            //if (isSaved)
            //{
            //    MessageBox.Show("Save Success!!!", "Save_Check!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            if (_frAlarm == null || _frAlarm.IsDisposed)
            {
                if (chkCalc.Checked)
                {
                    _frAlarm = new frAlarm(100, "[PC] 온도 데이터 저장 완료 되었습니다.(2개) ", ptAlarmLos);
                }
                else
                {
                    _frAlarm = new frAlarm(100, "[PC] 온도 데이터 저장 완료 되었습니다.(1개)", ptAlarmLos);
                }

                _frAlarm.Show();
            }
            else
            {
                //_frAlarm.Focus();
            }

            //swDataConvertTime.Stop();
            iStatusFlag = iReady;
            //thSaveWorker.Abort();
        }
        #endregion

        public void ConfigDataUpdate()
        {
            if (strGetConfigData == null)
                return;


            if (_frConfigDev != null)
            {
                _frConfigDev.Status(strGetConfigData);
            }

            string a = null; string b = null; string c = null; string d = null;
            int mode = 0;

            strGetConfigData = strGetConfigData.Replace("\0", null);


            string[] buff = strGetConfigData.Split('/');
            int[] iLenth = new int[buff.Length];

            //string[] config = new string[] { "S:", "M:", "B:", "RC:", "EC:", "SN:", "PN:", "TP:", "T:", "IV:", "V:", "R:", "DR:", "CL:", "DL:", "CN:", "ST:" };

            for (int i = 0; i < buff.Length; i++)
            {
                int last = buff[i].LastIndexOf(':');

                if (!string.IsNullOrEmpty(buff[i]))
                {
                    //if (buff[i].Contains("S:"))
                    //{
                    //    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //    if (tmp == "00")
                    //    {
                    //        //iStatusFlag = iReady;
                    //        lbDebugMsg.Text = "스테이션 동작 00.";

                    //    }
                    //    if (tmp == "01")
                    //    {
                    //        //iStatusFlag = iGettering;
                    //        lbDebugMsg.Text = "스테이션 동작 01.";
                    //    }
                    //    if (tmp == "02")
                    //    {
                    //        lbDebugMsg.Text = "스테이션 동작 02.";
                    //    }
                    //    mode = int.Parse(tmp);
                    //}

                    if (buff[i].Contains("M:"))
                    {
                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                        if (tmp == "00")
                        {
                            lbConfigMode.Text = "Nomal";
                        }
                        if (tmp == "01")
                        {
                            lbConfigMode.Text = "Delay";
                        }
                        if (tmp == "02")
                        {
                            lbConfigMode.Text = "Temp Limit";
                        }
                    }

                    if (buff[i].Contains("EC:"))
                    {
                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                        lbConfigError.Text = tmp;
                    }

                    //if (buff[i].Contains("TP:"))
                    //{
                    //    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //    lbConfigTotalpage.Text = (int.Parse(tmp)).ToString();
                    //    //work.iTotalPage = int.Parse(tmp) + 1;
                    //}

                    //if (buff[i].Contains("PN:"))
                    //{
                    //    lbConfigPagenumber.Text = string.Format("{0}", buff[i].Substring(last + 1));
                    //}

                    if (buff[i].Contains("IV:"))
                    {
                        lbConfigCycle.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        c = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("DL:"))
                    {
                        lbConfigDelaytime.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        a = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("CN:"))
                    {
                        lbConfigLimitcount.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " Sec";
                        d = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("ST:"))
                    {
                        lbConfigSetTemperature.Text = string.Format("{0}", buff[i].Substring(last + 1)) + " ℃";
                        b = string.Format("{0}", buff[i].Substring(last + 1));
                    }

                    if (buff[i].Contains("ID:"))
                    {
                        txtLog0.Text = string.Format("{0}", buff[i].Substring(last + 1));
                        txtLog0.ReadOnly = true;
                    }
                    //if (buff[i].Contains("B:"))
                    //{
                    //    string tmp = string.Format("{0}", buff[i].Substring(last + 1));
                    //    lbBattLevel.Text = tmp.Insert(1, ".");
                    //}
                    if (buff[i].Contains("UT:"))
                    {
                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));

                        int num = int.Parse(tmp);
                        iRemainSec = num;

                        int hour = num / 3600;
                        int min = num % 3600 / 60;
                        int sec = num % 3600 % 60;

                        lbRemainTime.Text = string.Format("누적 사용 시간 : {0} 시간 {1} 분 {2} 초", hour, min, sec);

                        if (iRemainSec <= 0)
                        {
                            if (_frAlarm == null || _frAlarm.IsDisposed)
                            {
                                _frAlarm = new frAlarm(1, "[SOW] 남은 사용시간이 없습니다.", swAlarmTime, ptAlarmLos);
                                _frAlarm.Show();
                            }
                            else
                            {
                                _frAlarm.Focus();
                            }
                        }
                    }
                    if (buff[i].Contains("MT:"))
                    {
                        string tmp = string.Format("{0}", buff[i].Substring(last + 1));

                        int num = int.Parse(tmp);
                        iTotalSec = num;

                        num = iTotalSec - iRemainSec;

                        int hour = num / 3600;
                        int min = num % 3600 / 60;
                        int sec = num % 3600 % 60;

                        lbTotalUseTime.Text = string.Format("남은 사용 시간 : {0} 시간 {1} 분 {2} 초", hour, min, sec);
                    }

                    //if (_frConfigUser != null)
                    //{
                    //    _frConfigUser.Configdata_Update();
                    //}
                }
            }
        }

        #region Button Event
        private void btnGetTemperature_Click(object sender, EventArgs e)
        {
            swStationCheckingTime.Reset();
            swStationCheckingTime.Start();

            btnResetTemperature_Click(null, null);

            bOnlyReadData = true;       //09.21 KimIkHwan 수정

            if (work.Status())
            {
                //bTempGetStart = true;

                bTempGetStart = false;
                string tmp = DateTime.Now.ToString("yy");

                int year = clsHextoBcd.Hex2Bcd(int.Parse(tmp));
                int month = clsHextoBcd.Hex2Bcd(DateTime.Now.Month);
                int day = clsHextoBcd.Hex2Bcd(DateTime.Now.Day);

                int hour = clsHextoBcd.Hex2Bcd(DateTime.Now.Hour);
                int min = clsHextoBcd.Hex2Bcd(DateTime.Now.Minute);
                int sec = clsHextoBcd.Hex2Bcd(DateTime.Now.Second);

                work.Set_Date(year, month, day);
                Thread.Sleep(20);
                work.Set_Time(hour, min, sec);
                Thread.Sleep(20);
                work.Sta_Start();
                //lbTempStartTime.Text = string.Format("{0:00}시{1:00}분{2:00}초", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                //swGetteringTime.Restart();
            }
        }

        private void btnEndTemperature_Click(object sender, EventArgs e)
        {
            if(swStationCheckingTime.IsRunning)
            {
                swStationCheckingTime.Stop();
            }

            if (work.Status())
            {
                //bTempGetEnd = true;
                work.Sta_Stop();
                //work.Get_Status();        09.13 온도수집 종료후 데이터 읽기 우선순위 온도 데이터 다 읽은 후 Station 충전
                //lbTempEndTime.Text = string.Format("{0:00}시{1:00}분{2:00}초", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                //swGetteringTime.Stop();
            }
        }

        private void btnReadTemperature_Click(object sender, EventArgs e)
        {
            bOnlyReadData = false;      //09.21 KimIkHwan 수정

            //if (work.iTotalPage !=0 ||  work.iGetPage == 0)//first
            if (work.iGetPage == 0 || work.iGetPage >= work.iTotalPage)
            {
                work.iTotalPage = 0;
                work.iGetPage = 0;
                iDataHeadCount = 0;
                work.iReadCount = 0;
             
                _InitDataGridview();
                //work.Get_TotalPage();
                work.Get_AcqisitionDate();
            }
            else
            {
                if (work.iGetPage < work.iTotalPage)
                {
                    if (string.IsNullOrEmpty(txtLog4.Text))
                    {
                        work.iTotalPage = 0;
                        work.iGetPage = 0;
                        iDataHeadCount = 0;
                        work.iReadCount = 0;

                        _InitDataGridview();
                        //work.Get_TotalPage();
                        work.Get_AcqisitionDate();
                    }
                    else
                    {
                        work.iReadCount = 0;
                        work.Get_TempData(dgvData.RowCount);
                    }
                    
                }
                else
                {

                }

                
            }

            if (work.Status())
            {
                iStatusFlag = iDatConverting;
            }
        }

        private void btnResetTemperature_Click(object sender, EventArgs e)
        {
            if (work.Status())
            {
                iStatusFlag = iReady;
                work.iGetPage = 0;
                work.iReadCount = 0;
                work.iTotalPage = 0;
                iDataHeadCount = 0;
                bTempGetStart = false;
                //bTempRead = false;
                bTempGetEnd = false;
                //bWaiting = false;
                bOnlyReadData = false;
                swGetteringTime.Stop();
                swGetteringTime.Reset();
                lbTempStartTime.Text = "00시 00분 00초";
                lbTempEndTime.Text = "00시 00분 00초";
                lbTemppingTime.Text = "0시간 00분 00초";
                timeSpan = new TimeSpan();
                pBarDiscard.Value = 0;
                pBarRecevied.Value = 0;
                pBarSend.Value = 0;

                
                work.Reset();
                txtLog4.Text = "";
                _InitDataGridview();


                work.Get_Status();
            }
        }

        private void btnConfigUser_Click_1(object sender, EventArgs e)
        {
            if (_frConfigUser == null || _frConfigUser.IsDisposed)
            {
                _frConfigUser = new frConfigUser(this);
                _frConfigUser.Show();
            }
            else
            {
                _frConfigUser.Focus();
            }
        }

        private void btnOffset_Click(object sender, EventArgs e)
        {
            if (_frOffset == null || _frOffset.IsDisposed)
            {
                _frOffset = new frOffset(this);
                _frOffset.Show();
            }
            else
            {
                _frOffset.Focus();
            }
        }

        private void btnSerialReflash_Click(object sender, EventArgs e)
        {
            int comCount = 0;

            clsRS_232C._FindSerial(ref comCount);
            
            comboBoxSerial.Items.Clear();
            if (comCount == -1)
            {
                
                comboBoxSerial.Text = "None";
            }
            else
            {
                object[] obj = clsRS_232C._FindSerial();
                comboBoxSerial.Items.AddRange(obj);

                comboBoxSerial.SelectedIndex = 0;
            }
        }

        private void btnSerialOpen_Click(object sender, EventArgs e)
        {
            if (work.com.ErrorCount > 5)
            {
                work.com.ErrorCount = 0;

                //work.com.Close();
            }

            if (!string.IsNullOrEmpty(comboBoxSerial.Text))
            {
                if(txtLog4.Text != "")
                {
                    bOnlyReadData = true;
                }
                if (!work.com.Status())
                {
                    //if(mStation.com.

                    work.Open(comboBoxSerial.Text);

                    System.Threading.Thread.Sleep(100);

                    work.Get_Config();
                    work.Get_AcqisitionDate();
                }

                else
                {
                    work.com.Close();
                }
            }
        }

        Stopwatch swRecognition = new Stopwatch();
        Stopwatch swModelFormLoadTime = new Stopwatch();
        //bool bDownClock = false;
        private void lbConfigDev_MouseDown(object sender, MouseEventArgs e)
        {
            //bDownClock = true;
            swRecognition.Start();
        }

        private void lbConfigDev_MouseUp(object sender, MouseEventArgs e)
        {
            //bDownClock = false;
            swRecognition.Reset();
            swRecognition.Stop();
        }


        private void label7_MouseDown(object sender, MouseEventArgs e)
        {
            swModelFormLoadTime.Start();
        }

        private void label7_MouseUp(object sender, MouseEventArgs e)
        {
            swModelFormLoadTime.Reset();
            swModelFormLoadTime.Stop();
        }
        private void chkCalc_CheckedChanged(object sender, EventArgs e)
        {
            if(OffSetWaferID != txtLog0.Text)       //2018.10.06 Kim
            {
                if (_frAlarm == null || _frAlarm.IsDisposed)
                {
                    _frAlarm = new frAlarm(1, "[Wafer ID 확인 부탁드립니다.]", ptAlarmLos);
                    _frAlarm.Show();
                    chkCalc.Checked = false;
                }
            }
            if (dCalData != null)
            {
                object[] obj = new object[74];
                if (chkCalc.Checked)
                {
                    //Cal_Data();

                    Master_Calibaration();


                    dgvData.Rows.Clear();
                    for (int i = 0; i < dRawData.GetLength(0); i++)
                    {
                        obj[0] = i + 1;
                        for (int u = 0; u < dRawData.GetLength(1); u++)
                        {
                            obj[u + 1] = dCalData[i, u].ToString("000.000"); ;
                        }
                        dgvData.Rows.Add(obj);
                    }
                }
                else
                {
                    dgvData.Rows.Clear();

                    for (int i = 0; i < dRawData.GetLength(0); i++)
                    {
                        obj[0] = i + 1;

                        for (int u = 0; u < dRawData.GetLength(1); u++)
                        {
                            obj[u + 1] = dRawData[i, u].ToString("000.000"); ;
                        }
                        dgvData.Rows.Add(obj);
                    }
                }
            }
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            OpenFileDialog openModeldlg = new OpenFileDialog();
            openModeldlg.Filter = "*.csv|*.csv| All-File *.* | *.*";

            if (openModeldlg.ShowDialog() == DialogResult.OK)
            {
                string file = openModeldlg.FileName;

                string[] data = clsLog.ReadLog(file);
                const int iDefault = 18;

                if (!file.Contains("Master"))
                {
                    string[] WaferID = data[0].Split(',');

                    if (WaferID.Length > 3)
                    {
                        txtLog0.Text = WaferID[1].PadRight(5, '0');
                    }
                    else
                    {
                        txtLog0.Text = "000000";
                    }

                    dgvData.Rows.Clear();

                    for (int i = iDefault; i < data.Length; i++)
                    {
                        string[] Buffer = data[i].Split(',');
                        Object[] obj = new object[Buffer.Length];
                        for (int u = 0; u < obj.Length; u++)
                        {
                            obj[u] = Buffer[u];
                        }
                        dgvData.Rows.Add(obj);
                    }

                    //Cal_Data();

                    Master_Calibaration();

                    if (chkCalc.Checked)
                    {
                        dgvData.Rows.Clear();
                        for (int i = 0; i < dCalData.GetLength(0); i++)
                        {
                            Object[] obj = new object[dCalData.GetLength(1) + 1];
                            obj[0] = (i + 1).ToString();

                            for (int u = 1; u < obj.Length; u++)
                            {
                                obj[u] = dCalData[i, u - 1].ToString("0.000");
                            }
                            dgvData.Rows.Add(obj);
                        }
                    }
                }
                else
                {
                    int iDefaultMaster = 3;
                    dgvData.Rows.Clear();

                    for (int i = iDefaultMaster; i < data.Length; i++)
                    {
                        string[] Buffer = data[i].Split(',');
                        if (!data[i].Contains("DATA"))
                        {
                            Object[] obj = new object[Buffer.Length];
                            for (int u = 0; u < obj.Length; u++)
                            {
                                obj[u] = Buffer[u];
                            }
                            dgvData.Rows.Add(obj);
                        }
                        else
                        {
                            //dgvData.Rows.Add(Buffer[1]);
                        }
                        
                    }
                    Master_Calibaration();
                    //Cal_Data();
                }


                
            }
        }


        /// <summary>
        /// 임시 키인 마스터 디폴트 입니다. 계산기
        /// </summary>
        void Master_Calibaration()
        {
            try
            {
                dCalData = new double[dgvData.RowCount - 1, int.Parse(SelectdModel.Depatcher)];
                dRawData = new double[dgvData.RowCount - 1, int.Parse(SelectdModel.Depatcher)];

                //Raw AVG


                double[] dRawAvg = new double[dgvData.RowCount - 1];
                double sum = 0;
                double avg = 0;
                for (int i = 0; i < dgvData.RowCount - 1; i++)
                {

                    for (int u = 0; u < dgvData.ColumnCount - 1; u++)
                    {
                        dRawData[i, u] = double.Parse(dgvData.Rows[i].Cells[u + 1].Value.ToString());
                    }
                }

                for (int i = 0; i < dgvData.RowCount - 1; i++)
                {
                    for (int u = 0; u < dgvData.ColumnCount - 1; u++)
                    {
                        dCalData[i, u] = Cal_Divide(u, dRawData[i, u]);
                    }
                }
            }
            catch
            { }
        }


        /// <summary>
        /// Sensor Pos, Value
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="val"></param>
        double Cal_Divide(int pos, double val)
        {
            double rtn = -100;
            int row = 2;

            if (val >=0 && val <= 10)
            {
                row = 2;
            }
            else if (val <= 20)
            {
                row = 2;
            }
            else if (val <= 30)
            {
                row = 2;
            }
            else if (val <= 40)
            {
                row = 2;
            }
            else if (val <= 50)
            {
                row = 3;
            }
            else if (val <= 60)
            {
                row = 4;
            }
            else if (val <= 70)
            {
                row = 5;
            }
            else if (val <= 80)
            {
                row = 6;
            }
            else if (val <= 90)
            {
                row = 7;
            }
            else if (val <= 100)
            {
                row = 8;
            }
            else if (val <= 110)
            {
                row = 9;
            }
            else if (val <= 120)
            {
                row = 10;
            }
            else if (val <= 130)
            {
                row = 10;
            }
            else if (val <= 140)
            {
                row = 10;
            }
            else//140 이상
            {
                row = 10;
            }
            rtn = val / 0.0625 * double.Parse(Offset.Data[row, pos]);

            return rtn;
        }

        #endregion

    }
}