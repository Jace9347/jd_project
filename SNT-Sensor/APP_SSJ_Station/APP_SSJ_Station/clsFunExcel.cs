﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP_SSJ_Station
{
    using Excel = Microsoft.Office.Interop.Excel;
    using System.Windows.Forms;
    using System.ComponentModel;
    using System.Data.OleDb;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Diagnostics;

    public class clsFunExcel
    {
        #region TEST
        public void Save()
        {
            try
            {
                Excel.Application App = new Excel.Application();

                object misValue = System.Reflection.Missing.Value;
                App.Visible = false;

                Excel.Workbooks Books = App.Workbooks;
                //Book = App.Workbooks.Open(DB_PATH);
                Excel.Workbook Book = App.Workbooks.Add(misValue);
                //Sheet = (Excel.Worksheet)Book.Sheets[1]; // Explict cast is not required here
                Excel.Worksheet Sheet = (Excel.Worksheet)Book.Worksheets.get_Item(1);

                Excel.Range Range = (Excel.Range)Sheet.Cells.get_Item("1", "A");
                //lastRow = Sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                Excel.Range Cells = (Excel.Range)Sheet.Cells;

                //Info
                //X,Y
                //Data
                //lastRow += 1;
                Sheet.Cells[1, 1] = "a";
                Sheet.Cells[2, 2] = "e";
                Sheet.Cells[3, 3] = "c";
                Sheet.Cells[4, 4] = "d";
                //EmpList.Add(emp);
                //Book.Save();


                App.DisplayAlerts = false;

                //Book.SaveAs(Application.StartupPath + "\\tet.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                Book.SaveAs(Application.StartupPath + "\\tet.xlsx", Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);

                //Book.Saved = true;

                //Book.Close(true, misValue, misValue);
                //Book.Close(true, Application.StartupPath + "\\tet.xlsx", null);
                //Book.Close(false);

                //Range = null;
                //cells = null;
                //Sheet = null;
                ////Book = null;
                //Books = null;
                //App = null;


                Book.Close();
                //Book = null;
                App.Quit();



                Marshal.ReleaseComObject(Range);
                Marshal.ReleaseComObject(Cells);

                Marshal.ReleaseComObject(Sheet);
                Marshal.ReleaseComObject(Book);
                Marshal.ReleaseComObject(Books);
                Marshal.ReleaseComObject(App);



                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //Process[] process = Process.GetProcessesByName("EXCEL");
                //foreach (Process p in process)
                //{
                //    if (!string.IsNullOrEmpty(p.ProcessName))
                //    {
                //        try
                //        {
                //            p.Kill();
                //        }
                //        catch { }
                //    }
                //}

            }
            catch (Exception ex)
            {

            }
        } 
        #endregion


        /// <summary>
        /// 엑셀 저장 부분
        /// </summary>
        /// <param name="dat">온도 데이터 배열</param>
        /// <param name="path">저장경로</param>
        /// <param name="file">저장이름</param>
        public bool Save(string[] dat, string path, string file)
        {
            //var
            bool rtn = false;

            try
            {
                Excel.Application App = new Excel.Application();
                object misValue = System.Reflection.Missing.Value;
                App.Visible = false;
                Excel.Workbooks Books = App.Workbooks;
                Excel.Workbook Book = App.Workbooks.Add(misValue);
                Excel.Worksheet Sheet = (Excel.Worksheet)Book.Worksheets.get_Item(1);
                Excel.Range Range = (Excel.Range)Sheet.Cells.get_Item("1", "A");
                Excel.Range Cells = (Excel.Range)Sheet.Cells;
                for (int row = 0; row < dat.Length; row++)
                {
                    string[] buf = dat[row].Split(',');
                    for (int col = 0; col < buf.Length; col++)
                    {
                        Sheet.Cells[row + 1, col + 1] = buf[col];
                    }
                }
                App.DisplayAlerts = false;
                //Book.SaveAs(path + file, Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                Book.SaveAs(path + file, Excel.XlFileFormat.xlCSV, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                Book.Close();
                App.Quit();

                Marshal.ReleaseComObject(Range);
                Marshal.ReleaseComObject(Cells);
                Marshal.ReleaseComObject(Sheet);
                Marshal.ReleaseComObject(Book);
                Marshal.ReleaseComObject(Books);
                Marshal.ReleaseComObject(App);

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                rtn = true;
            }
            catch
            {

            }
            return rtn;
        }
    }
}
