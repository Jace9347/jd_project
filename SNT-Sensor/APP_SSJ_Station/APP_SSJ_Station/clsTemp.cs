﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP_SSJ_Station
{
    class clsTemp
    {
        public double[] Value { get; set; }
        public string[] Info { get; set; }
        public double[] X { get; set; }
        public double[] Y { get; set; }
    }
}
