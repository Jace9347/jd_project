﻿namespace APP_SSJ_Station
{
    partial class frConfigUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTemp = new System.Windows.Forms.TextBox();
            this.txtSendTemp = new System.Windows.Forms.TextBox();
            this.txtSendDelay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDelay = new System.Windows.Forms.TextBox();
            this.rbtnTemp = new System.Windows.Forms.RadioButton();
            this.rbtnDelay = new System.Windows.Forms.RadioButton();
            this.rbtnNormal = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSendCntLimit = new System.Windows.Forms.TextBox();
            this.txtSendInterval = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCntLimit = new System.Windows.Forms.TextBox();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSensorBdReset = new System.Windows.Forms.Button();
            this.btnStationReset = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gbMode.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMode
            // 
            this.gbMode.BackColor = System.Drawing.Color.DarkGray;
            this.gbMode.Controls.Add(this.label10);
            this.gbMode.Controls.Add(this.label9);
            this.gbMode.Controls.Add(this.label7);
            this.gbMode.Controls.Add(this.txtTemp);
            this.gbMode.Controls.Add(this.txtSendTemp);
            this.gbMode.Controls.Add(this.txtSendDelay);
            this.gbMode.Controls.Add(this.label2);
            this.gbMode.Controls.Add(this.label1);
            this.gbMode.Controls.Add(this.txtDelay);
            this.gbMode.Controls.Add(this.rbtnTemp);
            this.gbMode.Controls.Add(this.rbtnDelay);
            this.gbMode.Controls.Add(this.rbtnNormal);
            this.gbMode.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbMode.Location = new System.Drawing.Point(27, 3);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(408, 179);
            this.gbMode.TabIndex = 1;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "Mode";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("돋움", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(171, -29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 32);
            this.label7.TabIndex = 5;
            this.label7.Text = "label7";
            // 
            // txtTemp
            // 
            this.txtTemp.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTemp.Location = new System.Drawing.Point(257, 124);
            this.txtTemp.Name = "txtTemp";
            this.txtTemp.Size = new System.Drawing.Size(70, 29);
            this.txtTemp.TabIndex = 6;
            this.txtTemp.Text = "0";
            this.txtTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSendTemp
            // 
            this.txtSendTemp.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSendTemp.Location = new System.Drawing.Point(161, 124);
            this.txtSendTemp.Name = "txtSendTemp";
            this.txtSendTemp.ReadOnly = true;
            this.txtSendTemp.Size = new System.Drawing.Size(70, 29);
            this.txtSendTemp.TabIndex = 15;
            this.txtSendTemp.Text = "0";
            this.txtSendTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSendDelay
            // 
            this.txtSendDelay.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSendDelay.Location = new System.Drawing.Point(161, 85);
            this.txtSendDelay.Name = "txtSendDelay";
            this.txtSendDelay.ReadOnly = true;
            this.txtSendDelay.Size = new System.Drawing.Size(70, 29);
            this.txtSendDelay.TabIndex = 14;
            this.txtSendDelay.Text = "0";
            this.txtSendDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(348, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "℃";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(334, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "Sec";
            // 
            // txtDelay
            // 
            this.txtDelay.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtDelay.Location = new System.Drawing.Point(257, 85);
            this.txtDelay.Name = "txtDelay";
            this.txtDelay.Size = new System.Drawing.Size(70, 29);
            this.txtDelay.TabIndex = 3;
            this.txtDelay.Text = "0";
            this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // rbtnTemp
            // 
            this.rbtnTemp.AutoSize = true;
            this.rbtnTemp.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtnTemp.Location = new System.Drawing.Point(32, 125);
            this.rbtnTemp.Name = "rbtnTemp";
            this.rbtnTemp.Size = new System.Drawing.Size(114, 23);
            this.rbtnTemp.TabIndex = 2;
            this.rbtnTemp.Text = "Temp Limit";
            this.rbtnTemp.UseVisualStyleBackColor = true;
            // 
            // rbtnDelay
            // 
            this.rbtnDelay.AutoSize = true;
            this.rbtnDelay.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtnDelay.Location = new System.Drawing.Point(32, 86);
            this.rbtnDelay.Name = "rbtnDelay";
            this.rbtnDelay.Size = new System.Drawing.Size(70, 23);
            this.rbtnDelay.TabIndex = 1;
            this.rbtnDelay.Text = "Delay";
            this.rbtnDelay.UseVisualStyleBackColor = true;
            // 
            // rbtnNormal
            // 
            this.rbtnNormal.AutoSize = true;
            this.rbtnNormal.Checked = true;
            this.rbtnNormal.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtnNormal.Location = new System.Drawing.Point(32, 47);
            this.rbtnNormal.Name = "rbtnNormal";
            this.rbtnNormal.Size = new System.Drawing.Size(81, 23);
            this.rbtnNormal.TabIndex = 0;
            this.rbtnNormal.TabStop = true;
            this.rbtnNormal.Text = "Normal";
            this.rbtnNormal.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Controls.Add(this.txtSendCntLimit);
            this.panel1.Controls.Add(this.txtSendInterval);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtCntLimit);
            this.panel1.Controls.Add(this.txtInterval);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.gbMode);
            this.panel1.Location = new System.Drawing.Point(12, 89);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 314);
            this.panel1.TabIndex = 2;
            // 
            // txtSendCntLimit
            // 
            this.txtSendCntLimit.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSendCntLimit.Location = new System.Drawing.Point(188, 259);
            this.txtSendCntLimit.Name = "txtSendCntLimit";
            this.txtSendCntLimit.ReadOnly = true;
            this.txtSendCntLimit.Size = new System.Drawing.Size(70, 29);
            this.txtSendCntLimit.TabIndex = 17;
            this.txtSendCntLimit.Text = "0";
            this.txtSendCntLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSendInterval
            // 
            this.txtSendInterval.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSendInterval.Location = new System.Drawing.Point(188, 206);
            this.txtSendInterval.Name = "txtSendInterval";
            this.txtSendInterval.ReadOnly = true;
            this.txtSendInterval.Size = new System.Drawing.Size(70, 29);
            this.txtSendInterval.TabIndex = 16;
            this.txtSendInterval.Text = "0";
            this.txtSendInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(364, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 19);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sec";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(364, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 19);
            this.label5.TabIndex = 5;
            this.label5.Text = "Sec";
            // 
            // txtCntLimit
            // 
            this.txtCntLimit.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCntLimit.Location = new System.Drawing.Point(284, 259);
            this.txtCntLimit.Name = "txtCntLimit";
            this.txtCntLimit.Size = new System.Drawing.Size(70, 29);
            this.txtCntLimit.TabIndex = 8;
            this.txtCntLimit.Text = "0";
            this.txtCntLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtInterval
            // 
            this.txtInterval.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtInterval.Location = new System.Drawing.Point(284, 206);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(70, 29);
            this.txtInterval.TabIndex = 7;
            this.txtInterval.Text = "1";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(55, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Count Limit :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(55, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Interval :";
            // 
            // btnSend
            // 
            this.btnSend.Font = new System.Drawing.Font("돋움", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSend.Location = new System.Drawing.Point(72, 422);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(132, 36);
            this.btnSend.TabIndex = 3;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("돋움", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnClose.Location = new System.Drawing.Point(284, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(132, 36);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(464, 58);
            this.panel2.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("돋움", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(464, 58);
            this.label8.TabIndex = 0;
            this.label8.Text = "SET UP";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSensorBdReset
            // 
            this.btnSensorBdReset.Font = new System.Drawing.Font("돋움", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSensorBdReset.Location = new System.Drawing.Point(12, 490);
            this.btnSensorBdReset.Name = "btnSensorBdReset";
            this.btnSensorBdReset.Size = new System.Drawing.Size(464, 52);
            this.btnSensorBdReset.TabIndex = 6;
            this.btnSensorBdReset.Text = "Sensor Board Reset";
            this.btnSensorBdReset.UseVisualStyleBackColor = true;
            this.btnSensorBdReset.Click += new System.EventHandler(this.btnSensorBdReset_Click);
            // 
            // btnStationReset
            // 
            this.btnStationReset.Font = new System.Drawing.Font("돋움", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStationReset.Location = new System.Drawing.Point(12, 560);
            this.btnStationReset.Name = "btnStationReset";
            this.btnStationReset.Size = new System.Drawing.Size(464, 52);
            this.btnStationReset.TabIndex = 7;
            this.btnStationReset.Text = "Station Reset";
            this.btnStationReset.UseVisualStyleBackColor = true;
            this.btnStationReset.Click += new System.EventHandler(this.btnStationReset_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(163, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "설정값";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("돋움", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(258, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 19);
            this.label10.TabIndex = 17;
            this.label10.Text = "입력값";
            // 
            // frConfigUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 624);
            this.ControlBox = false;
            this.Controls.Add(this.btnStationReset);
            this.Controls.Add(this.btnSensorBdReset);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.panel1);
            this.Name = "frConfigUser";
            this.Text = "SET UP DISPLAY";
            this.Load += new System.EventHandler(this.frConfigUser_Load);
            this.gbMode.ResumeLayout(false);
            this.gbMode.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbMode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDelay;
        private System.Windows.Forms.RadioButton rbtnTemp;
        private System.Windows.Forms.RadioButton rbtnDelay;
        private System.Windows.Forms.RadioButton rbtnNormal;
        private System.Windows.Forms.TextBox txtTemp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCntLimit;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSensorBdReset;
        private System.Windows.Forms.Button btnStationReset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSendTemp;
        private System.Windows.Forms.TextBox txtSendDelay;
        private System.Windows.Forms.TextBox txtSendCntLimit;
        private System.Windows.Forms.TextBox txtSendInterval;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}