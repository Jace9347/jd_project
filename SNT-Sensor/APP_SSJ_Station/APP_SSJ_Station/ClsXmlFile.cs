﻿namespace JDS_Library_X86
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    public class ClsXmlFile
    {

        /// <summary>
        /// Version
        /// </summary>
        private const string VERSION = "1.0";//Jace 2018.09.17
        private const string ENCODING = "UTF-8";//Jace 2018.09.17


        /// <summary>
        /// XML EDIT
        /// </summary>
        public enum EModifier
        {
            Edit, Delete, Add
        }

        /// <summary>
        /// 파일 경로, 형식 이름, 데이터 이름, 값, XML 형식 쓰기 (멀티)
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="attribute"></param>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public void Write(string strXml, string attribute, string[] element, string[] value)
        {
            try
            {
                try
                {
                    XmlTextWriter textWriter = new XmlTextWriter(strXml, Encoding.UTF8);
                    textWriter.Formatting = Formatting.Indented;
                    //root
                    textWriter.WriteStartDocument();

                    //node
                    textWriter.WriteStartElement(attribute);

                    //sub node
                    int cnt = 0;
                    foreach (string node in element)
                    {
                        textWriter.WriteStartElement(node);
                        textWriter.WriteString(value[cnt++]);
                        textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();
                    textWriter.WriteEndDocument();
                    textWriter.Close();
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            catch(IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        /// <summary>
        /// XML 형식 쓰기 (싱글)
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="attribute"></param>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public void Write(string strXml, string attribute, string element, string value)
        {
            try
            {
                if (File.Exists(strXml))
                {
                    Modifier(strXml, attribute, element, value, EModifier.Add);
                }
                else
                {
                    XmlTextWriter textWriter = new XmlTextWriter(strXml, Encoding.UTF8);
                    textWriter.Formatting = Formatting.Indented;

                    //root
                    textWriter.WriteStartDocument();

                    //node
                    textWriter.WriteStartElement(attribute);

                    //sub node
                    textWriter.WriteStartElement(element);
                    textWriter.WriteString(value);
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();

                    textWriter.WriteEndDocument();

                    textWriter.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        /// <summary>
        /// XML READ Single
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="attribute"></param>
        /// <param name="element"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string Read(string strXml, string attribute, string element)
        {
            string rtn = null;
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(strXml);
                XmlElement root = xmldoc.DocumentElement;
                //if (attribute != root.Name)
                //{
                //    return rtn;
                //}
                XmlNodeList nodes = root.ChildNodes;
                // 노드 요소의 값을 읽어 옵니다.
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList subnode = node.ChildNodes;
                    if (node.Name == attribute)
                    {
                        foreach (XmlNode val in subnode)
                        {
                            if (val.Name == element)
                            {
                                rtn = val.InnerText;
                                break;
                            }
                        }
                    }
                }
                
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return rtn;
        }
        

        public List<List<string>> Read(string strXml)
        {
            var adds = new List<List<string>>();
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(strXml);

                XmlElement root = xmldoc.DocumentElement;
                XmlNodeList xnList = root.ChildNodes; //접근할 노드

                
                foreach (XmlNode xn in xnList)
                {
                    var add = new List<string>();
                    add.Add(xn.Name);
                    XmlNodeList nodeList = xn.ChildNodes;
                    //var addval = new List<string>();
                    foreach (XmlNode subxn in nodeList)
                    {   
                        //addval.Add(subxn.Name);
                        //addval.Add(subxn.InnerText);
                        add.Add(subxn.InnerText);
                    }
                    adds.Add(add);
                }
               
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return adds;
        }

        public void Modifier(string strXml, string attribute, string element, string value, EModifier modifier)
        {
            try
            {
                // XML문서를 불러온다
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.Load(strXml);

                //// 하위 노드 특성에 날짜를 입력하기를 원할때(추가를 원할때)
                XmlDoc.CreateAttribute("DATE", DateTime.Now.ToString());
               

                XmlNode node = XmlDoc.DocumentElement;
                node.SelectSingleNode(element);
                node.RemoveChild(node);

                node.AppendChild(CreateNode(XmlDoc, attribute, element, value));

                
                

                //switch (modifier)
                //{
                //    case EModifier.Add:
                //        // 아래 한줄은 추가, 수정할때 사용하면 된다.
                //        if (string.IsNullOrEmpty(value))
                //        {
                //            value = "NULL";
                //        }
                //        SubNode.AppendChild(CreateNode(XmlDoc, attribute, element, value));
                //        break;
                //    case EModifier.Delete:
                //        // 하위 노드를 추가, 삭제, 수정하고 싶을때(BOOK보다 하위)
                //        // 아래 두줄은 삭제할때나, 수정할때 사용하면 된다.
                //        DeleteNode = SubNode.SelectSingleNode(element);
                //        SubNode.RemoveChild(DeleteNode);
                //        break;
                //    case EModifier.Edit:
                //        DeleteNode = SubNode.SelectSingleNode(element);
                //        SubNode.RemoveChild(DeleteNode);
                //        FristNode.ReplaceChild(SubNode, SubNode);// ReplaceChild(SubNode, SubNode); 에서 () 안에 앞에 노드는 변경할 노드
                //        break;
                //    default:
                //        break;

                //}

                XmlDoc.Save(strXml);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected XmlNode CreateNode(XmlDocument xmlDoc, string attribute, string name, string innerXml)
        {
            //XmlNode node = xmlDoc.CreateElement(string.Empty, name, string.Empty);
            XmlNode node = xmlDoc.CreateElement(string.Empty, name, string.Empty);
            node.InnerXml = innerXml;

            return node;
        }
    }
}
