﻿namespace APP_SSJ_Station
{
    partial class frModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtSelectID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvModelViewer = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtSelectName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSelectDepatcher = new System.Windows.Forms.TextBox();
            this.rbtnTemperature = new System.Windows.Forms.RadioButton();
            this.rbtnPlasma = new System.Windows.Forms.RadioButton();
            this.rbtnHybrid = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelViewer)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(450, 274);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(174, 40);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(450, 330);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(174, 40);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtSelectID
            // 
            this.txtSelectID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSelectID.Location = new System.Drawing.Point(524, 29);
            this.txtSelectID.Name = "txtSelectID";
            this.txtSelectID.Size = new System.Drawing.Size(100, 27);
            this.txtSelectID.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(447, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID";
            // 
            // dgvModelViewer
            // 
            this.dgvModelViewer.AllowUserToResizeRows = false;
            this.dgvModelViewer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvModelViewer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvModelViewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelViewer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvModelViewer.Location = new System.Drawing.Point(12, 12);
            this.dgvModelViewer.MultiSelect = false;
            this.dgvModelViewer.Name = "dgvModelViewer";
            this.dgvModelViewer.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvModelViewer.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvModelViewer.RowHeadersVisible = false;
            this.dgvModelViewer.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvModelViewer.RowTemplate.Height = 23;
            this.dgvModelViewer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvModelViewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelViewer.Size = new System.Drawing.Size(424, 414);
            this.dgvModelViewer.TabIndex = 4;
            this.dgvModelViewer.DoubleClick += new System.EventHandler(this.dgvModelViewer_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Name";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Depatcher";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(450, 386);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(174, 40);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtSelectName
            // 
            this.txtSelectName.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSelectName.Location = new System.Drawing.Point(232, 241);
            this.txtSelectName.Name = "txtSelectName";
            this.txtSelectName.Size = new System.Drawing.Size(100, 27);
            this.txtSelectName.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(447, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Depatcher";
            // 
            // txtSelectDepatcher
            // 
            this.txtSelectDepatcher.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtSelectDepatcher.Location = new System.Drawing.Point(524, 239);
            this.txtSelectDepatcher.Name = "txtSelectDepatcher";
            this.txtSelectDepatcher.Size = new System.Drawing.Size(100, 27);
            this.txtSelectDepatcher.TabIndex = 9;
            // 
            // rbtnTemperature
            // 
            this.rbtnTemperature.AutoSize = true;
            this.rbtnTemperature.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTemperature.Location = new System.Drawing.Point(10, 27);
            this.rbtnTemperature.Name = "rbtnTemperature";
            this.rbtnTemperature.Size = new System.Drawing.Size(118, 23);
            this.rbtnTemperature.TabIndex = 11;
            this.rbtnTemperature.TabStop = true;
            this.rbtnTemperature.Text = "Temperature";
            this.rbtnTemperature.UseVisualStyleBackColor = true;
            this.rbtnTemperature.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // rbtnPlasma
            // 
            this.rbtnPlasma.AutoSize = true;
            this.rbtnPlasma.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPlasma.Location = new System.Drawing.Point(10, 61);
            this.rbtnPlasma.Name = "rbtnPlasma";
            this.rbtnPlasma.Size = new System.Drawing.Size(77, 23);
            this.rbtnPlasma.TabIndex = 12;
            this.rbtnPlasma.TabStop = true;
            this.rbtnPlasma.Text = "Plasma";
            this.rbtnPlasma.UseVisualStyleBackColor = true;
            this.rbtnPlasma.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // rbtnHybrid
            // 
            this.rbtnHybrid.AutoSize = true;
            this.rbtnHybrid.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHybrid.Location = new System.Drawing.Point(10, 97);
            this.rbtnHybrid.Name = "rbtnHybrid";
            this.rbtnHybrid.Size = new System.Drawing.Size(74, 23);
            this.rbtnHybrid.TabIndex = 13;
            this.rbtnHybrid.TabStop = true;
            this.rbtnHybrid.Text = "Hybrid";
            this.rbtnHybrid.UseVisualStyleBackColor = true;
            this.rbtnHybrid.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnTemperature);
            this.groupBox1.Controls.Add(this.rbtnHybrid);
            this.groupBox1.Controls.Add(this.rbtnPlasma);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(451, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 136);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Name";
            // 
            // frModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 438);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSelectDepatcher);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvModelViewer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSelectID);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtSelectName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frModel";
            this.Text = "Model";
            this.Load += new System.EventHandler(this.frModel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelViewer)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.TextBox txtSelectID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvModelViewer;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        public System.Windows.Forms.TextBox txtSelectName;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSelectDepatcher;
        private System.Windows.Forms.RadioButton rbtnTemperature;
        private System.Windows.Forms.RadioButton rbtnPlasma;
        private System.Windows.Forms.RadioButton rbtnHybrid;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}