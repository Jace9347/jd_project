﻿namespace JDX_Library_X86.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    public class ClsLog
    {

        public static string HeaderLog = null;

        public static void WriteLog(string path, string file, string[] strMsg, DateTime dt)
        {
            //szFileName = dt.ToString("yyyy-MM-dd");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (!File.Exists(path + file))
            {
                using (StreamWriter sw = File.CreateText(path + file))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(path + file))
            {
                for (int i = 0; i < strMsg.Length; i++)
                {
                    sw.WriteLine(strMsg[i]);
                }
                sw.Close(); // File Close
            }
        }

        public static void WriteLog(string path, string file, string strMsg, DateTime dt)
        {
            //szFileName = dt.ToString("yyyy-MM-dd");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (!File.Exists(path + file))
            {
                using (StreamWriter sw = File.CreateText(path + file))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(path + file))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
     
        #region READ File
        public static string[] ReadLog(string Url)
        {
            if (!File.Exists(Url))
            {
                return null;
            }
            else
            {
                string[] strTxtValue = File.ReadAllLines(Url);
                if (strTxtValue.Length > 0)
                {
                    return strTxtValue;
                }
                else
                {
                    return null;
                }
            }
        }  
        #endregion      

    }
    
}
