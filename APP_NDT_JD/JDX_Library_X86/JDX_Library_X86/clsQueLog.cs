﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JDX_Library_X86
{
    using System.Data;
    using System.Collections.Concurrent;
    using Common;

    public class clsQueLog
    {
        ConcurrentQueue<string> LogQ;
        private string Path = null;
        private string File = null;

        public clsQueLog()
        {
            LogQ = new ConcurrentQueue<string>();
        }

        public void Log_Enqueue(string dat)
        {
            LogQ.Enqueue(dat);
        }

        public void Log_Dequeue()
        { 
            string log = null;
            lock (LogQ)
            {
                string[] dat = new string[LogQ.Count];
                for (int idx = 0; idx < LogQ.Count; idx++)
                {
                    LogQ.TryDequeue(out log);
                    if (!string.IsNullOrEmpty(log))
                    {
                        dat[idx] = log;
                    }
                }
                ClsLog.WriteLog(Path, File, dat, DateTime.Now);
            }
        }

        public int Que_Count
        {
            get
            {
                return LogQ.Count;
            }
        }

        public string Que_Path
        {
            set
            {
                Path = value;
            }
        }

        public string Que_File
        {
            set
            {
                File = value;
            }
        }
    }
}
