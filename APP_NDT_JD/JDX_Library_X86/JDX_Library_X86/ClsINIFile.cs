﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;//Directroy를 만들기 위해서 쓰기위한 사용
using System.Runtime.InteropServices;//ini파일관리
//using System.Windows.Forms;

namespace JDX_Library_X86
{
    public class ClsIniFile
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
            
        // INI 값 읽기
        public static String GetValue(String section, String key, String iniFilePath)
        {
            try
            {
                StringBuilder sbTemp = new StringBuilder(255);
                int i = GetPrivateProfileString(section, key, "", sbTemp, 255, iniFilePath);
                return sbTemp.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("["+ ex.Message +"]");
                return "Error";
            }
        }
        // INI 값 설정 
        public static bool SetValue(String section, String key, String value, String iniFilePath)
        {
            try
            {
                WritePrivateProfileString(section, key, value, iniFilePath);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[" + ex.Message + "]");
                return false;
            }
        }


    }
}
