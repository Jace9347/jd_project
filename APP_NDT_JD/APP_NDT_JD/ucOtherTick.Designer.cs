﻿namespace APP_NDT_JD
{
    partial class ucOtherTick
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.group1 = new DevExpress.XtraEditors.GroupControl();
            this.lbTick1 = new System.Windows.Forms.Label();
            this.pbIO1 = new System.Windows.Forms.PictureBox();
            this.group2 = new DevExpress.XtraEditors.GroupControl();
            this.pbIO2 = new System.Windows.Forms.PictureBox();
            this.lbTick2 = new System.Windows.Forms.Label();
            this.group3 = new DevExpress.XtraEditors.GroupControl();
            this.pbIO3 = new System.Windows.Forms.PictureBox();
            this.lbTick3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.group4 = new DevExpress.XtraEditors.GroupControl();
            this.pbIO4 = new System.Windows.Forms.PictureBox();
            this.lbTick4 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.group5 = new DevExpress.XtraEditors.GroupControl();
            this.pbIO5 = new System.Windows.Forms.PictureBox();
            this.lbTick5 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.group6 = new DevExpress.XtraEditors.GroupControl();
            this.pbIO6 = new System.Windows.Forms.PictureBox();
            this.lbTick6 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group1)).BeginInit();
            this.group1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group2)).BeginInit();
            this.group2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group3)).BeginInit();
            this.group3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group4)).BeginInit();
            this.group4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group5)).BeginInit();
            this.group5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group6)).BeginInit();
            this.group6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::APP_NDT_JD.Properties.Resources.Arrowhead_Right;
            this.pictureBox2.Location = new System.Drawing.Point(141, 14);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // group1
            // 
            this.group1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group1.Appearance.Options.UseBackColor = true;
            this.group1.Appearance.Options.UseFont = true;
            this.group1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group1.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group1.AppearanceCaption.Options.UseFont = true;
            this.group1.AppearanceCaption.Options.UseForeColor = true;
            this.group1.Controls.Add(this.pbIO1);
            this.group1.Controls.Add(this.lbTick1);
            this.group1.Location = new System.Drawing.Point(3, 3);
            this.group1.Name = "group1";
            this.group1.Size = new System.Drawing.Size(132, 52);
            this.group1.TabIndex = 2;
            this.group1.Text = "Forward";
            // 
            // lbTick1
            // 
            this.lbTick1.AutoSize = true;
            this.lbTick1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick1.Location = new System.Drawing.Point(8, 28);
            this.lbTick1.Name = "lbTick1";
            this.lbTick1.Size = new System.Drawing.Size(91, 16);
            this.lbTick1.TabIndex = 1;
            this.lbTick1.Text = "Tick : 0.0 sec";
            // 
            // pbIO1
            // 
            this.pbIO1.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO1.Location = new System.Drawing.Point(107, 2);
            this.pbIO1.Name = "pbIO1";
            this.pbIO1.Size = new System.Drawing.Size(20, 16);
            this.pbIO1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO1.TabIndex = 0;
            this.pbIO1.TabStop = false;
            // 
            // group2
            // 
            this.group2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group2.Appearance.Options.UseBackColor = true;
            this.group2.Appearance.Options.UseFont = true;
            this.group2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group2.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group2.AppearanceCaption.Options.UseFont = true;
            this.group2.AppearanceCaption.Options.UseForeColor = true;
            this.group2.Controls.Add(this.pbIO2);
            this.group2.Controls.Add(this.lbTick2);
            this.group2.Location = new System.Drawing.Point(171, 3);
            this.group2.Name = "group2";
            this.group2.Size = new System.Drawing.Size(132, 52);
            this.group2.TabIndex = 3;
            this.group2.Text = "Forward";
            // 
            // pbIO2
            // 
            this.pbIO2.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO2.Location = new System.Drawing.Point(107, 2);
            this.pbIO2.Name = "pbIO2";
            this.pbIO2.Size = new System.Drawing.Size(20, 16);
            this.pbIO2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO2.TabIndex = 0;
            this.pbIO2.TabStop = false;
            // 
            // lbTick2
            // 
            this.lbTick2.AutoSize = true;
            this.lbTick2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick2.Location = new System.Drawing.Point(8, 28);
            this.lbTick2.Name = "lbTick2";
            this.lbTick2.Size = new System.Drawing.Size(91, 16);
            this.lbTick2.TabIndex = 1;
            this.lbTick2.Text = "Tick : 0.0 sec";
            // 
            // group3
            // 
            this.group3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group3.Appearance.Options.UseBackColor = true;
            this.group3.Appearance.Options.UseFont = true;
            this.group3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group3.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group3.AppearanceCaption.Options.UseFont = true;
            this.group3.AppearanceCaption.Options.UseForeColor = true;
            this.group3.Controls.Add(this.pbIO3);
            this.group3.Controls.Add(this.lbTick3);
            this.group3.Location = new System.Drawing.Point(339, 3);
            this.group3.Name = "group3";
            this.group3.Size = new System.Drawing.Size(132, 52);
            this.group3.TabIndex = 5;
            this.group3.Text = "Forward";
            // 
            // pbIO3
            // 
            this.pbIO3.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO3.Location = new System.Drawing.Point(107, 2);
            this.pbIO3.Name = "pbIO3";
            this.pbIO3.Size = new System.Drawing.Size(20, 16);
            this.pbIO3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO3.TabIndex = 0;
            this.pbIO3.TabStop = false;
            // 
            // lbTick3
            // 
            this.lbTick3.AutoSize = true;
            this.lbTick3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick3.Location = new System.Drawing.Point(8, 28);
            this.lbTick3.Name = "lbTick3";
            this.lbTick3.Size = new System.Drawing.Size(91, 16);
            this.lbTick3.TabIndex = 1;
            this.lbTick3.Text = "Tick : 0.0 sec";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::APP_NDT_JD.Properties.Resources.Arrowhead_Right;
            this.pictureBox4.Location = new System.Drawing.Point(309, 14);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(24, 24);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // group4
            // 
            this.group4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group4.Appearance.Options.UseBackColor = true;
            this.group4.Appearance.Options.UseFont = true;
            this.group4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group4.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group4.AppearanceCaption.Options.UseFont = true;
            this.group4.AppearanceCaption.Options.UseForeColor = true;
            this.group4.Controls.Add(this.pbIO4);
            this.group4.Controls.Add(this.lbTick4);
            this.group4.Location = new System.Drawing.Point(507, 3);
            this.group4.Name = "group4";
            this.group4.Size = new System.Drawing.Size(132, 52);
            this.group4.TabIndex = 7;
            this.group4.Text = "Forward";
            // 
            // pbIO4
            // 
            this.pbIO4.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO4.Location = new System.Drawing.Point(107, 2);
            this.pbIO4.Name = "pbIO4";
            this.pbIO4.Size = new System.Drawing.Size(20, 16);
            this.pbIO4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO4.TabIndex = 0;
            this.pbIO4.TabStop = false;
            // 
            // lbTick4
            // 
            this.lbTick4.AutoSize = true;
            this.lbTick4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick4.Location = new System.Drawing.Point(8, 28);
            this.lbTick4.Name = "lbTick4";
            this.lbTick4.Size = new System.Drawing.Size(91, 16);
            this.lbTick4.TabIndex = 1;
            this.lbTick4.Text = "Tick : 0.0 sec";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::APP_NDT_JD.Properties.Resources.Arrowhead_Right;
            this.pictureBox6.Location = new System.Drawing.Point(477, 14);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(24, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            // 
            // group5
            // 
            this.group5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group5.Appearance.Options.UseBackColor = true;
            this.group5.Appearance.Options.UseFont = true;
            this.group5.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group5.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group5.AppearanceCaption.Options.UseFont = true;
            this.group5.AppearanceCaption.Options.UseForeColor = true;
            this.group5.Controls.Add(this.pbIO5);
            this.group5.Controls.Add(this.lbTick5);
            this.group5.Location = new System.Drawing.Point(675, 3);
            this.group5.Name = "group5";
            this.group5.Size = new System.Drawing.Size(132, 52);
            this.group5.TabIndex = 9;
            this.group5.Text = "Forward";
            // 
            // pbIO5
            // 
            this.pbIO5.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO5.Location = new System.Drawing.Point(107, 2);
            this.pbIO5.Name = "pbIO5";
            this.pbIO5.Size = new System.Drawing.Size(20, 16);
            this.pbIO5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO5.TabIndex = 0;
            this.pbIO5.TabStop = false;
            // 
            // lbTick5
            // 
            this.lbTick5.AutoSize = true;
            this.lbTick5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick5.Location = new System.Drawing.Point(8, 28);
            this.lbTick5.Name = "lbTick5";
            this.lbTick5.Size = new System.Drawing.Size(91, 16);
            this.lbTick5.TabIndex = 1;
            this.lbTick5.Text = "Tick : 0.0 sec";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::APP_NDT_JD.Properties.Resources.Arrowhead_Right;
            this.pictureBox8.Location = new System.Drawing.Point(645, 14);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(24, 24);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 10;
            this.pictureBox8.TabStop = false;
            // 
            // group6
            // 
            this.group6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group6.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group6.Appearance.Options.UseBackColor = true;
            this.group6.Appearance.Options.UseFont = true;
            this.group6.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group6.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group6.AppearanceCaption.Options.UseFont = true;
            this.group6.AppearanceCaption.Options.UseForeColor = true;
            this.group6.Controls.Add(this.pbIO6);
            this.group6.Controls.Add(this.lbTick6);
            this.group6.Location = new System.Drawing.Point(843, 3);
            this.group6.Name = "group6";
            this.group6.Size = new System.Drawing.Size(132, 52);
            this.group6.TabIndex = 11;
            this.group6.Text = "Forward";
            // 
            // pbIO6
            // 
            this.pbIO6.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO6.Location = new System.Drawing.Point(107, 2);
            this.pbIO6.Name = "pbIO6";
            this.pbIO6.Size = new System.Drawing.Size(20, 16);
            this.pbIO6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO6.TabIndex = 0;
            this.pbIO6.TabStop = false;
            // 
            // lbTick6
            // 
            this.lbTick6.AutoSize = true;
            this.lbTick6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick6.Location = new System.Drawing.Point(8, 28);
            this.lbTick6.Name = "lbTick6";
            this.lbTick6.Size = new System.Drawing.Size(91, 16);
            this.lbTick6.TabIndex = 1;
            this.lbTick6.Text = "Tick : 0.0 sec";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::APP_NDT_JD.Properties.Resources.Arrowhead_Right;
            this.pictureBox10.Location = new System.Drawing.Point(813, 14);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(24, 24);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 12;
            this.pictureBox10.TabStop = false;
            // 
            // ucOtherTick
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.group6);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.group5);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.group4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.group3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.group2);
            this.Controls.Add(this.group1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "ucOtherTick";
            this.Size = new System.Drawing.Size(979, 71);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group1)).EndInit();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group2)).EndInit();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group3)).EndInit();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group4)).EndInit();
            this.group4.ResumeLayout(false);
            this.group4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group5)).EndInit();
            this.group5.ResumeLayout(false);
            this.group5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group6)).EndInit();
            this.group6.ResumeLayout(false);
            this.group6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.GroupControl group1;
        private System.Windows.Forms.PictureBox pbIO1;
        private System.Windows.Forms.Label lbTick1;
        private DevExpress.XtraEditors.GroupControl group2;
        private System.Windows.Forms.PictureBox pbIO2;
        private System.Windows.Forms.Label lbTick2;
        private DevExpress.XtraEditors.GroupControl group3;
        private System.Windows.Forms.PictureBox pbIO3;
        private System.Windows.Forms.Label lbTick3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private DevExpress.XtraEditors.GroupControl group4;
        private System.Windows.Forms.PictureBox pbIO4;
        private System.Windows.Forms.Label lbTick4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private DevExpress.XtraEditors.GroupControl group5;
        private System.Windows.Forms.PictureBox pbIO5;
        private System.Windows.Forms.Label lbTick5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private DevExpress.XtraEditors.GroupControl group6;
        private System.Windows.Forms.PictureBox pbIO6;
        private System.Windows.Forms.Label lbTick6;
        private System.Windows.Forms.PictureBox pictureBox10;
    }
}
