﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_NDT_JD
{
    using System.Diagnostics;
    public partial class ucBattModule : UserControl
    {
        public ucBattModule()
        {
            InitializeComponent();
        }

        private void ucBattModule_Load(object sender, EventArgs e)
        {

        }

        public struct Item
        {
            public DateTime dtDateTime;
            public Stopwatch swTick;
            public string Number;
            public string Id;
            public string OrthopedicTime;
            public string OrthopedicTemperature;
            public string Ir;
            public string Ocv;
            public string Thickness;
            public string Height;
            public string Weight;
        }


    }
}
