﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_NDT_JD
{
    public partial class ucTick : UserControl
    {
        public ucTick()
        {
            InitializeComponent();
        }

        [Category("ucModule"), Description("groupText")]
        public string groupText
        {
            get
            {
                return this.group.Text;
            }
            set
            {
                this.group.Text = value;
            }
        }

        [Category("ucModule"), Description("TickText")]
        public string TickText
        {
            get
            {
                return this.lbTick.Text;
            }
            set
            {
                this.lbTick.Text = value;
            }
        }
    }
}
