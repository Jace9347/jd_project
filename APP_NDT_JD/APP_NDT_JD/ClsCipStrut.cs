﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_NDT_JD
{
    public class ClsCipStrut
    {
        /*
         * CIP Meassage Strut
         * Link Path, Service Code, Request Path, Request Service Data
         */

        private string IP = "192.168.1.2";
        private int PortNumber = 44818; //Default CIP msg

        const int TCP_PORT = 9600;
        const int CIP_PORT = 44818;

        const int READ_MAX = 999; //FINS Read Word Max
        const int WRITE_MAX = 996;//FINS Write Word Max

        public enum EDataType
        {
            //CIP Common DataType
            BOOL = 0xC1,
            SINT = 0xC2,
            INT = 0xC3,
            DINT = 0xC4,
            LINT = 0xC5,
            USINT = 0xC6,
            UINT = 0xC7,
            UDINT = 0xC8,
            ULINT = 0xC9,
            REAL = 0xCA,
            LREAL = 0xCB,
            STRING = 0xD0,
            BYTE = 0xD1,
            WORD = 0xD2,
            DWORD = 0xD3,
            TIME = 0xDB,
            LWORD = 0xD4,
            Abbreviated_STRUCT = 0xA0,

            //Vendor Specific
            UINT_BCD = 0x04,
            UDINT_BCD = 0x05,
            ULINT_BCD = 0x06,
            ENUM = 0x07,
            DATE_NSEC = 0x08,
            TIME_NSEC = 0x09,
            DATE_AND_TIME_NSEC = 0x0A,
            TIME_OF_DAY_NSEC = 0x0B,
            Union = 0x0C,
        };

        //Read Ack code
        public enum EMeassage
        {
            SUCCESS = 0x00,//정상
            RESOURCE_UNABAILABLE = 0x02,//내부처리용 버퍼를 현재 이용할 수 없음
            PATH_SEGMENT_ERROR = 0x04,//요청 경로 지정이 틀림
            PATH_DESTINATION_UNKNOWN = 0x05,//변수의 수신측 지정이 잘못됨
            OBJECT_STATE_CONFLICT = 0x0C, //ADD State 8010 다운로드 중, 8011 태그 메모리 이상중
            REPLY_DATA_TOO_LARGE = 0x11,//최대 응답길이 초과
            NOT_ENOUGH_DATA = 0x13,//데이터 길이 부족
            TOO_MUCH_DATA = 0x15,//데이터 길이 과다
            VENDOR_SPECIFIC_ERROR = 0x1F,//ADD State
            INVALID_PARAMETER = 0x20,
        };

        public enum EUnitNode
        {
            CPU = 0x00,
            EtherNet_IP = 0x10, // Unit Number + 0x10
            IO = 0x20,//Unit Number + 0x20
            INNER_Board = 0xE1,
            PC = 0x01,
            Network = 0xFE,
        };

        public enum EProductCode
        {
            NJ501_13 = 0x0665,
            NJ501_14 = 0x0666,
            NJ501_15 = 0x0667,
            NJ301_11 = 0x066C,
            NJ301_12 = 0x066C,
            CJ1W_EIP21 = 0x0668,
        };

        public enum EDevice
        {
            DM = 0x82,//Word Type Data Memory 32768 D
            CIO = 0x30,//Bit Type 6144 (0 ~ 6143)
            WR = 0x00,//Work Relay 512 W
            HR = 0x00,//Latch Relay 1536 H
            EM = 0x00//Latch Relay 32768 E0_0 ~ E18_32767 
        };

        public bool Read()
        {
            bool rtn = false;
            try
            {

            }
            catch
            { }
            return rtn;
        }

        public bool Write()
        {
            bool rtn = false;
            try
            {

            }
            catch
            { }
            return rtn;
        }

        private byte[] Make_Frame()
        {
            byte[] tmp = new byte[0];


            return tmp;
        }
    }
}
