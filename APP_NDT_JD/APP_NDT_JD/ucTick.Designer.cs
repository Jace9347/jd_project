﻿namespace APP_NDT_JD
{
    partial class ucTick
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.group = new DevExpress.XtraEditors.GroupControl();
            this.lbTick = new System.Windows.Forms.Label();
            this.pbIO = new System.Windows.Forms.PictureBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.group)).BeginInit();
            this.group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // group
            // 
            this.group.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.group.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group.Appearance.Options.UseBackColor = true;
            this.group.Appearance.Options.UseFont = true;
            this.group.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.group.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.group.AppearanceCaption.Options.UseFont = true;
            this.group.AppearanceCaption.Options.UseForeColor = true;
            this.group.Controls.Add(this.pbIO);
            this.group.Controls.Add(this.lbTick);
            this.group.Location = new System.Drawing.Point(5, 5);
            this.group.Name = "group";
            this.group.Size = new System.Drawing.Size(133, 52);
            this.group.TabIndex = 2;
            this.group.Text = "Forward";
            // 
            // lbTick
            // 
            this.lbTick.AutoSize = true;
            this.lbTick.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTick.Location = new System.Drawing.Point(8, 28);
            this.lbTick.Name = "lbTick";
            this.lbTick.Size = new System.Drawing.Size(91, 16);
            this.lbTick.TabIndex = 1;
            this.lbTick.Text = "Tick : 0.0 sec";
            // 
            // pbIO
            // 
            this.pbIO.Image = global::APP_NDT_JD.Properties.Resources.LED_Gray;
            this.pbIO.Location = new System.Drawing.Point(107, 2);
            this.pbIO.Name = "pbIO";
            this.pbIO.Size = new System.Drawing.Size(20, 16);
            this.pbIO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIO.TabIndex = 0;
            this.pbIO.TabStop = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.panelControl1.Controls.Add(this.group);
            this.panelControl1.Location = new System.Drawing.Point(3, 3);
            this.panelControl1.LookAndFeel.SkinName = "Seven Classic";
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(143, 63);
            this.panelControl1.TabIndex = 1;
            // 
            // ucTick
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "ucTick";
            this.Size = new System.Drawing.Size(146, 80);
            ((System.ComponentModel.ISupportInitialize)(this.group)).EndInit();
            this.group.ResumeLayout(false);
            this.group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl group;
        private System.Windows.Forms.PictureBox pbIO;
        private System.Windows.Forms.Label lbTick;
        private DevExpress.XtraEditors.PanelControl panelControl1;
    }
}
