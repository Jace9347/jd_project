﻿namespace APP_NDT_JD
{
    partial class FormMain
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.TileItemElement tileItemElement49 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement57 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement50 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement51 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement52 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement53 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement54 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement55 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement56 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement58 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement59 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement60 = new DevExpress.XtraEditors.TileItemElement();
            this.lstSend = new System.Windows.Forms.ListBox();
            this.navigationFrame1 = new DevExpress.XtraBars.Navigation.NavigationFrame();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.lstRecv = new System.Windows.Forms.ListBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.navigationPage2 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.navigationPage3 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.tileNavPane1 = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navButton2 = new DevExpress.XtraBars.Navigation.NavButton();
            this.tileNavCategory4 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavCategory3 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavItem5 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem6 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem7 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem8 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem9 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.Visual = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavItem2 = new DevExpress.XtraBars.Navigation.TileNavItem();
            this.tileNavCategory5 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavCategory6 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.tileNavCategory7 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.ucOtherTick1 = new APP_NDT_JD.ucOtherTick();
            ((System.ComponentModel.ISupportInitialize)(this.navigationFrame1)).BeginInit();
            this.navigationFrame1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstSend
            // 
            this.lstSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSend.FormattingEnabled = true;
            this.lstSend.ItemHeight = 14;
            this.lstSend.Location = new System.Drawing.Point(2, 21);
            this.lstSend.Margin = new System.Windows.Forms.Padding(0);
            this.lstSend.Name = "lstSend";
            this.lstSend.Size = new System.Drawing.Size(451, 101);
            this.lstSend.TabIndex = 0;
            // 
            // navigationFrame1
            // 
            this.navigationFrame1.Controls.Add(this.navigationPage1);
            this.navigationFrame1.Controls.Add(this.navigationPage2);
            this.navigationFrame1.Controls.Add(this.navigationPage3);
            this.navigationFrame1.Location = new System.Drawing.Point(1, 1);
            this.navigationFrame1.Name = "navigationFrame1";
            this.navigationFrame1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1,
            this.navigationPage2,
            this.navigationPage3});
            this.navigationFrame1.SelectedPage = this.navigationPage1;
            this.navigationFrame1.Size = new System.Drawing.Size(1252, 622);
            this.navigationFrame1.TabIndex = 1;
            this.navigationFrame1.Text = "navigationFrame1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.AutoScroll = true;
            this.navigationPage1.Controls.Add(this.ucOtherTick1);
            this.navigationPage1.Controls.Add(this.tableLayoutPanel1);
            this.navigationPage1.Margin = new System.Windows.Forms.Padding(0);
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(1252, 622);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupControl2);
            this.tableLayoutPanel1.Controls.Add(this.groupControl1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 498);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(911, 124);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.lstRecv);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(455, 0);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(456, 124);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "RECV";
            // 
            // lstRecv
            // 
            this.lstRecv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstRecv.FormattingEnabled = true;
            this.lstRecv.ItemHeight = 14;
            this.lstRecv.Location = new System.Drawing.Point(2, 21);
            this.lstRecv.Margin = new System.Windows.Forms.Padding(0);
            this.lstRecv.Name = "lstRecv";
            this.lstRecv.Size = new System.Drawing.Size(452, 101);
            this.lstRecv.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.lstSend);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(455, 124);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "SEND";
            // 
            // navigationPage2
            // 
            this.navigationPage2.Caption = "navigationPage2";
            this.navigationPage2.Name = "navigationPage2";
            this.navigationPage2.Size = new System.Drawing.Size(1252, 622);
            // 
            // navigationPage3
            // 
            this.navigationPage3.Caption = "navigationPage3";
            this.navigationPage3.Name = "navigationPage3";
            this.navigationPage3.Size = new System.Drawing.Size(1252, 622);
            // 
            // tileNavPane1
            // 
            this.tileNavPane1.Buttons.Add(this.navButton2);
            this.tileNavPane1.Categories.AddRange(new DevExpress.XtraBars.Navigation.TileNavCategory[] {
            this.tileNavCategory4,
            this.tileNavCategory3,
            this.tileNavCategory5,
            this.tileNavCategory6,
            this.tileNavCategory7});
            // 
            // tileNavCategory1
            // 
            this.tileNavPane1.DefaultCategory.Name = "tileNavCategory1";
            this.tileNavPane1.DefaultCategory.OwnerCollection = null;
            // 
            // 
            // 
            this.tileNavPane1.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileNavPane1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileNavPane1.Location = new System.Drawing.Point(0, 0);
            this.tileNavPane1.Margin = new System.Windows.Forms.Padding(0);
            this.tileNavPane1.Name = "tileNavPane1";
            this.tileNavPane1.Size = new System.Drawing.Size(1260, 50);
            this.tileNavPane1.TabIndex = 3;
            this.tileNavPane1.Text = "tileNavPane1";
            // 
            // navButton2
            // 
            this.navButton2.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton2.Caption = "navButton2";
            this.navButton2.Name = "navButton2";
            // 
            // tileNavCategory4
            // 
            this.tileNavCategory4.Caption = "tileNavCategory4";
            this.tileNavCategory4.Name = "tileNavCategory4";
            this.tileNavCategory4.OwnerCollection = this.tileNavPane1.Categories;
            // 
            // 
            // 
            this.tileNavCategory4.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement49.Text = "Non Destructive Testing";
            this.tileNavCategory4.Tile.Elements.Add(tileItemElement49);
            this.tileNavCategory4.Tile.Name = "tileBarItem3";
            // 
            // tileNavCategory3
            // 
            this.tileNavCategory3.Caption = "tileNavCategory3";
            this.tileNavCategory3.Items.AddRange(new DevExpress.XtraBars.Navigation.TileNavItem[] {
            this.tileNavItem5,
            this.tileNavItem6,
            this.tileNavItem7,
            this.tileNavItem8,
            this.tileNavItem9,
            this.Visual,
            this.tileNavItem2});
            this.tileNavCategory3.Name = "tileNavCategory3";
            this.tileNavCategory3.OwnerCollection = this.tileNavPane1.Categories;
            // 
            // 
            // 
            this.tileNavCategory3.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement57.Text = "Motion";
            this.tileNavCategory3.Tile.Elements.Add(tileItemElement57);
            this.tileNavCategory3.Tile.Name = "tileBarItem2";
            // 
            // tileNavItem5
            // 
            this.tileNavItem5.Caption = "tileNavItem5";
            this.tileNavItem5.Name = "tileNavItem5";
            this.tileNavItem5.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem5.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement50.Text = "Orthopedic";
            this.tileNavItem5.Tile.Elements.Add(tileItemElement50);
            this.tileNavItem5.Tile.Name = "tileBarItem8";
            // 
            // tileNavItem6
            // 
            this.tileNavItem6.Caption = "tileNavItem6";
            this.tileNavItem6.Name = "tileNavItem6";
            this.tileNavItem6.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem6.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement51.Text = "IR/OCV";
            this.tileNavItem6.Tile.Elements.Add(tileItemElement51);
            this.tileNavItem6.Tile.Name = "tileBarItem9";
            // 
            // tileNavItem7
            // 
            this.tileNavItem7.Caption = "tileNavItem7";
            this.tileNavItem7.Name = "tileNavItem7";
            this.tileNavItem7.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem7.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement52.Text = "Thickness";
            this.tileNavItem7.Tile.Elements.Add(tileItemElement52);
            this.tileNavItem7.Tile.Name = "tileBarItem10";
            // 
            // tileNavItem8
            // 
            this.tileNavItem8.Caption = "tileNavItem8";
            this.tileNavItem8.Name = "tileNavItem8";
            this.tileNavItem8.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem8.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement53.Text = "Height";
            this.tileNavItem8.Tile.Elements.Add(tileItemElement53);
            this.tileNavItem8.Tile.Name = "tileBarItem11";
            // 
            // tileNavItem9
            // 
            this.tileNavItem9.Caption = "tileNavItem9";
            this.tileNavItem9.Name = "tileNavItem9";
            this.tileNavItem9.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem9.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement54.Text = "Width";
            this.tileNavItem9.Tile.Elements.Add(tileItemElement54);
            this.tileNavItem9.Tile.Name = "tileBarItem12";
            // 
            // Visual
            // 
            this.Visual.Caption = "tileNavItem1";
            this.Visual.Name = "Visual";
            this.Visual.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.Visual.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement55.Text = "Visual";
            this.Visual.Tile.Elements.Add(tileItemElement55);
            this.Visual.Tile.Name = "tileBarItem1";
            // 
            // tileNavItem2
            // 
            this.tileNavItem2.Caption = "tileNavItem2";
            this.tileNavItem2.Name = "tileNavItem2";
            this.tileNavItem2.OwnerCollection = this.tileNavCategory3.Items;
            // 
            // 
            // 
            this.tileNavItem2.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement56.Text = "Shilling";
            this.tileNavItem2.Tile.Elements.Add(tileItemElement56);
            this.tileNavItem2.Tile.Name = "tileBarItem5";
            // 
            // tileNavCategory5
            // 
            this.tileNavCategory5.Caption = "tileNavCategory5";
            this.tileNavCategory5.Name = "tileNavCategory5";
            this.tileNavCategory5.OwnerCollection = this.tileNavPane1.Categories;
            // 
            // 
            // 
            this.tileNavCategory5.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement58.Text = "IO";
            this.tileNavCategory5.Tile.Elements.Add(tileItemElement58);
            this.tileNavCategory5.Tile.Name = "tileBarItem3";
            // 
            // tileNavCategory6
            // 
            this.tileNavCategory6.Caption = "tileNavCategory6";
            this.tileNavCategory6.Name = "tileNavCategory6";
            this.tileNavCategory6.OwnerCollection = this.tileNavPane1.Categories;
            // 
            // 
            // 
            this.tileNavCategory6.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement59.Text = "PLC";
            this.tileNavCategory6.Tile.Elements.Add(tileItemElement59);
            this.tileNavCategory6.Tile.Name = "tileBarItem4";
            // 
            // tileNavCategory7
            // 
            this.tileNavCategory7.Caption = "tileNavCategory7";
            this.tileNavCategory7.Name = "tileNavCategory7";
            this.tileNavCategory7.OwnerCollection = this.tileNavPane1.Categories;
            // 
            // 
            // 
            this.tileNavCategory7.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            tileItemElement60.Text = "Vision";
            this.tileNavCategory7.Tile.Elements.Add(tileItemElement60);
            this.tileNavCategory7.Tile.Name = "tileBarItem7";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(0, 50);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 627);
            this.splitterControl1.TabIndex = 4;
            this.splitterControl1.TabStop = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl1.Location = new System.Drawing.Point(5, 50);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AutoScroll = true;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.splitContainerControl1.Panel1.Controls.Add(this.navigationFrame1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1255, 627);
            this.splitContainerControl1.SplitterPosition = 1015;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // ucOtherTick1
            // 
            this.ucOtherTick1.groupText = "Forward";
            this.ucOtherTick1.Location = new System.Drawing.Point(4, 3);
            this.ucOtherTick1.Name = "ucOtherTick1";
            this.ucOtherTick1.Size = new System.Drawing.Size(979, 71);
            this.ucOtherTick1.TabIndex = 2;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1260, 677);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.tileNavPane1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormMain";
            this.Text = "Non_Destructive_Testing Ver 1.0";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navigationFrame1)).EndInit();
            this.navigationFrame1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstSend;
        private DevExpress.XtraBars.Navigation.NavigationFrame navigationFrame1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage2;
        private DevExpress.XtraBars.Navigation.TileNavPane tileNavPane1;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory4;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.ListBox lstRecv;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory3;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory5;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory6;
        private DevExpress.XtraBars.Navigation.NavButton navButton2;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem5;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem6;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem7;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem8;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem9;
        private DevExpress.XtraBars.Navigation.TileNavItem Visual;
        private DevExpress.XtraBars.Navigation.TileNavItem tileNavItem2;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory7;
        private ucOtherTick ucOtherTick1;
    }
}

