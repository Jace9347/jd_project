﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APP_NDT_JD
{
    public partial class ucOtherTick : UserControl
    {
        public ucOtherTick()
        {
            InitializeComponent();
        }

        [Category("ucModule"), Description("groupText")]
        public string groupText
        {
            get
            {
                return this.group1.Text;
            }
            set
            {
                this.group1.Text = value;
            }
        }
    }
}
