﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App_Calc_Table
{
    public partial class UserControl_ListView : UserControl
    {
        public UserControl_ListView()
        {
            InitializeComponent();
        }

        public void Init()
        {
            this.Size = Parent.Size;
            lsvControl.Dock = DockStyle.Fill;

            lsvControl.Columns.Clear();
            lsvControl.Items.Clear();
            lsvControl.Groups.Clear();
            lsvControl.GridLines = true;
            //lsvControl.ShowGroups = false;
            //autosizestyle = new EResizeStyle();
        }

        public bool isVirturlDiposed()
        {
            bool rtn = false;

            if (lsvControl.Items.Count == 0)
            {
                rtn = true;
            }

            return rtn;
        }

        public void InitColumnsHeader(string[] heads)
        {
            lsvControl.Columns.Clear();
            lsvControl.Items.Clear();
            for (int i = 0; i < heads.Length; i++)
            {
                lsvControl.Columns.Add(heads[i], -2, HorizontalAlignment.Center);
            }
            AutoArrange(EResizeStyle.HeaderSize);
        }

        public void Test()
        {
            ListViewItem lv = new ListViewItem("TEST1");
            for (int i = 0; i < lsvControl.Columns.Count; i++)
            {
                lv.SubItems.Add("aa" + i.ToString());
            }
            lsvControl.Items.Add(lv);
            AutoArrange(EResizeStyle.HeaderSize);

            //Group Test
            //ListViewGroup listViewGroup = new ListViewGroup("TEST1");
            //lsvControl.Groups.Add(listViewGroup);
            //for (int i = 0; i < 100; i++)
            //{
            //    lsvControl.Items.Add(new ListViewItem(new string[] { "a", "b", i.ToString() }, listViewGroup));
            //}

            //listViewGroup = new ListViewGroup("TEST2");
            //lsvControl.Groups.Add(listViewGroup);

            //for (int i = 0; i < 100; i++)
            //{
            //    lsvControl.Items.Add(new ListViewItem(new string[] { "a", "b", i.ToString() }, listViewGroup));
            //}
        }

        public delegate void ListViewCallback();

        public void Insert()
        {

        }

        public void Clear()
        {
            lsvControl.Items.Clear();
        }

        public string[][] Get_SubItems()
        {
            string[][] dat = new string[lsvControl.Items.Count][];
            for (int i = 0; i < dat.GetLength(0); i++)
            {
                dat[i] = new string[lsvControl.Items[i].SubItems.Count];
                for (int j = 0; j < dat[i].Length; j++)
                {
                    dat[i][j] = lsvControl.Items[i].SubItems[j].Text;
                }
            }

            return dat;
        }

        public void Add(ListViewItem lvi)
        {
            lsvControl.Items.Add(lvi);
        }
        public void AddRange(ListViewItem[] lvi)
        {
            lsvControl.Items.AddRange(lvi);
        }

        public int ItemsCount()
        {
            return lsvControl.Items.Count;
        }

        public int ColumnsCount()
        {
            return lsvControl.Columns.Count;
        }

        public void ListViewErrorCheck()
        {
            string[][] dat = new string[lsvControl.Items.Count][];
            for (int i = 0; i < dat.GetLength(0); i++)
            {
                dat[i] = new string[lsvControl.Items[i].SubItems.Count];
                for (int j = 0; j < dat[i].Length; j++)
                {
                    if (string.IsNullOrEmpty(lsvControl.Items[i].SubItems[j].Text))
                    {
                        lsvControl.Columns.RemoveAt(j);
                        break;
                    }
                    else
                    {
                        double con = double.Parse(lsvControl.Items[i].SubItems[j].Text);
                        if (con < -1)
                        {
                            try
                            {
                                lsvControl.Items[i].SubItems[j].Text = null;
                                lsvControl.Items[i].SubItems.RemoveAt(j);
                                lsvControl.Columns.RemoveAt(j);
                                Console.WriteLine("ListView Subitems Count : " + lsvControl.Columns.Count);
                                break;
                            }
                            catch
                            {
                                break;
                            }
                        }
                        else
                        {
                            dat[i][j] = lsvControl.Items[i].SubItems[j].Text;
                        }
                    }
                }
            }
        }

        public enum EResizeStyle
        {
            HeaderSize, ColumnContent, None
        };

        //public EResizeStyle autosizestyle;

        public void AutoArrange(EResizeStyle style)
        {
            switch (style)
            {
                case EResizeStyle.HeaderSize:
                    lsvControl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    break;
                case EResizeStyle.ColumnContent:
                    lsvControl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                    break;
                case EResizeStyle.None:
                    lsvControl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);
                    break;
            }
        }
    }
}
