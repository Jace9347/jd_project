﻿namespace JDX_Library_X86
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    public class ClsLog
    {
        static string szFolder = String.Format("D:\\{0}\\0.Logs\\DataLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, DateTime.Now, DateTime.Now, DateTime.Now);  // 실시간 데이터;
        static string szFileName = DateTime.Now.ToString("yyyy-MM-dd");  // 실시간 데이터;   

        static string szProgramName = "SNT_TempSensor_Measure_System";
        static string strIrShortHeader = "";
        static string strThicknessHeader = "";
        static string strWeightHeader = "";
        static string strOcvHeader = "";

        public static void WriteLog(string path, string file,string[] strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (!File.Exists(path + file))
            {
                using (StreamWriter sw = File.CreateText(path + file))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(path + file))
            {
                for (int i = 0; i < strMsg.Length; i++)
                {
                    sw.WriteLine(strMsg[i]);
                }
                sw.Close(); // File Close
            }
        }

        public static void WriteLog(string path, string file, string strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (!File.Exists(path + file))
            {
                using (StreamWriter sw = File.CreateText(path + file))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(path + file))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
       
        
        public static void ProcessLog(string strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\SystemLogs\\4.ProcessLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt,dt);
            }
            else
            {
                szFolder = String.Format("./\\{0}\\SystemLogs\\4.ProcessLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt,dt);
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + "PROCESS" + ".log";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        } 
        public static void ErrorLog(string strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\SystemLogs\\3.ErrorLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt,dt); 
            }
            else
            {
                szFolder = String.Format("./\\{0}\\SystemLogs\\3.ErrorLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + "ERROR" + ".log";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
        public static void DataLog(string strMsg, DateTime dt, string FileName)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\DataLogs\\1.DataLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);  // 실시간 데이터;    
            }
            else
            {
                szFolder = String.Format("./\\{0}\\DataLogs\\1.DataLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);  // 실시간 데이터;    
            }   
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + FileName + ".CSV";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
                //Header Log
                using (StreamWriter sw = File.AppendText(sFilePath))
                {
                    //string msg = "No,Time,Temp";
                    //sw.WriteLine(msg);
                    //sw.Close(); // File Close
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
        public static void TotalDataLog(string strMsg)
        {
            szFileName = DateTime.Now.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\DataLogs\\2.Total_DataLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, DateTime.Now, DateTime.Now, DateTime.Now);  // 실시간 데이터;    
            }
            else
            {
                szFolder = String.Format("./\\{0}\\DataLogs\\2.Total_DataLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, DateTime.Now, DateTime.Now, DateTime.Now);  // 실시간 데이터;    
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" +".CSV";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
                //Header Log
                //using (StreamWriter sw = File.AppendText(sFilePath))
                //{
                //    string msg = "No,Channel,Date,Time,Model,TrayID,Value,M-Time,Result,Remarks";
                //    sw.WriteLine(msg);
                //    sw.Close(); // File Close
                //}
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
        public static void EquipLog(string strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\SystemLogs\\1.EquipmentLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName,dt, dt, dt);  // 실시간 데이터;    
            }
            else
            {
                szFolder = String.Format("./\\{0}\\SystemLogs\\1.EquipmentLog\\{1:yyyy}\\{2:MM}\\{3:dd}",szProgramName, dt, dt, dt);  // 실시간 데이터;    
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + "EQUIPMENT" + ".Log";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {  
                }
                //Header Log
                //using (StreamWriter sw = File.AppendText(sFilePath))
                //{
                //    //string msg = "No, Time, Contents";
                //    //sw.WriteLine(DateTime.Now.ToString("yy-MM-dd,HH:mm:ss.fff") + strMsg);
                //    sw.Close(); // File Close
                //}
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
        public static void WarningLog(string strMsg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\SystemLogs\\2.WaringLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt); 
            }
            else
            {
                szFolder = String.Format("./\\{0}\\SystemLogs\\2.WaringLog\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + "WARNING" + ".Log";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
                //Header Log
                //using (StreamWriter sw = File.AppendText(sFilePath))
                //{
                //    //string msg = "No, Time, Contents";
                //    //sw.WriteLine(msg);
                //    sw.Close(); // File Close
                //}
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(strMsg);
                sw.Close(); // File Close
            }
        }
        public static void PLC_Log(string msg, DateTime dt)
        {
            szFileName = dt.ToString("yyyy-MM-dd");  // 실시간 데이터;
            DriveInfo[] drv = DriveInfo.GetDrives();
            bool bDrvInfo = false;
            foreach (DriveInfo d in drv)
            {
                if (d.DriveType == DriveType.Fixed)
                {
                    if (d.Name == "D:\\")
                    {
                        bDrvInfo = true;
                        break;
                    }
                }
            }
            if (bDrvInfo)
            {
                szFolder = String.Format("D:\\{0}\\SystemLogs\\2.PLC_Log\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);
            }
            else
            {
                szFolder = String.Format("./\\{0}\\SystemLogs\\2.PLC_Log\\{1:yyyy}\\{2:MM}\\{3:dd}", szProgramName, dt, dt, dt);
            }
            if (!Directory.Exists(szFolder))
            {
                Directory.CreateDirectory(szFolder);
            }
            string sFilePath = szFolder + "\\" + szFileName + "-" + "PLC" + ".Log";   // 파일 경로
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
                //Header Log
                //using (StreamWriter sw = File.AppendText(sFilePath))
                //{
                //    //string msg = "No, Time, Contents";
                //    //sw.WriteLine(msg);
                //    sw.Close(); // File Close
                //}
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                sw.WriteLine(msg);
                sw.Close(); // File Close
            }
        }

        public static void SowLog(string[] strMsg, string path, string file)
        {   
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string sFilePath = path + file;
            if (!File.Exists(sFilePath))
            {
                using (StreamWriter sw = File.CreateText(sFilePath))
                {
                }
            }
            //File Open
            using (StreamWriter sw = File.AppendText(sFilePath))
            {
                for (int i = 0; i < strMsg.Length; i++)
                {
                    sw.WriteLine(strMsg[i]);
                }
                sw.Close(); // File Close
            }
        }

        #region READ File
        public static string[] ReadLog(string Url)
        {
            try
            {


            }
            catch
            {

            }

            if (!File.Exists(Url))
            {
                return null;
            }
            else
            {
                string[] strTxtValue = File.ReadAllLines(Url);
                if (strTxtValue.Length > 0)
                {
                    return strTxtValue;
                }
                else
                {
                    return null;
                }
            }
        }  
        #endregion      

    }
    
}
