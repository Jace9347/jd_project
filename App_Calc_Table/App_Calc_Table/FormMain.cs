﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace App_Calc_Table
{
    using JDC_Library;
    using JDX_Library_X86;
    using JDS_Library_X86;
    using System.IO;

    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private GroupBox gBoxCon;
        private Button btnLoad;
        private Button btnSave;
        private Button btnExit;
        private Label lbWaferId;
        private ListBox listBox1;

        //UserControl_ListView lsvData = new UserControl_ListView();TEST
        private TabControl tabCon;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TextBox txtKeyinDat;
        private Button btnKeyIn;
        private Timer timerUpdate;
        private IContainer components;
        private Label lbTickTime;
        UserControl_ListView[] lsvBuffer = new UserControl_ListView[] { };
        private int iWaitTime = 0;
        private RadioButton rBFloting;
        private RadioButton rBDecimal;
        private string[][,] strRawData = new string[][,] { };
        private GroupBox groupBox1;
        private Label lbSenCount;
        private Label lbCalCount;
        private RadioButton radioButton3;
        private RadioButton radioButton1;
        private RadioButton radioButton2;
        private RadioButton[] rbModelName = new RadioButton[3];
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;

        private string strLoadPath = null;
        private string strSavePath = null;
        private string[] CalibrationType;
        private int count = 0;

        RadioButton rbtnSelectName;     //2018.10.05 Kim

        enum SelectName {Temperature, Plazma, Hybrid}

        SelectName selectName;

        //ClsTempCalc tempCalc = new ClsTempCalc();
        ClsXmlFile xmlFunc = new ClsXmlFile();

        #region Load
        private void FormMain_Load(object sender, EventArgs e)
        {
            //label1.Text = "Temperature";          //2018.10.05 Kim 수정필요

            //rbtnSelectName.Text = "Temperature";

            //LIb Test
            //double de =  ClsTempCalc.Decode("119.187", "120");
            //double val = ClsTempCalc.Calculator("119.187", de.ToString());
            //Console.WriteLine(val);
            rbModelName[0] = radioButton1;
            rbModelName[1] = radioButton2;
            rbModelName[2] = radioButton3;


            tabCon.TabPages.Clear();
            tabCon.TabPages.Add("Calc");
            for (int i = 0; i < 9; i++)
            {
                tabCon.TabPages.Add((i * 10 + 40).ToString());
            }
            lsvBuffer = new UserControl_ListView[tabCon.TabCount];
            for (int i = 0; i < tabCon.TabCount; i++)
            {
                lsvBuffer[i] = new UserControl_ListView();
                tabCon.TabPages[i].Controls.Add(lsvBuffer[i]);
                lsvBuffer[i].Init();
            }
            //gBoxCon.Controls.Add(lsvData);
            //lsvData.Init();
            string[] head = new string[] { "40", "50", "60", "70", "80", "90", "100", "110", "120" };
            lsvBuffer[0].InitColumnsHeader(head);

            //panel1.Controls.Add(lsvData);
            //lsvData.InitColumnsHeader(head);
        } 
        #endregion

        #region Button Event
        private void 열기ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 끝내기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 다른이름으로저장ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 새로만들기ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region Init Control
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gBoxCon = new System.Windows.Forms.GroupBox();
            this.tabCon = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.lbWaferId = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txtKeyinDat = new System.Windows.Forms.TextBox();
            this.btnKeyIn = new System.Windows.Forms.Button();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.lbTickTime = new System.Windows.Forms.Label();
            this.rBFloting = new System.Windows.Forms.RadioButton();
            this.rBDecimal = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbSenCount = new System.Windows.Forms.Label();
            this.lbCalCount = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gBoxCon.SuspendLayout();
            this.tabCon.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBoxCon
            // 
            this.gBoxCon.Controls.Add(this.tabCon);
            this.gBoxCon.Location = new System.Drawing.Point(12, 198);
            this.gBoxCon.Name = "gBoxCon";
            this.gBoxCon.Size = new System.Drawing.Size(1028, 314);
            this.gBoxCon.TabIndex = 0;
            this.gBoxCon.TabStop = false;
            this.gBoxCon.Text = "Calibration Data";
            // 
            // tabCon
            // 
            this.tabCon.Controls.Add(this.tabPage1);
            this.tabCon.Controls.Add(this.tabPage2);
            this.tabCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCon.Location = new System.Drawing.Point(3, 18);
            this.tabCon.Name = "tabCon";
            this.tabCon.SelectedIndex = 0;
            this.tabCon.Size = new System.Drawing.Size(1022, 293);
            this.tabCon.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1014, 266);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1014, 268);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(596, 20);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(274, 84);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "LOAD";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(596, 108);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(274, 84);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(928, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(112, 45);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbWaferId
            // 
            this.lbWaferId.Location = new System.Drawing.Point(6, 18);
            this.lbWaferId.Name = "lbWaferId";
            this.lbWaferId.Size = new System.Drawing.Size(208, 24);
            this.lbWaferId.TabIndex = 4;
            this.lbWaferId.Text = "ID :";
            this.lbWaferId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 14;
            this.listBox1.Location = new System.Drawing.Point(3, 18);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(230, 73);
            this.listBox1.TabIndex = 5;
            // 
            // txtKeyinDat
            // 
            this.txtKeyinDat.Location = new System.Drawing.Point(9, 45);
            this.txtKeyinDat.Name = "txtKeyinDat";
            this.txtKeyinDat.Size = new System.Drawing.Size(134, 22);
            this.txtKeyinDat.TabIndex = 6;
            this.txtKeyinDat.Text = "00000";
            this.txtKeyinDat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnKeyIn
            // 
            this.btnKeyIn.Location = new System.Drawing.Point(149, 45);
            this.btnKeyIn.Name = "btnKeyIn";
            this.btnKeyIn.Size = new System.Drawing.Size(65, 22);
            this.btnKeyIn.TabIndex = 7;
            this.btnKeyIn.Text = "Key In";
            this.btnKeyIn.UseVisualStyleBackColor = true;
            this.btnKeyIn.Click += new System.EventHandler(this.btnKeyIn_Click);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // lbTickTime
            // 
            this.lbTickTime.Location = new System.Drawing.Point(925, 171);
            this.lbTickTime.Name = "lbTickTime";
            this.lbTickTime.Size = new System.Drawing.Size(115, 24);
            this.lbTickTime.TabIndex = 8;
            this.lbTickTime.Text = "Tick :";
            this.lbTickTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rBFloting
            // 
            this.rBFloting.AutoSize = true;
            this.rBFloting.Checked = true;
            this.rBFloting.Location = new System.Drawing.Point(22, 28);
            this.rBFloting.Name = "rBFloting";
            this.rBFloting.Size = new System.Drawing.Size(67, 18);
            this.rBFloting.TabIndex = 9;
            this.rBFloting.TabStop = true;
            this.rBFloting.Text = "Floting";
            this.rBFloting.UseVisualStyleBackColor = true;
            this.rBFloting.CheckedChanged += new System.EventHandler(this.rBFloting_CheckedChanged);
            // 
            // rBDecimal
            // 
            this.rBDecimal.AutoSize = true;
            this.rBDecimal.Location = new System.Drawing.Point(125, 28);
            this.rBDecimal.Name = "rBDecimal";
            this.rBDecimal.Size = new System.Drawing.Size(71, 18);
            this.rBDecimal.TabIndex = 10;
            this.rBDecimal.Text = "Decimal";
            this.rBDecimal.UseVisualStyleBackColor = true;
            this.rBDecimal.CheckedChanged += new System.EventHandler(this.rBDecimal_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbSenCount);
            this.groupBox1.Controls.Add(this.lbCalCount);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Location = new System.Drawing.Point(257, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 116);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // lbSenCount
            // 
            this.lbSenCount.Location = new System.Drawing.Point(19, 78);
            this.lbSenCount.Name = "lbSenCount";
            this.lbSenCount.Size = new System.Drawing.Size(131, 24);
            this.lbSenCount.TabIndex = 17;
            this.lbSenCount.Text = "Sen_Count : ";
            this.lbSenCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCalCount
            // 
            this.lbCalCount.Location = new System.Drawing.Point(24, 54);
            this.lbCalCount.Name = "lbCalCount";
            this.lbCalCount.Size = new System.Drawing.Size(131, 24);
            this.lbCalCount.TabIndex = 16;
            this.lbCalCount.Text = "Cal_Count : ";
            this.lbCalCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(201, 24);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(65, 18);
            this.radioButton3.TabIndex = 15;
            this.radioButton3.Text = "Hybrid";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 24);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(103, 18);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.Text = "Temperature";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(127, 24);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(67, 18);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.Text = "Plazma";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.rbtnSelectName_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbWaferId);
            this.groupBox2.Controls.Add(this.txtKeyinDat);
            this.groupBox2.Controls.Add(this.btnKeyIn);
            this.groupBox2.Location = new System.Drawing.Point(15, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(236, 80);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Location = new System.Drawing.Point(15, 98);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(236, 94);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rBFloting);
            this.groupBox4.Controls.Add(this.rBDecimal);
            this.groupBox4.Location = new System.Drawing.Point(257, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(296, 58);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // FormMain
            // 
            this.ClientSize = new System.Drawing.Size(1052, 524);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbTickTime);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gBoxCon);
            this.Controls.Add(this.btnLoad);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormMain";
            this.Text = "Calc app R1.0";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.gBoxCon.ResumeLayout(false);
            this.tabCon.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        #region Files Load
        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openModeldlg = new OpenFileDialog();
            openModeldlg.Filter = "*.csv|*.csv| All-File *.* | *.*";

            if (string.IsNullOrEmpty(strLoadPath))
            {
                openModeldlg.InitialDirectory = Application.StartupPath + "\\";
            }
            else
            {
                openModeldlg.InitialDirectory = strLoadPath;
            }

            openModeldlg.Multiselect = true;
            if (openModeldlg.ShowDialog() == DialogResult.OK)
            {
                listBox1.Items.Clear();
                iWaitTime = 0;
                strLoadPath = System.IO.Path.GetDirectoryName(openModeldlg.FileName);
                //string file = openModeldlg.FileName;
                string[] files = openModeldlg.FileNames;
                strRawData = new string[files.Length][,];
                const int iDefault = 18;
                for (int idx = 0; idx < files.Length; idx++)
                {
                    string[] dat = ClsLog.ReadLog(files[idx]);

                    string[] WaferID = dat[0].Split(',');

                    //listBox1.Items.Add(WaferID[1].PadRight(5, '0'));
                    if (WaferID.Length > 1)
                    {
                        listBox1.Items.Add(WaferID[1]);
                    }



                    for (int i = iDefault; i < dat.Length; i++)
                    {
                        string[] Buffer = dat[i].Split(',');

                        if (i == iDefault)
                        {
                            string[] heads = new string[Buffer.Length];
                            for (int head = 0; head < heads.Length; head++)
                            {
                                if (head == 0)
                                {
                                    heads[head] = "Label";
                                }
                                else
                                {
                                    heads[head] = "S" + (head - 1).ToString();
                                }
                            }
                            lsvBuffer[idx + 1].InitColumnsHeader(heads);
                            strRawData[idx] = new string[dat.Length - iDefault, Buffer.Length - 1];
                        }

                        ListViewItem lvi = new ListViewItem(Buffer[0]);
                        string[] subitems = new string[Buffer.Length - 1];
                        for (int ux = 0; ux < Buffer.Length - 1; ux++)
                        {
                            if (!string.IsNullOrEmpty(Buffer[ux + 1]))
                            {
                                double value = double.Parse(Buffer[ux + 1]);
                                subitems[ux] = value.ToString("0.000");

                                //subitems[ux] = Buffer[ux + 1].PadRight(6, '0');
                                //Console.WriteLine(subitems[ux]);
                                strRawData[idx][i - iDefault, ux] = subitems[ux];
                            }
                        }
                        lvi.SubItems.AddRange(subitems);
                        lsvBuffer[idx + 1].Add(lvi);
                    }
                    lsvBuffer[idx + 1].AutoArrange(UserControl_ListView.EResizeStyle.ColumnContent);
                }


                //CalibrationErrorChecker((UserControl_ListView)lsvBuffer[0]);
                CalibrationData();
            }
        } 
        #endregion
        private void btnSave_Click(object sender, EventArgs e)
        {
            if(lbWaferId.Text.Replace("ID :", "") == "")
            {
                lbWaferId.Text = txtKeyinDat.Text;
            }

            if (selectName.ToString() == "Temperature")     //2018.10.11 Kim
            {
                radioButton1.Checked = true;
            }
            else if(selectName.ToString() == "Plazma")
            {
                radioButton2.Checked = true;
            }
            else if (selectName.ToString() == "Hybrid")
            {
                radioButton3.Checked = true;
            }

            try
            {
                string id = lbWaferId.Text;

                id = id.Replace("ID :", null);
                id = id.Replace(" ", null);

                string sensorCount = lbSenCount.Text;
                sensorCount = sensorCount.Replace("Sen_Count :", null);
                sensorCount = sensorCount.Replace(" ", null);

                string calibrationCount = lbCalCount.Text;
                calibrationCount = calibrationCount.Replace("Cal_Count :", null);
                calibrationCount = calibrationCount.Replace(" ", null);

                string modelName = null;
                string key = null;
                for (int i = 0; i < 3; i++)
                {
                    if (rbModelName[i].Checked)
                    {
                        modelName = rbModelName[i].Text;
                        key = rbModelName[i].Text.Substring(0, 1);
                        break;
                    }
                }

                //Save Calibration File
                SaveFileDialog SaveModeldlg = new SaveFileDialog();
                SaveModeldlg.Filter = "*.xml|*.xml| All-File *.* | *.*";
                if (string.IsNullOrEmpty(strSavePath))
                {
                    SaveModeldlg.InitialDirectory = Application.StartupPath + "\\";
                }
                else
                {
                    SaveModeldlg.InitialDirectory = strSavePath;
                }   

                if (SaveModeldlg.ShowDialog() == DialogResult.OK)
                {
                    strSavePath = System.IO.Path.GetDirectoryName(SaveModeldlg.FileName);
                    string strFileName = SaveModeldlg.FileName;

                    string[][] dat = lsvBuffer[0].Get_SubItems();


                    using (XmlWriter wr = XmlWriter.Create(strFileName))
                    {
                        try
                        {
                            count = 0;
                            wr.WriteStartElement("Calibration");
                            wr.WriteStartElement("Signiture");
                            wr.WriteAttributeString("ID", lbWaferId.Text);     //WaferID

                            wr.WriteElementString("ID", lbWaferId.Text);  //Temperature, Plazma, Hybrid
                            wr.WriteElementString("Name", selectName.ToString());  //Temperature, Plazma, Hybrid
                            wr.WriteElementString("SensorCount", sensorCount);          //73, 103, 44
                            wr.WriteElementString("CalCount", calibrationCount);

                            for (int i = 0; i < dat.Length; i++)
                            {
                                wr.WriteElementString("K", dat[i][count]);

                                for (int count = 1; count < dat[i].Length; count++)
                                {
                                    wr.WriteElementString("k" + count, dat[i][count]);
                                }
                            }
                            wr.WriteEndElement();

                            wr.WriteEndDocument();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }


                    //xmlFunc.Write(strFileName, modelName, "ID", id);
                    //xmlFunc.Write(strFileName, modelName, "SensorCount", sensorCount);
                    //xmlFunc.Write(strFileName, modelName, "CalibrationCount", calibrationCount);

                    //string[] attr = new string[int.Parse(calibrationCount)];        //2018.10.05 Kim

                    //for (int i = 0; i < dat.Length; i++)
                    //{
                    //    xmlFunc.Write(strFileName, modelName, key + dat[i], dat[i][i]);
                    //}

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Save failed : "+ex.Message);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnKeyIn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKeyinDat.Text))
                return;
            lbWaferId.Text = string.Format("ID : {0}", txtKeyinDat.Text);
        }

        void CalibrationData()
        {
            //double[,] cal = new double[strRawData.Length, strRawData[i].GetLength(1)];
            string[] head = new string[strRawData[0].GetLength(1) + 1];
            CalibrationType = new string[strRawData.Length];
            for (int cnt = 0; cnt < head.Length; cnt++)
            {
                if (cnt == 0)
                {
                    head[cnt] = "Label";
                }
                else
                {
                    head[cnt] = "S" + (cnt - 1).ToString();
                }
            }
            lsvBuffer[0].InitColumnsHeader(head);

            
            for (int i = 0; i < strRawData.Length; i++)
            {
                double val = 0;
                double max = 0;
                double[,] dat = new double[strRawData.Length, strRawData[i].GetLength(1)];
                double absMax = -1;
                double[] absMaxs = new double[strRawData[i].GetLength(0)];
                for (int col = 0; col < strRawData[i].GetLength(1); col++) //Sensor Count
                {
                    max = -1;
                    for (int row = 0; row < strRawData[i].GetLength(0); row++) //Data Row Count
                    {
                        if (!string.IsNullOrEmpty(strRawData[i][row, col]))
                        {
                            val = double.Parse(strRawData[i][row, col]);
                            if (val > max)
                            {
                                max = val;
                            }
                        }
                    }
                    dat[i, col] = max;
                    absMaxs[col] = max;
                }

                //절대치 계산
                absMax = absMaxs.Max();
                double taget = absMax / 10;
                taget = (Math.Round(taget)) * 10;

                CalibrationType[count] = taget.ToString();      //2018.10.05 Kim Cailbration 온도 넣기
                count++;

                ListViewItem lvi = new ListViewItem(taget.ToString());
                for (int idx = 0; idx < dat.GetLength(1); idx++)
                {   
                    double cal = ClsTempCalc.Decode(dat[i, idx].ToString(), taget.ToString());
                    if (rBFloting.Checked)
                    {
                        lvi.SubItems.Add(cal.ToString("0.0000000000"));
                    }
                    if (rBDecimal.Checked)
                    {
                        cal = cal * Math.Pow(10, 10);
                        lvi.SubItems.Add(cal.ToString("000000000"));
                    }
                }
                lsvBuffer[0].Add(lvi);
            }
            lsvBuffer[0].ListViewErrorCheck();
            lsvBuffer[0].AutoArrange(UserControl_ListView.EResizeStyle.ColumnContent);

            lbCalCount.Text = string.Format("Cal_Count : {0}", lsvBuffer[0].ItemsCount());
            lbSenCount.Text = string.Format("Sen_Count : {0}", lsvBuffer[0].ColumnsCount() - 1);
        }
        
        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            iWaitTime++;
            lbTickTime.Text = iWaitTime.ToString();
        }

        #region Calc Convert
        private void rBFloting_CheckedChanged(object sender, EventArgs e)
        {
            if (lsvBuffer[0].isVirturlDiposed())
                return;
            if (rBFloting.Checked)
            {
                string[][] dat = lsvBuffer[0].Get_SubItems();
                ListViewItem[] lvi = new ListViewItem[dat.Length];
                for (int i = 0; i < dat.Length; i++)
                {
                    for (int j = 0; j < dat[i].Length; j++)
                    {
                        if (j == 0)
                        {
                            lvi[i] = new ListViewItem(dat[i][j]);
                        }
                        else
                        {
                            dat[i][j] = dat[i][j].Insert(0, "0.0");
                            lvi[i].SubItems.Add(dat[i][j]);
                        }
                    }

                }
                lsvBuffer[0].Clear();
                lsvBuffer[0].AddRange(lvi);
                lsvBuffer[0].AutoArrange(UserControl_ListView.EResizeStyle.ColumnContent);
            }
        }

        private void rBDecimal_CheckedChanged(object sender, EventArgs e)
        {
            if (lsvBuffer[0].isVirturlDiposed())
                return;
            if (rBDecimal.Checked)
            {
                string[][] dat = lsvBuffer[0].Get_SubItems();
                ListViewItem[] lvi = new ListViewItem[dat.Length];
                for (int i = 0; i < dat.Length; i++)
                {
                    for (int j = 0; j < dat[i].Length; j++)
                    {
                        if (j == 0)
                        {
                            lvi[i] = new ListViewItem(dat[i][j]);
                        }
                        else
                        {
                            dat[i][j] = dat[i][j].Substring(3);
                            lvi[i].SubItems.Add(dat[i][j]);
                        }
                    }

                }
                lsvBuffer[0].Clear();
                lsvBuffer[0].AddRange(lvi);
                lsvBuffer[0].AutoArrange(UserControl_ListView.EResizeStyle.ColumnContent);
            }
        }
        #endregion

        private void rbtnSelectName_CheckedChanged(object sender, EventArgs e)      //2018.10.05 Kim Temperature, Plazma, Hybrid 라디오버튼 때문에 생성
        {
            rbtnSelectName = sender as RadioButton;

            if (rbtnSelectName.Checked == false)
            {
                return;
            }

            switch (rbtnSelectName.Text)
            {
                case "Temperature": selectName = SelectName.Temperature; break;
                case "Plazma": selectName = SelectName.Plazma; break;
                case "Hybrid": selectName = SelectName.Hybrid; break;
            }
        }
    }
}
